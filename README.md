# RocketDock v.1.1

Welcome to a shorthand documentation of RocketDock. Here you can find
a small instruction on how to install RocketDock on your device 
and which changes has been done since the last version of RocketDock.

RocketDock is an open source project-management tool to manage
your own teams and projects in a smart way.

## Getting Started

These instructions will get you a copy of the project up and 
running on your local machine for development and testing 
purposes. See deployment for notes on how to deploy the 
project on a live system.

### Prerequisites

Before you can enjoy the smart way of organization, you need 
to install some tools & software on your device:

- node (RocketDock is a nodeJs application)
- mongoDB (NoSQL Database)
- angular (^7.0.0)

### Installing

With this step by step guide, nothing can go wrong :).

First of all, pull this project, enter the project and 
install all the requirements of node - called node_modules.
Afterwards you only need to build this project via Angular CLI.

```$bash
cd RocketDock
npm i
ng build
```

At this point your project will be ready to get launched on your
local device. Just run your server.js file in RocketDock/server.
The database will be established on its own.

If any error occur, try to contact our RocketDock team. Every error 
will be logged by RocketDock in /logs/errors/.

## Built with & Tools used

- [Angular](https://angular.io/) - Mobile & Desktop Framework
- [mongoDB](https://mongodb.com/) - NoSQL Database
- [ng-Bootstrap](https://ng-bootstrap.github.io) - (S)CSS-Framework
- [Express](https://expressjs.com/de/) - Web-Framework for NodeJs
- [Express-session](https://www.npmjs.com/package/express-session) - Session Management for Express
- [NodeJs](https://nodejs.org/en/) - JavaScript Engine
- and some other libraries as cryptoJs, passport, chartJs

## Versions

### RocketDock v.1.0.0
- UserManager (Admin-based tools to manage user)
- AccountManager (Login/Logout, Session Management)

### RocketDock v.1.1.0
- PermissionManager (Admin & User-based tools to manage projects)
- GroupManager (Admin-based tools to manage groups & permissions)
- ProjectManager (Admin & User-based tools to manage projects)
- LogManager (Admin-based tools for an overview of changes)

## License
This project is licensed under the MIT License - see the LICENSE.md file for details.
