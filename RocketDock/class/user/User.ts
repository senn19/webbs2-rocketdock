import {ObjectID} from "bson";
import {UserData} from "./UserData";
import {UserVerified} from "./UserVerified";

export class User {
  private _id : ObjectID;

  private _email : string;
  private _username : string;
  private _password : string;
  private _date : string;

  private _userData : UserData;
  private _userVerified : UserVerified;

  constructor(username : string = null, password : string = null, email : string = null,
              userData : UserData = null, userVerified : UserVerified = null) {
    this._id = new ObjectID();
    this._username = username;
    this._password = password;
    this._email = email;
    this._userData = userData;
    this._userVerified = userVerified;
    this._date = new Date().toUTCString();
  }

  public getUsername(): string {
    return this._username;
  }
  public setUsername(uName : string) : void {
    this._username = uName;
  }

  public getPassword(): string {
    return this._password;
  }
  public setPassword(password : string) : void {
    this._password = password;
  }

  public getDate() : string {
    return this._date;
  }
  public setDate(date : string) : void {
    this._date = date;
  }

  public getID() : ObjectID {
    return new ObjectID(this._id);
  }
  public setID(id) : void {
    this._id = new ObjectID(id);
  }

  public getEmail() : string {
    return this._email;
  }
  public setEmail(email : string) : void {
    this._email = email;
  }

  public getUserData() : UserData {
    return this._userData;
  }
  public setUserData(userData : UserData) : void {
    this._userData = userData;
  }

  public getUserVerified() : UserVerified {
    return this._userVerified;
  }
  public setUserVerified(verified : UserVerified) : void {
    this._userVerified = verified;
  }
}
