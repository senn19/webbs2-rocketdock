import {ObjectID} from "bson";

export class UserData {

  public _status : boolean = false;

  public _foreName : string;
  public _lastName : string;
  public _sex : string;

  public _avatar : string;
  public _description : string;
  public _group : ObjectID;

  constructor(foreName: string, lastName: string, sex?: string, avatar?: string, description?: string,
  group : ObjectID = null) {
    this._foreName = foreName;
    this._lastName = lastName;
    this._sex = sex;
    this._avatar = avatar;
    this._description = description;
    this._group = group;
  }
}
