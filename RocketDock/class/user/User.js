"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bson_1 = require("bson");
class User {
    constructor(username = null, password = null, email = null, userData = null, userVerified = null) {
        this._id = new bson_1.ObjectID();
        this._username = username;
        this._password = password;
        this._email = email;
        this._userData = userData;
        this._userVerified = userVerified;
        this._date = new Date().toUTCString();
    }
    getUsername() {
        return this._username;
    }
    setUsername(uName) {
        this._username = uName;
    }
    getPassword() {
        return this._password;
    }
    setPassword(password) {
        this._password = password;
    }
    getDate() {
        return this._date;
    }
    setDate(date) {
        this._date = date;
    }
    getID() {
        return new bson_1.ObjectID(this._id);
    }
    setID(id) {
        this._id = new bson_1.ObjectID(id);
    }
    getEmail() {
        return this._email;
    }
    setEmail(email) {
        this._email = email;
    }
    getUserData() {
        return this._userData;
    }
    setUserData(userData) {
        this._userData = userData;
    }
    getUserVerified() {
        return this._userVerified;
    }
    setUserVerified(verified) {
        this._userVerified = verified;
    }
}
exports.User = User;
//# sourceMappingURL=User.js.map