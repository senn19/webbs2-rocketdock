"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UserData {
    constructor(foreName, lastName, sex, avatar, description, group = null) {
        this._status = false;
        this._foreName = foreName;
        this._lastName = lastName;
        this._sex = sex;
        this._avatar = avatar;
        this._description = description;
        this._group = group;
    }
}
exports.UserData = UserData;
//# sourceMappingURL=UserData.js.map