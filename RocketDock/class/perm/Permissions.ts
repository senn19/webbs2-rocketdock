export class Permissions {

  private _isAdmin : boolean;
  private _isProjectManager : boolean;
  private _isMember : boolean;

  constructor(isAdmin: boolean = false,
              isProjectManager: boolean = false,
              isMember : boolean = false) {
    this._isAdmin = isAdmin;
    this._isProjectManager = isProjectManager;
    this._isMember = isMember;
  }
}
