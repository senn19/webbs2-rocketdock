"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Permissions {
    constructor(isAdmin = false, isProjectManager = false, isMember = false) {
        this._isAdmin = isAdmin;
        this._isProjectManager = isProjectManager;
        this._isMember = isMember;
    }
}
exports.Permissions = Permissions;
//# sourceMappingURL=Permissions.js.map