import {Permissions} from "./Permissions";
import {ObjectID} from "bson";

export class PermissionGroup {
  private _id : ObjectID;
  private _groupName : string;
  private _permissions : Permissions;

  constructor(groupName : string = null, permissions : Permissions = null) {
    this._id = new ObjectID();
    this._groupName = groupName;
    this._permissions = permissions;
  }

  public setGroupID(groupID : ObjectID) : void {
    this._id = new ObjectID(groupID);
  }
  public getGroupID() : ObjectID {
    return this._id;
  }

  public setGroupName(groupName : string) : void {
    this._groupName = groupName;
  }
  public getGroupName() : string {
    return this._groupName;
  }

  public setPermissions(permissions : Permissions) {
    this._permissions = permissions;
  }
  public getPermissions() : Permissions {
    return this._permissions;
  }
}
