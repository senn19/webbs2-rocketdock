"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bson_1 = require("bson");
class PermissionGroup {
    constructor(groupName = null, permissions = null) {
        this._id = new bson_1.ObjectID();
        this._groupName = groupName;
        this._permissions = permissions;
    }
    setGroupID(groupID) {
        this._id = new bson_1.ObjectID(groupID);
    }
    getGroupID() {
        return this._id;
    }
    setGroupName(groupName) {
        this._groupName = groupName;
    }
    getGroupName() {
        return this._groupName;
    }
    setPermissions(permissions) {
        this._permissions = permissions;
    }
    getPermissions() {
        return this._permissions;
    }
}
exports.PermissionGroup = PermissionGroup;
//# sourceMappingURL=PermissionGroup.js.map