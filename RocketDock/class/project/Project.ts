import {ObjectID} from "bson";
import {ProjectDetails} from "./ProjectDetails";

export class Project {
  private _id: ObjectID;
  private _projectName: string;
  private _projectDescription: string;
  private _projectStatus: boolean;
  private _projectDate: string;
  private _projectAuthor: ObjectID;
  private _projectDetails: ProjectDetails;


  constructor(projectName: string = null, projectDescription: string = null, projectAuthor: ObjectID = null, projectDetails: ProjectDetails = null,
              projectStatus: boolean = true) {
    this._id = new ObjectID();
    this._projectName = projectName;
    this._projectDescription = projectDescription;
    this._projectAuthor = projectAuthor;
    this._projectDetails = projectDetails;
    this._projectStatus = projectStatus;
    this._projectDate = new Date().toLocaleString();
  }

  public getProjectID(): ObjectID {
    return this._id;
  }

  public setProjectID(value) :void {
    this._id = new ObjectID(value);
  }

  public getProjectName(): string {
    return this._projectName;
  }

  public setProjectName(value: string) {
    this._projectName = value;
  }

  public getProjectDescription(): string {
    return this._projectDescription;
  }

  public setProjectDescription(value: string) {
    this._projectDescription = value;
  }

  public getProjectAuthor(): ObjectID {
    return this._projectAuthor;
  }

  public setProjectAuthor(value) {
    this._projectAuthor = value;
  }

  public getProjectDetails(): ProjectDetails {
    return this._projectDetails;
  }

  public setProjectDetails(value: ProjectDetails) {
    this._projectDetails = value;
  }

  public getProjectStatus(): boolean {
    return this._projectStatus;
  }

  public setProjectStatus(value: boolean) {
    this._projectStatus = value;
  }

  public getProjectDate(): string {
    return this._projectDate;
  }

  public setProjectDate(date: string): void {
    this._projectDate = date;
  }
}
