import {ObjectID} from "bson";

export class ToDo {
  private _id : ObjectID;

  private _projectID : ObjectID;
  private _authorID : ObjectID;
  private _todoContent : string;
  private _startDate : string;
  private _endDate : string;

  constructor(projectID: ObjectID = null, authorID: ObjectID = null, todoContent: string = null, startDate: string = null, endDate: string = null) {
    this._id = projectID;
    this._projectID = projectID;
    this._authorID = authorID;
    this._todoContent = todoContent;
    this._startDate = startDate;
    this._endDate = endDate;
  }

  public getToDoID(): ObjectID {
    return this._id;
  }

  public setToDoID(value: ObjectID): void {
    this._id = value;
  }

  public getProjectID(): ObjectID {
    return this._projectID;
  }

  public setProjectID(value: ObjectID): void {
    this._projectID = value;
  }

  public getAuthorID(): ObjectID {
    return this._authorID;
  }

  public setAuthorID(value: ObjectID): void {
    this._authorID = value;
  }

  public getTodoContent(): string {
    return this._todoContent;
  }

  public setTodoContent(value : string) {
    this._todoContent = value;
  }

  public getStartDate(): string {
    return this._startDate;
  }

  public setStartDate(value: string) {
    this._startDate = value;
  }

  public getEndDate(): string {
    return this._endDate;
  }

  public setEndDate(value: string) {
    this._endDate = value;
  }
}
