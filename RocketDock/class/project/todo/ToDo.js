"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ToDo {
    constructor(projectID = null, authorID = null, todoContent = null, startDate = null, endDate = null) {
        this._id = projectID;
        this._projectID = projectID;
        this._authorID = authorID;
        this._todoContent = todoContent;
        this._startDate = startDate;
        this._endDate = endDate;
    }
    getToDoID() {
        return this._id;
    }
    setToDoID(value) {
        this._id = value;
    }
    getProjectID() {
        return this._projectID;
    }
    setProjectID(value) {
        this._projectID = value;
    }
    getAuthorID() {
        return this._authorID;
    }
    setAuthorID(value) {
        this._authorID = value;
    }
    getTodoContent() {
        return this._todoContent;
    }
    setTodoContent(value) {
        this._todoContent = value;
    }
    getStartDate() {
        return this._startDate;
    }
    setStartDate(value) {
        this._startDate = value;
    }
    getEndDate() {
        return this._endDate;
    }
    setEndDate(value) {
        this._endDate = value;
    }
}
exports.ToDo = ToDo;
//# sourceMappingURL=ToDo.js.map