"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bson_1 = require("bson");
class Project {
    constructor(projectName = null, projectDescription = null, projectAuthor = null, projectDetails = null, projectStatus = true) {
        this._id = new bson_1.ObjectID();
        this._projectName = projectName;
        this._projectDescription = projectDescription;
        this._projectAuthor = projectAuthor;
        this._projectDetails = projectDetails;
        this._projectStatus = projectStatus;
        this._projectDate = new Date().toLocaleString();
    }
    getProjectID() {
        return this._id;
    }
    setProjectID(value) {
        this._id = new bson_1.ObjectID(value);
    }
    getProjectName() {
        return this._projectName;
    }
    setProjectName(value) {
        this._projectName = value;
    }
    getProjectDescription() {
        return this._projectDescription;
    }
    setProjectDescription(value) {
        this._projectDescription = value;
    }
    getProjectAuthor() {
        return this._projectAuthor;
    }
    setProjectAuthor(value) {
        this._projectAuthor = value;
    }
    getProjectDetails() {
        return this._projectDetails;
    }
    setProjectDetails(value) {
        this._projectDetails = value;
    }
    getProjectStatus() {
        return this._projectStatus;
    }
    setProjectStatus(value) {
        this._projectStatus = value;
    }
    getProjectDate() {
        return this._projectDate;
    }
    setProjectDate(date) {
        this._projectDate = date;
    }
}
exports.Project = Project;
//# sourceMappingURL=Project.js.map