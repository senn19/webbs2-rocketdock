"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bson_1 = require("bson");
class Comment {
    constructor(projectID = null, authorID = null, content = null) {
        this._id = new bson_1.ObjectID();
        this._projectID = projectID;
        this._authorID = authorID;
        this._content = content;
        this._date = new Date().toLocaleString();
    }
    getCommentID() {
        return this._id;
    }
    setCommentID(value) {
        this._id = value;
    }
    getProjectID() {
        return this._projectID;
    }
    setProjectID(value) {
        this._projectID = value;
    }
    getAuthorID() {
        return this._authorID;
    }
    setAuthorID(value) {
        this._authorID = value;
    }
    getContent() {
        return this._content;
    }
    setContent(value) {
        this._content = value;
    }
    getDate() {
        return this._date;
    }
    setDate(date) {
        this._date = date;
    }
}
exports.Comment = Comment;
//# sourceMappingURL=Comment.js.map