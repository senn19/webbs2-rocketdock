import {ObjectID} from "bson";

export class Comment {

  private _id : ObjectID;
  private _projectID : ObjectID;
  private _authorID : ObjectID;
  private _content : string;
  private _date : string;

  constructor(projectID: ObjectID = null, authorID: ObjectID= null, content: string = null) {
    this._id = new ObjectID();
    this._projectID = projectID;
    this._authorID = authorID;
    this._content = content;
    this._date = new Date().toLocaleString();
  }

  public getCommentID(): ObjectID {
    return this._id;
  }

  public setCommentID(value: ObjectID) {
    this._id = value;
  }

  public getProjectID(): ObjectID {
    return this._projectID;
  }

  public setProjectID(value: ObjectID) {
    this._projectID = value;
  }

  public getAuthorID(): ObjectID {
    return this._authorID;
  }

  public setAuthorID(value: ObjectID) {
    this._authorID = value;
  }

  public getContent(): string {
    return this._content;
  }

  public setContent(value: string) {
    this._content = value;
  }

  public getDate() : string {
    return this._date;
  }

  public setDate(date : string) {
    this._date = date;
  }
}
