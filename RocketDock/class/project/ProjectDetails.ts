import {User} from "../user/User";

export class ProjectDetails {

  public _projectAvatar : string;
  public _projectLevel : boolean;
  public _projectURIs : string[];
  public _projectTeam : User[];

  public todoList : boolean = false;
  public comments : boolean = false;

  constructor(projectURIs: string[] = null, projectTeam : User[] = null, projectAvatar : string = null, projectLevel : boolean = false) {
    this._projectAvatar = projectAvatar;
    this._projectURIs = projectURIs;
    this._projectTeam = projectTeam;
    this._projectLevel = projectLevel;
  }
}
