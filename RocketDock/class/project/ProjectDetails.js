"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ProjectDetails {
    constructor(projectURIs = null, projectTeam = null, projectAvatar = null, projectLevel = false) {
        this.todoList = false;
        this.comments = false;
        this._projectAvatar = projectAvatar;
        this._projectURIs = projectURIs;
        this._projectTeam = projectTeam;
        this._projectLevel = projectLevel;
    }
}
exports.ProjectDetails = ProjectDetails;
//# sourceMappingURL=ProjectDetails.js.map