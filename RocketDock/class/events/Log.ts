import {ObjectID} from "bson";

const LogAction = {
  login : "has logged in.",
  logout : "has logged out.",
  user_edit : "has edited %s",
  user_add : "has added %s",
  user_del : "has deleted %s",
  group_edit : "has edited g: %s",
  group_add : "has added g: %s",
  group_del : "has deleted g: %s",
  project_add: "has added p: %s",
  project_del: "has deleted p: %s",
  project_edit: "has deleted p: %s",
  comment_add: "has added p: %s",
  comment_edit: " has edited p: %s",
  comment_del: " has deleted p: %s",
  todo_add: "has added p: %s",
  todo_edit: "has edited p: %s",
  todo_del: "has deleted p: %s",
  contact_add : "has added p : %s",
  contact_edit : "has edited p : %s",
  contact_del : "has deleted p : %s",

};

const LogType = {
  login : "login",
  logout : "logout",
  user_edit: "user_edit",
  user_add: "user_add",
  user_del: "user_del",
  group_edit: "group_edit",
  group_add: "group_add",
  group_del: "group_del",
  project_add:"project_add",
  project_del:"project_del",
  project_edit:"project_edit",
  comment_add:" comment_add",
  comment_del:"comment_del",
  comment_edit:"comment_edit",
  todo_add:" todo_add",
  todo_edit:"todo_edit",
  todo_del:"todo_del",
  contact_add: "contact_add",
  contact_edit:"contact_edit",
  contact_del:"contact_del",
};

export class Log {
  public _userID: ObjectID;
  public _username: string;
  public _action: string;
  public _date: string;
  public _type: string;
  public _groupID : ObjectID;

  constructor(userID, groupID, username, action, type, date?) {
    this._userID = userID;
    this._username = username;
    this._action = action;
    this._date = (date ? date : new Date().toLocaleString());
    this._type = type;
    this._groupID = groupID;
  }
}

export {LogAction, LogType};
