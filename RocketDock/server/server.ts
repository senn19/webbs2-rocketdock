import * as express from 'express';
import {Request, Response} from 'express';
import * as session from 'express-session';
import * as fs from 'fs';
import * as https from 'https';
import * as http from 'http';
import * as passport from 'passport';
import {Profile} from 'passport';
import {AUTH, PATHS} from "./routes/PATHS";
import {log} from "./utils/logger/ConsoleLogger";
import {ClientPermissions} from "./utils/permissions/ClientPermissions";

const router = express();
const client = new ClientPermissions();
const privateKey = fs.readFileSync(__dirname + '/utils/openSSL/localhost.key', 'utf-8');
const certificate = fs.readFileSync(__dirname + '/utils/openSSL/localhost.crt', 'utf-8');

/** -   -   -   -   -   -   M I D D L E W A R E   -   -   -   -   -   -   **/

router.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
router.use(session({
  resave: true,
  saveUninitialized: true,
  name : 'Creative Cookie',
  secret: '2C44-4D44-WppQ38S',
  cookie : { maxAge: 20 * 60 * 1000 }}));
router.use(express.json({limit: '5mb'}));
router.use(passport.initialize());
router.use(passport.session());
passport.serializeUser((profile:Profile, done) => { done(null, profile); });
passport.deserializeUser((profile:Profile, done) => { done(null, profile); });



/** -   -   -   -   -   -   R O U T I N G   -   -   -   -   -   -   **/

// main path handling: every request will be redirected to user-control-panel
router.all("/", (req: Request, res: Response) => { res.redirect("/ucp"); });
// load the rendered angular files on main path
router.use('/', express.static(__dirname + "/../dist/RocketDock"));


/**
 * Middleware for /ucp verification, user needs to be logged in.
 * Otherwise the user will be redirected to login.
 */
router.use(PATHS.ucp, (req : Request, res : Response, next) => {
  if(client.IsLoggedIn(req)) return next();
  res.redirect(PATHS.login);
});
router.use(PATHS.ucp, express.static(`${__dirname}/../dist/RocketDock`));

/**
 * Middleware for /acp verification, user needs to be an admin.
 * Otherwise the user will be redirected to ucp.
 */
router.use(PATHS.acp, (req : Request, res : Response, next) => {
  if(client.IsAdmin(req)) return next();
  res.redirect(PATHS.ucp);
});
router.use(PATHS.acp, express.static(`${__dirname}/../dist/RocketDock`));

/**
 * Middleware for /login, only if user is not logged in.
 * Otherwise the user will be redirected to ucp.
 */
router.use(PATHS.login, (req : Request, res : Response, next) => {
  if(!(client.IsLoggedIn(req))) return next();
  res.redirect(PATHS.ucp);
});
router.use(PATHS.login, express.static(__dirname + "/../dist/RocketDock"));

router.use(PATHS.register, (req : Request, res : Response, next) => {
  if(!(client.IsLoggedIn(req))) return next();
  res.redirect(PATHS.ucp);
});
router.use(PATHS.register, express.static(__dirname + "/../dist/RocketDock"));


// UserManager Route
require("./routes/cruds/userCRUD")(router, client);
// RouterManager Route
require("./routes/account/account")(router, client);
// ProjectManager Route
require("./routes/cruds/projectCRUD")(router, client);
// GroupManager Route
require("./routes/cruds/permCRUD")(router);
// P: CommentManager Route
require("./routes/cruds/commentCRUD")(router, client);


// EventLogs
require("./routes/utils/eventlogs")(router);
// IconFinder
require("./routes/utils/iconFinder")(router);


/**          A U T H E N T I C A T I O N     R O U T E S           **/

/*
const GoogleAPI = AUTH.auth + AUTH.google;

router.get(GoogleAPI, passport.authenticate('google', { scope : [ 'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/plus.profile.emails.read' ]}));
router.get(GoogleAPI + AUTH.callback, passport.authenticate('google', {
  successRedirect : '/',
  failureRedirect : '/login'
}));


const FacebookAPI = AUTH.auth + AUTH.facebook;

router.get(FacebookAPI, passport.authenticate('facebook', {scope : ['user_gender']}));
router.get(FacebookAPI + AUTH.callback, passport.authenticate('facebook', {
  successRedirect : '/',
  failureRedirect : '/login'
}));

*/

router.use(PATHS.ucp + "/*", express.static(`${__dirname}/../dist/RocketDock`));
router.use(PATHS.acp + "/*", express.static(`${__dirname}/../dist/RocketDock`));
router.use((req : Request, res : Response) => {
  res.redirect(PATHS.ucp);
});

const server = http.createServer(router).listen(8080);
https.createServer({key : privateKey, cert : certificate}, router)
  .listen(8443, function () {
    log.info('');
    log.info('-------------------------------------------------------------');
    log.success('                 RocketDock v.1 is running                   ');
    log.info('-------------------------------------------------------------');
    log.success('       HTTPS:      https://localhost:8443                    ');
    log.success('       HTTP:       http://localhost:8080                     ');
    log.info('-------------------------------------------------------------');
  });
const io = require('socket.io')(server);
require('./sockets/project-sockets')(io);
