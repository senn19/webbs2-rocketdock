"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const passport = require("passport");
const passportFacebook = require("passport-facebook");
const AUTH_API_1 = require("./AUTH_API");
const UserVerified_1 = require("../../../class/user/UserVerified");
const UserData_1 = require("../../../class/user/UserData");
const User_1 = require("../../../class/user/User");
const UserManager_1 = require("../../class/user/UserManager");
const bson_1 = require("bson");
const PermissionGroup_1 = require("../../../class/perm/PermissionGroup");
const PermissionManager_1 = require("../../class/permissions/PermissionManager");
const mongoDb_1 = require("../../database/mongoDb");
const COLLECTIONS_1 = require("../../database/COLLECTIONS");
const LogEvent_1 = require("../../class/events/LogEvent");
const Log_1 = require("../../../class/events/Log");
passport.use(new passportFacebook.Strategy({
    clientID: AUTH_API_1.FACEBOOK.clientID,
    clientSecret: AUTH_API_1.FACEBOOK.clientSecret,
    callbackURL: AUTH_API_1.FACEBOOK.callbackURL,
    profileFields: ['id', 'email', 'name', 'gender', 'picture.type(large)'],
    passReqToCallback: true
}, function (req, accessToken, refreshToken, profile, done) {
    let user = new User_1.User(profile.name.givenName.substr(0, 1) +
        profile.name.familyName.substr(0, 3).toLowerCase() +
        profile.id.slice(-3), null, profile.email, new UserData_1.UserData(profile.name.givenName, profile.name.familyName, profile.gender, profile._json.picture.data.url), new UserVerified_1.UserVerified(null, null, profile.id));
    mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.userManager).findOne({ "_userVerified._facebookID": profile.id }, { projection: { _password: null } })
        .then((result) => {
        if (result) {
            let group = new PermissionGroup_1.PermissionGroup();
            group.setGroupID(new bson_1.ObjectID(result._userData._group));
            return new PermissionManager_1.PermissionManager(group).getPermissionGroup()
                .then((permissionGroup) => {
                req.session.user = result;
                req.session.group = permissionGroup;
                new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, Log_1.LogAction.login, Log_1.LogType.login));
                done(null, profile);
            });
        }
        let userManager = new UserManager_1.UserManager(user);
        userManager.addUser().then(() => {
            req.session.user = user;
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, Log_1.LogAction.login, Log_1.LogType.login));
            done(null, profile);
        });
    });
}));
//# sourceMappingURL=facebook.js.map