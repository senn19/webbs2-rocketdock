import * as passport from 'passport';
import * as passportFacebook from 'passport-facebook';
import {FACEBOOK} from "./AUTH_API";
import {UserVerified} from "../../../class/user/UserVerified";
import {UserData} from "../../../class/user/UserData";
import {User} from "../../../class/user/User";
import {UserManager} from "../../class/user/UserManager";
import {ObjectID} from "bson";
import {PermissionGroup} from "../../../class/perm/PermissionGroup";
import {PermissionManager} from "../../class/permissions/PermissionManager";
import {database} from "../../database/mongoDb";
import {COLLECTIONS} from "../../database/COLLECTIONS";
import {LogEvent} from "../../class/events/LogEvent";
import {Log, LogAction, LogType} from "../../../class/events/Log";

passport.use(new passportFacebook.Strategy({
    clientID: FACEBOOK.clientID,
    clientSecret: FACEBOOK.clientSecret,
    callbackURL: FACEBOOK.callbackURL,
    profileFields: ['id', 'email', 'name', 'gender','picture.type(large)'],
    passReqToCallback: true
  },
  function(req, accessToken, refreshToken, profile, done) {

    let user : User = new User(
      profile.name.givenName.substr(0,1) +
      profile.name.familyName.substr(0,3).toLowerCase() +
      profile.id.slice(-3),
      null,
      profile.email,
      new UserData(profile.name.givenName,
        profile.name.familyName,
        profile.gender,
        profile._json.picture.data.url),
      new UserVerified(null, null, profile.id)
    );

    database.collection(COLLECTIONS.userManager).findOne({"_userVerified._facebookID" : profile.id}, {projection:{_password: null}})
      .then((result : any) => {
        if(result) {
          let group : PermissionGroup = new PermissionGroup();
          group.setGroupID(new ObjectID(result._userData._group));
          return new PermissionManager(group).getPermissionGroup()
            .then((permissionGroup : PermissionGroup) => {
              req.session.user = result;
              req.session.group = permissionGroup;
              new LogEvent(new Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, LogAction.login, LogType.login));
              done(null, profile);
            });
        }
        let userManager : UserManager = new UserManager(user);
        userManager.addUser().then(()=> {
          req.session.user = user;
          new LogEvent(new Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, LogAction.login, LogType.login));
          done(null, profile)
        });
      });
  }
));
