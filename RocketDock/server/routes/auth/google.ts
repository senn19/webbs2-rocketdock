import * as passportGoogle from 'passport-google-oauth20';
import * as passport from 'passport';
import {GOOGLE} from './AUTH_API';
import {database} from "../../database/mongoDb";
import {COLLECTIONS} from "../../database/COLLECTIONS";
import {User} from "../../../class/user/User";
import {UserData} from "../../../class/user/UserData";
import {UserVerified} from "../../../class/user/UserVerified";
import {UserManager} from "../../class/user/UserManager";
import {PermissionManager} from "../../class/permissions/PermissionManager";
import {ObjectID} from "bson";
import {PermissionGroup} from "../../../class/perm/PermissionGroup";
import {LogEvent} from "../../class/events/LogEvent";
import {Log, LogAction, LogType} from "../../../class/events/Log";

passport.use(new passportGoogle.Strategy({
    clientID: GOOGLE.clientID,
    clientSecret: GOOGLE.clientSecret,
    callbackURL: GOOGLE.callbackURL,
    passReqToCallback: true
  },
  function(req, accessToken, refreshToken, profile, done) {

    let user : User = new User(
      profile.name.givenName.substr(0,1) +
      profile.name.familyName.substr(0,3).toLowerCase() +
      profile.id.slice(-3),
      null,
      profile.emails[0].value,
      new UserData(profile.name.givenName,
        profile.name.familyName,
        profile.gender,
        profile.photos[0].value.slice(0, -2) + "200"),
      new UserVerified(profile.id)
    );

    database.collection(COLLECTIONS.userManager).findOne({"_userVerified._googleID" : profile.id}, {projection:{_password: null}})
      .then((result : any) => {
        if(result) {
          let group : PermissionGroup = new PermissionGroup();
          group.setGroupID(new ObjectID(result._userData._group));
          return new PermissionManager(group).getPermissionGroup()
            .then((permissionGroup : PermissionGroup) => {
              req.session.user = result;
              req.session.group = permissionGroup;
              new LogEvent(new Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, LogAction.login, LogType.login));
              done(null, profile);
            });
        }
        let userManager : UserManager = new UserManager(user);
        userManager.addUser().then(()=> {
          req.session.user = user;
          new LogEvent(new Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, LogAction.login, LogType.login));
          done(null, profile)
        });
      });
  })
);

