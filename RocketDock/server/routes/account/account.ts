import {Request, Response} from "express";
import {Log, LogAction, LogType} from "../../../class/events/Log";
import {LogEvent} from "../../class/events/LogEvent";
import {PermissionGroup} from "../../../class/perm/PermissionGroup";
import {LoginManager} from "../../class/account/LoginManager";
import {PermissionManager} from "../../class/permissions/PermissionManager";
import {PATHS} from "../PATHS";
import {LANGUAGE} from "../../../language/LANGUAGE";

module.exports = (router, client) => {
  /**
   * Method checks if the combination of user and password exists in the database.
   * @param {e.Request} req {_username : string, _password : string}
   * @param {e.Response} res 200 OK, 500 Client Error
   */
  router.post(PATHS.login,(req : Request, res : Response) => {
    //create new instance of LoginManager with email, password and request.
    let loginManager = new LoginManager(req.body._email, req.body._password, req);
    loginManager.loginUser().then((user : any) => {

      //create new instance of PermissionGroup, set group and check if user has a group.
      let group: PermissionGroup = new PermissionGroup();
      group.setGroupID(user._userData ? user._userData._group : null);

      let permManager: PermissionManager = new PermissionManager(group);
      return permManager.getPermissionGroup()
    }).then((permissionGroup : PermissionGroup) => {

      req.session.group = (permissionGroup ? permissionGroup : null);

      new LogEvent(new Log(
        req.session.user._id,
        (req.session.user._userData ?
          req.session.user._userData._group :
          null),
        req.session.user._username,
        LogAction.login,
        LogType.login));

      res.status(200).json({message: LANGUAGE.EN.ACCOUNT_MANAGER.LOGIN});

    }).catch((error : any) => {
      res.status(500).json({message: error.message});
    });
  });


  /**
   * Method destroys the current session of an user
   * @param {e.Request} req Request
   * @param {e.Response} res 200 OK, 500 Server Error
   */
  router.get(PATHS.logout, (req : Request, res : Response) => {
    try {

      new LogEvent(new Log(
        req.session.user._id,
        (req.session.user._userData ?
          req.session.user._userData._group :
          null),
        req.session.user._username,
        LogAction.logout,
        LogType.logout));

      req.session.destroy(() => {
        req.logOut();
        res.status(200).json({message: LANGUAGE.EN.ACCOUNT_MANAGER.LOGOUT});
      });
    } catch(error) {
      res.status(500).json({message: LANGUAGE.EN.ACCOUNT_MANAGER.LOGOUT_ERROR});
    }
  });

  /**
   * Method response with session variables.
   * @param {e.Request} req Request
   * @param {e.Response} res Response with: login status, user, group
   */
  router.get(PATHS.check, (req : Request, res : Response) => {
    res.status(200).json({user : req.session.user, group : req.session.group});
  });
};
