"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Log_1 = require("../../../class/events/Log");
const LogEvent_1 = require("../../class/events/LogEvent");
const PermissionGroup_1 = require("../../../class/perm/PermissionGroup");
const LoginManager_1 = require("../../class/account/LoginManager");
const PermissionManager_1 = require("../../class/permissions/PermissionManager");
const PATHS_1 = require("../PATHS");
const LANGUAGE_1 = require("../../../language/LANGUAGE");
module.exports = (router, client) => {
    /**
     * Method checks if the combination of user and password exists in the database.
     * @param {e.Request} req {_username : string, _password : string}
     * @param {e.Response} res 200 OK, 500 Client Error
     */
    router.post(PATHS_1.PATHS.login, (req, res) => {
        //create new instance of LoginManager with email, password and request.
        let loginManager = new LoginManager_1.LoginManager(req.body._email, req.body._password, req);
        loginManager.loginUser().then((user) => {
            //create new instance of PermissionGroup, set group and check if user has a group.
            let group = new PermissionGroup_1.PermissionGroup();
            group.setGroupID(user._userData ? user._userData._group : null);
            let permManager = new PermissionManager_1.PermissionManager(group);
            return permManager.getPermissionGroup();
        }).then((permissionGroup) => {
            req.session.group = (permissionGroup ? permissionGroup : null);
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, (req.session.user._userData ?
                req.session.user._userData._group :
                null), req.session.user._username, Log_1.LogAction.login, Log_1.LogType.login));
            res.status(200).json({ message: LANGUAGE_1.LANGUAGE.EN.ACCOUNT_MANAGER.LOGIN });
        }).catch((error) => {
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method destroys the current session of an user
     * @param {e.Request} req Request
     * @param {e.Response} res 200 OK, 500 Server Error
     */
    router.get(PATHS_1.PATHS.logout, (req, res) => {
        try {
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, (req.session.user._userData ?
                req.session.user._userData._group :
                null), req.session.user._username, Log_1.LogAction.logout, Log_1.LogType.logout));
            req.session.destroy(() => {
                req.logOut();
                res.status(200).json({ message: LANGUAGE_1.LANGUAGE.EN.ACCOUNT_MANAGER.LOGOUT });
            });
        }
        catch (error) {
            res.status(500).json({ message: LANGUAGE_1.LANGUAGE.EN.ACCOUNT_MANAGER.LOGOUT_ERROR });
        }
    });
    /**
     * Method response with session variables.
     * @param {e.Request} req Request
     * @param {e.Response} res Response with: login status, user, group
     */
    router.get(PATHS_1.PATHS.check, (req, res) => {
        res.status(200).json({ user: req.session.user, group: req.session.group });
    });
};
//# sourceMappingURL=account.js.map