import {PATHS} from "../PATHS";
import {Request, Response} from "express";
import {IconManager} from "../../class/project/IconManager";

module.exports = (router) => {
  router.post(PATHS.iconFinder, (req: Request, res: Response) => {

    let iconFinder = new IconManager(req.body.string);
    iconFinder.iconSearch().then((image: string) => {
      res.status(200).json({image: image});
    });
  });
}
