"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoDb_1 = require("../../database/mongoDb");
const COLLECTIONS_1 = require("../../database/COLLECTIONS");
const ConsoleLogger_1 = require("../../utils/logger/ConsoleLogger");
const PATHS_1 = require("../PATHS");
module.exports = (router) => {
    /**
     * Method gets all events from database and returns them as response
     * @param {e.Request} req Request
     * @param {e.Response} res 200: event logs, 500 Server Error
     */
    router.get(PATHS_1.PATHS.events, (req, res) => {
        mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.eventManager).find().sort({ _date: -1 }).toArray()
            .then((response) => {
            res.status(200).json({ logs: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error });
        });
    });
};
//# sourceMappingURL=eventlogs.js.map