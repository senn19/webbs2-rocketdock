import {Request, Response} from "express";
import {database} from "../../database/mongoDb";
import {MongoError} from "mongodb";
import {COLLECTIONS} from "../../database/COLLECTIONS";
import {log} from "../../utils/logger/ConsoleLogger";
import {PATHS} from "../PATHS";

module.exports = (router) => {

  /**
   * Method gets all events from database and returns them as response
   * @param {e.Request} req Request
   * @param {e.Response} res 200: event logs, 500 Server Error
   */
  router.get(PATHS.events, (req : Request, res : Response) => {
    database.collection(COLLECTIONS.eventManager).find().sort({_date : -1}).toArray()
      .then((response : any[]) => {
        res.status(200).json({logs : response});
      }).catch((error : MongoError) => {
      log.error(error);
      res.status(500).json({message: error});
    });
  });
};
