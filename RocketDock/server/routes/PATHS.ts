const PATHS = {

  ucp : "/ucp",
  acp : "/acp",

  user_public : "/ucp/crud/user",
  user_private : "/acp/crud/user",
  user_create : "/crud/user",

  group_public : "/ucp/crud/groups",
  group_private : "/acp/crud/groups",

  project_crud : "/ucp/crud/projects",
  project_filter : "/ucp/filter/%s/projects",

  iconFinder : "/ucp/iconFinder/",
  comment_crud : "/ucp/crud/comment",

  login : "/login",
  register : "/login/create",
  logout : "/ucp/logout",
  check : "/ucp/check",

  events : "/acp/utils/events",

  filter : {
    user : "user",
  }
};

const AUTH = {
  /*
  auth : "/auth",
  callback : "/callback",

  google : "/google",
  googleRoute : require("./auth/google"),

  facebook : "/facebook",
  facebookRoute : require("./auth/facebook"),
  */
};

export {PATHS, AUTH};
