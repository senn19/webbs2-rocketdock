"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bson_1 = require("bson");
const ConsoleLogger_1 = require("../../utils/logger/ConsoleLogger");
const Log_1 = require("../../../class/events/Log");
const LogEvent_1 = require("../../class/events/LogEvent");
const sprintf_js_1 = require("sprintf-js");
const PermissionGroup_1 = require("../../../class/perm/PermissionGroup");
const PermissionManager_1 = require("../../class/permissions/PermissionManager");
const PATHS_1 = require("../PATHS");
const LANGUAGE_1 = require("../../../language/LANGUAGE");
module.exports = (router) => {
    /**
     * Method updates a permGroup in database with received data from request
     * @param {e.Request} req req.body.{group}
     * @param {e.Response} res 200: OK, 400: Nothing has changed, 500: Server Error
     */
    router.put(PATHS_1.PATHS.group_private, (req, res) => {
        // create instance of group and set data from request
        let group = new PermissionGroup_1.PermissionGroup(req.body.group._groupName, req.body.group._permissions);
        group.setGroupID(req.body.group._id);
        if (req.session.group && req.session.group._id == group.getGroupID())
            return res.status(401).json({ message: LANGUAGE_1.LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS });
        // create instance of permissionManager and call update method.
        let groupManager = new PermissionManager_1.PermissionManager(group);
        groupManager.editPermissionGroup().then((response) => {
            // log this event
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.group_edit, group.getGroupName()), Log_1.LogType.group_edit));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method adds a group to database
     * @param {e.Request} req req.body.{group}
     * @param {e.Response} res 200 OK, 400 No User has been added, 500 Server Error
     */
    router.post(PATHS_1.PATHS.group_private, (req, res) => {
        // create instance of group
        let group = new PermissionGroup_1.PermissionGroup(req.body.group._groupName, req.body.group._permissions);
        // create instance of permissionManager and call add method.
        let groupManager = new PermissionManager_1.PermissionManager(group);
        groupManager.createPermissionGroup().then((response) => {
            //log this event
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.group_add, group.getGroupName()), Log_1.LogType.group_add));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method deletes group from database
     * @param {e.Request} req req.params.{id}
     * @param {e.Response} res 200 Ok, 400 Group does not exist, 500 Server Error
     */
    router.delete(PATHS_1.PATHS.group_private + "/:id", (req, res) => {
        // get request data and split into id and groupName
        let params = req.params.id.toString().split("|");
        if (req.session.group && req.session.group._id == params[0])
            return res.status(401).json({ message: LANGUAGE_1.LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS });
        let group = new PermissionGroup_1.PermissionGroup(params[1]);
        group.setGroupID(new bson_1.ObjectID(params[0]));
        // create instance of permissionManager and call delete method.
        let groupManager = new PermissionManager_1.PermissionManager(group);
        groupManager.deletePermissionGroup().then((response) => {
            // log this event
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.group_del, params[1]), Log_1.LogType.group_del));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method gets all groups from database
     * @param {e.Request} req Request
     * @param {e.Response} res 200: {groupList}, 500 Server Error
     */
    router.get(PATHS_1.PATHS.group_private, (req, res) => {
        let groupManager = new PermissionManager_1.PermissionManager();
        groupManager.getPermissionGroups().then((groupList) => {
            res.status(200).json({ groupList: groupList });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method gets group by received data from client
     * @param {e.Request} req req.params.{data}  = {groupName|id}
     * @param {e.Response} res 200: {group}, 200 Group not found, 500 Server error
     */
    router.get(PATHS_1.PATHS.group_public + "/:id", (req, res) => {
        let group = new PermissionGroup_1.PermissionGroup();
        group.setGroupID(req.params.id.length >= 12 ? new bson_1.ObjectID(req.params.id) : new bson_1.ObjectID());
        let groupManager = new PermissionManager_1.PermissionManager(group);
        groupManager.getPermissionGroup()
            .then((group) => {
            res.status(200).json({ group: group });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
};
//# sourceMappingURL=permCRUD.js.map