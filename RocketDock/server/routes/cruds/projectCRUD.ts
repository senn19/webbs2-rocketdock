import {Project} from "../../../class/project/Project";
import {Request, Response} from 'express';
import {ObjectID} from "bson";
import {ProjectManager} from "../../class/project/ProjectManager";
import {LogEvent} from "../../class/events/LogEvent";
import {Log, LogAction, LogType} from "../../../class/events/Log";
import {sprintf} from "sprintf-js";
import {PATHS} from "../PATHS";
import {log} from "../../utils/logger/ConsoleLogger";
import {LANGUAGE} from "../../../language/LANGUAGE";
import {User} from "../../../class/user/User";

module.exports = (router, client) => {

  /**
   * Method updates a project in database with received data from request
   * @param {e.Request} req req.body.{project}
   * @param {e.Response} res 200: OK, 400: Nothing has changed, 500: Server Error
   */
  router.put(PATHS.project_crud, (req: Request, res: Response) => {
    // check if requested route is called form projectManager or Admin
    if (!client.IsProjectManager(req) && !client.IsAdmin(req))
      return res.status(401).json({message: LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS});
    // create instance of project and fill it with request data
    let projectTeam : User[] = [],
      project: Project = new Project(
        req.body.project._projectName,
        req.body.project._projectDescription,
        new ObjectID(req.body.project._projectAuthor),
        req.body.project._projectDetails,
        req.body.project._projectStatus);
    for(let member of req.body.project._projectDetails._projectTeam) {
      let user : User = new User(
        member._username,
        member._password,
        member._email,
        member._userData,
        member._userVerified
      );
      user.setID(member._id);
      user.setDate(member._date);
      projectTeam.push(user);
    }
    project.getProjectDetails()._projectTeam = projectTeam;
    project.setProjectID(new ObjectID(req.body.project._id));
    project.setProjectDate(req.body.project._projectDate);

    //create instance of projectManager and
    let projectManager: ProjectManager = new ProjectManager(project);
    projectManager.editProject().then((response: any) => {
      // log this event
      new LogEvent(new Log(
        req.session.user._id,
        req.session.user._userData._group,
        req.session.user._username,
        sprintf(LogAction.project_edit,
          project.getProjectName()),
        LogType.project_edit));

      res.status(200).json({message: response});
    }).catch((error: any) => {
      log.error(error);
      res.status(500).json({message: error.message})
    });
  });

  /**
   * Method adds a project to database
   * @param {e.Request} req req.body.{project}
   * @param {e.Response} res 200 OK, 400 No Project has been added, 500 Server Error
   */
  router.post(PATHS.project_crud, (req: Request, res: Response) => {
    // check if requested route is called form projectManager or Admin
    if (!client.IsProjectManager(req) && !client.IsAdmin(req))
      return res.status(401).json({message: LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS});

    // create new instance of project and fill it with request data
    let projectTeam : User[] = [],
      project: Project = new Project(
        req.body.project._projectName,
        req.body.project._projectDescription,
        new ObjectID(req.body.project._projectAuthor),
        req.body.project._projectDetails,
        req.body.project._projectStatus);
    for(let member of req.body.project._projectDetails._projectTeam) {
      let user : User = new User(
        member._username,
        member._password,
        member._email,
        member._userData,
        member._userVerified
      );
      user.setID(member._id);
      user.setDate(member._date);
      projectTeam.push(user);
    }
    project.getProjectDetails()._projectTeam = projectTeam;

    // create new instance of projectManager and call add method
    let projectManager: ProjectManager = new ProjectManager(project);
    projectManager.addProject().then((response: any) => {

      //log this event
      new LogEvent(new Log(
        req.session.user._id,
        req.session.user._userData._group,
        req.session.user._username,
        sprintf(LogAction.project_add,
          project.getProjectName()),
        LogType.project_add));

      res.status(200).json({message: response});
    }).catch((error: any) => {
      log.error(error);
      res.status(500).json({message: error.message})
    });
  });

  /**
   * Method deletes project from database
   * @param {e.Request} req req.params.{id}
   * @param {e.Response} res 200 Ok, 400 Project does not exist, 500 Server Error
   */
  router.delete(PATHS.project_crud + "/:id", (req: Request, res: Response) => {
    // check if requested route is called form projectManager or Admin
    if (!client.IsProjectManager(req) && !client.IsAdmin(req))
      return res.status(401).json({message: LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS});

    // create instance of project and set id for deleting the right project. :)
    let query: string = req.params.id.toString().split("|"),
      project: Project = new Project(query[1]);
    project.setProjectID(query[0]);

    // create instance of projectManager and call delete method.
    let projectManager: ProjectManager = new ProjectManager(project);
    projectManager.deleteProject().then((response: any) => {

      // log this event
      new LogEvent(new Log(
        req.session.user._id,
        req.session.user._userData._group,
        req.session.user._username,
        sprintf(LogAction.project_del,
          query[1]),
        LogType.project_del));

      res.status(200).json({message: response});
    }).catch((error: any) => {
      log.error(error);
      res.status(500).json({message: error.message})
    });
  });

  /**
   * Method gets all projects from database
   * @param {e.Request} req Request
   * @param {e.Response} res 200: {projectList}, 500 Server Error
   */
  router.get(PATHS.project_crud, (req: Request, res: Response) => {

    //create instance of projectManager and call get method
    let projectManager: ProjectManager = new ProjectManager();
    projectManager.getProjectList().then((projectList: Project[]) => {

      res.status(200).json({projectList: projectList});
    }).catch((error: any) => {
      log.error(error);
      res.status(500).json({message: error.message});
    });
  });

  /**
   * Method gets project by received data from client
   * @param {e.Request} req req.params.{data}  = {projectname|id}
   * @param {e.Response} res 200: {project}, 200 Project not found, 500 Server error
   */
  router.get(PATHS.project_crud + "/:id", (req: Request, res: Response) => {

    // create instance of project and set ID
    let project: Project = new Project();
    project.setProjectID(req.params.id.length >= 12 ? new ObjectID(req.params.id) : new ObjectID());

    // create instance of projectManager and call get method.
    let projectManager: ProjectManager = new ProjectManager(project);
    projectManager.getProjectByData().then((project: Project) => {
      res.status(200).json({project: project});

    }).catch((error: any) => {
      log.error(error);
      res.status(500).json({message: error.message});
    });
  });

  /**
   * Method filters project by received user from client
   * @param {e.Request} req req.params.{id}  = {username}
   * @param {e.Response} res 200: {project}, 200 Project not found, 500 Server error
   */
  router.get(sprintf(PATHS.project_filter, PATHS.filter.user) + "/:id", (req: Request, res: Response) => {

    let userID : ObjectID = (req.params.id.length >= 12 ? new ObjectID(req.params.id) : new ObjectID()),
      projectManager = new ProjectManager();
    projectManager.getProjectByUser(userID).then((projects: Project[]) => {
      res.status(200).json({projects: projects});
    }).catch((error: any) => {
      log.error(error);
      res.status(500).json({message: error.message});
    });
  });
};
