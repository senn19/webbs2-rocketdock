import {Request, Response} from 'express';
import {ObjectID} from "bson";
import {LogEvent} from "../../class/events/LogEvent";
import {Log, LogAction, LogType} from "../../../class/events/Log";
import {sprintf} from "sprintf-js";
import {log} from "../../utils/logger/ConsoleLogger";
import {CommentManager} from "../../class/project/comment/CommentManager";
import {Comment} from "../../../class/project/comments/Comment"
import {PATHS} from "../PATHS";
import {LANGUAGE} from "../../../language/LANGUAGE";

module.exports = (router, client) => {

  /**
   * Method updates a project in database with received data from request
   * @param {e.Request} req req.body.{project}
   * @param {e.Response} res 200: OK, 400: Nothing has changed, 500: Server Error
   */
  router.put(PATHS.comment_crud, ( req : Request, res : Response) => {
    if(!req.session.user._id != req.body.comment._authorID && (!client.IsAdmin(req) || !client.IsProjectManager(req)))
      return res.status(401).json({message : LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS});

    let comment: Comment = new Comment(
      new ObjectID(req.body.comment._projectID),
      new ObjectID(req.body.comment._authorID),
      req.body.comment._content);
    comment.setCommentID(new ObjectID(req.body.comment._id));

    let commentManager : CommentManager = new CommentManager(comment);
    commentManager.editComment().then((response:any )=>{

      new LogEvent(new Log(req.session.user._id,
        req.session.user._userData._group,
        req.session.user._username,
        sprintf(LogAction.comment_edit,
          comment.getContent()),
        LogType.comment_edit));

      res.status(200).json({message: response});
    }).catch((error : any) => {
      log.error(error);
      res.status(500).json({message: error.message})
    });
  });

  /**
   * Method adds a project to database
   * @param {e.Request} req req.body.{project}
   * @param {e.Response} res 200 OK, 400 No Project has been added, 500 Server Error
   */
  router.post(PATHS.comment_crud, (req : Request, res : Response) => {
    let comment : Comment = new Comment(
      new ObjectID(req.body.comment._projectID),
      new ObjectID(req.body.comment._authorID),
      req.body.comment._content);
    let commentManager : CommentManager = new CommentManager(comment);

    commentManager.addComment().then((response : any) => {

      new LogEvent(new Log(req.session.user._id,
        req.session.user._userData._group,
        req.session.user._username,
        sprintf(LogAction.comment_add,
          comment.getContent()),
        LogType.comment_add));

      res.status(200).json({message : response});
    }).catch((error : any) => {
      log.error(error);
      res.status(500).json({message : error.message})
    });
  });

  /**
   * Method deletes project from database
   * @param {e.Request} req req.params.{id}
   * @param {e.Response} res 200 Ok, 400 Project does not exist, 500 Server Error
   */
  router.delete(PATHS.comment_crud + "/:id", (req : Request, res : Response) => {
    if(!client.IsAdmin(req) && !client.IsProjectManager(req))
      return res.status(401).json({message : LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS});

    let params : any[] = req.params.id.toString().split("|"),
      comment : Comment = new Comment();
    comment.setCommentID(new ObjectID(params[0]));

    let commentManager : CommentManager = new CommentManager(comment);
    commentManager.deleteComment().then((response : any) => {

      new LogEvent(new Log(req.session.user._id,
        req.session.user._userData._group,
        req.session.user._username,
        sprintf(LogAction.comment_del,
          params[1]),
        LogType.comment_del));

      res.status(200).json({message : response});
    }).catch((error : any) => {
      log.error(error);
      res.status(500).json({message : error.message})
    });
  });

  /**
   * Method gets all projects from database
   * @param {e.Request} req Request
   * @param {e.Response} res 200: {projectList}, 500 Server Error
   */
  router.get(PATHS.comment_crud + "/:id", (req : Request, res : Response) => {
    let commentManager : CommentManager = new CommentManager();

    commentManager.getCommentList(new ObjectID(req.params.id)).then((commentList : Comment[]) => {

      res.status(200).json({ commentList : commentList});
    }).catch((error : any) => {
      log.error(error);
      res.status(500).json({message :error.message });
    });
  });
};
