"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Project_1 = require("../../../class/project/Project");
const bson_1 = require("bson");
const ProjectManager_1 = require("../../class/project/ProjectManager");
const LogEvent_1 = require("../../class/events/LogEvent");
const Log_1 = require("../../../class/events/Log");
const sprintf_js_1 = require("sprintf-js");
const PATHS_1 = require("../PATHS");
const ConsoleLogger_1 = require("../../utils/logger/ConsoleLogger");
const LANGUAGE_1 = require("../../../language/LANGUAGE");
const User_1 = require("../../../class/user/User");
module.exports = (router, client) => {
    /**
     * Method updates a project in database with received data from request
     * @param {e.Request} req req.body.{project}
     * @param {e.Response} res 200: OK, 400: Nothing has changed, 500: Server Error
     */
    router.put(PATHS_1.PATHS.project_crud, (req, res) => {
        // check if requested route is called form projectManager or Admin
        if (!client.IsProjectManager(req) && !client.IsAdmin(req))
            return res.status(401).json({ message: LANGUAGE_1.LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS });
        // create instance of project and fill it with request data
        let projectTeam = [], project = new Project_1.Project(req.body.project._projectName, req.body.project._projectDescription, new bson_1.ObjectID(req.body.project._projectAuthor), req.body.project._projectDetails, req.body.project._projectStatus);
        for (let member of req.body.project._projectDetails._projectTeam) {
            let user = new User_1.User(member._username, member._password, member._email, member._userData, member._userVerified);
            user.setID(member._id);
            user.setDate(member._date);
            projectTeam.push(user);
        }
        project.getProjectDetails()._projectTeam = projectTeam;
        project.setProjectID(new bson_1.ObjectID(req.body.project._id));
        project.setProjectDate(req.body.project._projectDate);
        //create instance of projectManager and
        let projectManager = new ProjectManager_1.ProjectManager(project);
        projectManager.editProject().then((response) => {
            // log this event
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.project_edit, project.getProjectName()), Log_1.LogType.project_edit));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method adds a project to database
     * @param {e.Request} req req.body.{project}
     * @param {e.Response} res 200 OK, 400 No Project has been added, 500 Server Error
     */
    router.post(PATHS_1.PATHS.project_crud, (req, res) => {
        // check if requested route is called form projectManager or Admin
        if (!client.IsProjectManager(req) && !client.IsAdmin(req))
            return res.status(401).json({ message: LANGUAGE_1.LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS });
        // create new instance of project and fill it with request data
        let projectTeam = [], project = new Project_1.Project(req.body.project._projectName, req.body.project._projectDescription, new bson_1.ObjectID(req.body.project._projectAuthor), req.body.project._projectDetails, req.body.project._projectStatus);
        for (let member of req.body.project._projectDetails._projectTeam) {
            let user = new User_1.User(member._username, member._password, member._email, member._userData, member._userVerified);
            user.setID(member._id);
            user.setDate(member._date);
            projectTeam.push(user);
        }
        project.getProjectDetails()._projectTeam = projectTeam;
        // create new instance of projectManager and call add method
        let projectManager = new ProjectManager_1.ProjectManager(project);
        projectManager.addProject().then((response) => {
            //log this event
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.project_add, project.getProjectName()), Log_1.LogType.project_add));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method deletes project from database
     * @param {e.Request} req req.params.{id}
     * @param {e.Response} res 200 Ok, 400 Project does not exist, 500 Server Error
     */
    router.delete(PATHS_1.PATHS.project_crud + "/:id", (req, res) => {
        // check if requested route is called form projectManager or Admin
        if (!client.IsProjectManager(req) && !client.IsAdmin(req))
            return res.status(401).json({ message: LANGUAGE_1.LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS });
        // create instance of project and set id for deleting the right project. :)
        let query = req.params.id.toString().split("|"), project = new Project_1.Project(query[1]);
        project.setProjectID(query[0]);
        // create instance of projectManager and call delete method.
        let projectManager = new ProjectManager_1.ProjectManager(project);
        projectManager.deleteProject().then((response) => {
            // log this event
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.project_del, query[1]), Log_1.LogType.project_del));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method gets all projects from database
     * @param {e.Request} req Request
     * @param {e.Response} res 200: {projectList}, 500 Server Error
     */
    router.get(PATHS_1.PATHS.project_crud, (req, res) => {
        //create instance of projectManager and call get method
        let projectManager = new ProjectManager_1.ProjectManager();
        projectManager.getProjectList().then((projectList) => {
            res.status(200).json({ projectList: projectList });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method gets project by received data from client
     * @param {e.Request} req req.params.{data}  = {projectname|id}
     * @param {e.Response} res 200: {project}, 200 Project not found, 500 Server error
     */
    router.get(PATHS_1.PATHS.project_crud + "/:id", (req, res) => {
        // create instance of project and set ID
        let project = new Project_1.Project();
        project.setProjectID(req.params.id.length >= 12 ? new bson_1.ObjectID(req.params.id) : new bson_1.ObjectID());
        // create instance of projectManager and call get method.
        let projectManager = new ProjectManager_1.ProjectManager(project);
        projectManager.getProjectByData().then((project) => {
            res.status(200).json({ project: project });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method filters project by received user from client
     * @param {e.Request} req req.params.{id}  = {username}
     * @param {e.Response} res 200: {project}, 200 Project not found, 500 Server error
     */
    router.get(sprintf_js_1.sprintf(PATHS_1.PATHS.project_filter, PATHS_1.PATHS.filter.user) + "/:id", (req, res) => {
        let userID = (req.params.id.length >= 12 ? new bson_1.ObjectID(req.params.id) : new bson_1.ObjectID()), projectManager = new ProjectManager_1.ProjectManager();
        projectManager.getProjectByUser(userID).then((projects) => {
            res.status(200).json({ projects: projects });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
};
//# sourceMappingURL=projectCRUD.js.map