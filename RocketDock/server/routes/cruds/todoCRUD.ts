import {Request, Response} from 'express';
import {ToDo} from "../../../class/project/todo/ToDo";
import {ObjectID} from "bson";
import {ToDoManager} from "../../class/project/todo/ToDoManager";
import {LogEvent} from "../../class/events/LogEvent";
import {Log, LogAction, LogType} from "../../../class/events/Log";
import {sprintf} from "sprintf-js";
import {log} from "../../utils/logger/ConsoleLogger";

module.exports = {

  /**
   * Method updates a todoentry in database with received data from request
   * @param {e.Request} req req.body.{todoentry}
   * @param {e.Response} res 200: OK, 400: Nothing has changed, 500: Server Error
   */

  todoEDIT: (req: Request, res: Response) => {
    let todo: ToDo = new ToDo(
      new ObjectID(req.body.todo._projectID),
      new ObjectID(req.body.todo._authorID),
      req.body.todo._todoContent,
      req.body.todo._startDate,
      req.body.todo._endDate
    );
    todo.setToDoID(new ObjectID(req.body.todo._id));
    let todoManager : ToDoManager = new ToDoManager(todo);
    todoManager.editTodo().then((response: any) => {
      new LogEvent(new Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf(LogAction.todo_edit, todo.getTodoContent()), LogType.todo_edit));
      res.status(200).json({message: response});
    }).catch((error: any) => {
      log.error(error);
      res.status(500).json({message:error})
    });
  },
  /**
   * Method adds a todoentry to database
   * @param {e.Request} req req.body.{todoentry}
   * @param {e.Response} res 200 OK, 400 No ToDoEntry has been added, 500 Server Error
   */

  todoADD: (req: Request, res: Response) => {
    let todo: ToDo = new ToDo(
      new ObjectID(req.body.todo._projectID),
      new ObjectID(req.body.todo._authorID),
      req.body.todo._todoContent,
      req.body.todo._startDate,
      req.body.todo._endDate
    );
    todo.setToDoID(new ObjectID(req.body.todo._id));
    let todoManager: ToDoManager = new ToDoManager(todo);
    todoManager.editTodo().then((response: any) => {
      new LogEvent(new Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf(LogAction.todo_add, todo.getTodoContent()), LogType.todo_add));
      res.status(200).json({message: response});
    }).catch((error: any) => {
      log.error(error);
      res.status(500).json({message:error})
    });
  },

  /**
   * Method deletes todoentry from database
   * @param {e.Request} req req.params.{id}
   * @param {e.Response} res 200 Ok, 400 todoentry does not exist, 500 Server Error
   */


  todoDELETE : (req: Request, res: Response) => {
    let params : any[] = req.params.id.toString().split("|"),
      todo: ToDo = new ToDo();
    todo.setToDoID(new ObjectID(params[0]));
    let todoManager : ToDoManager = new ToDoManager(todo);
    todoManager.deleteToDo().then((response: any) => {
      new LogEvent(new Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf(LogAction.todo_del, params[1]), LogType.todo_del));
      res.status(200).json({message: response});
    }).catch((error: any) => {
      log.error(error);
      res.status(500).json({message: error})
    });
  },

  /**
   * Method gets all todoentry from database
   * @param {e.Request} req Request
   * @param {e.Response} res 200: {todoList}, 500 Server Error
   */

  todoGET: (req: Request, res: Response) => {
    let todoManager : ToDoManager = new ToDoManager(null);
    todoManager.getTodoList().then((todoList: ToDo[]) => {
      res.status(200).json({ todoList: todoList});
    }).catch((error: any) => {
      log.error(error);
      res.status(500).json(error);
    });
  }

};
