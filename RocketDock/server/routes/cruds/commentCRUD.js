"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bson_1 = require("bson");
const LogEvent_1 = require("../../class/events/LogEvent");
const Log_1 = require("../../../class/events/Log");
const sprintf_js_1 = require("sprintf-js");
const ConsoleLogger_1 = require("../../utils/logger/ConsoleLogger");
const CommentManager_1 = require("../../class/project/comment/CommentManager");
const Comment_1 = require("../../../class/project/comments/Comment");
const PATHS_1 = require("../PATHS");
const LANGUAGE_1 = require("../../../language/LANGUAGE");
module.exports = (router, client) => {
    /**
     * Method updates a project in database with received data from request
     * @param {e.Request} req req.body.{project}
     * @param {e.Response} res 200: OK, 400: Nothing has changed, 500: Server Error
     */
    router.put(PATHS_1.PATHS.comment_crud, (req, res) => {
        if (!req.session.user._id != req.body.comment._authorID && (!client.IsAdmin(req) || !client.IsProjectManager(req)))
            return res.status(401).json({ message: LANGUAGE_1.LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS });
        let comment = new Comment_1.Comment(new bson_1.ObjectID(req.body.comment._projectID), new bson_1.ObjectID(req.body.comment._authorID), req.body.comment._content);
        comment.setCommentID(new bson_1.ObjectID(req.body.comment._id));
        let commentManager = new CommentManager_1.CommentManager(comment);
        commentManager.editComment().then((response) => {
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.comment_edit, comment.getContent()), Log_1.LogType.comment_edit));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method adds a project to database
     * @param {e.Request} req req.body.{project}
     * @param {e.Response} res 200 OK, 400 No Project has been added, 500 Server Error
     */
    router.post(PATHS_1.PATHS.comment_crud, (req, res) => {
        let comment = new Comment_1.Comment(new bson_1.ObjectID(req.body.comment._projectID), new bson_1.ObjectID(req.body.comment._authorID), req.body.comment._content);
        let commentManager = new CommentManager_1.CommentManager(comment);
        commentManager.addComment().then((response) => {
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.comment_add, comment.getContent()), Log_1.LogType.comment_add));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method deletes project from database
     * @param {e.Request} req req.params.{id}
     * @param {e.Response} res 200 Ok, 400 Project does not exist, 500 Server Error
     */
    router.delete(PATHS_1.PATHS.comment_crud + "/:id", (req, res) => {
        if (!client.IsAdmin(req) && !client.IsProjectManager(req))
            return res.status(401).json({ message: LANGUAGE_1.LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS });
        let params = req.params.id.toString().split("|"), comment = new Comment_1.Comment();
        comment.setCommentID(new bson_1.ObjectID(params[0]));
        let commentManager = new CommentManager_1.CommentManager(comment);
        commentManager.deleteComment().then((response) => {
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.comment_del, params[1]), Log_1.LogType.comment_del));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method gets all projects from database
     * @param {e.Request} req Request
     * @param {e.Response} res 200: {projectList}, 500 Server Error
     */
    router.get(PATHS_1.PATHS.comment_crud + "/:id", (req, res) => {
        let commentManager = new CommentManager_1.CommentManager();
        commentManager.getCommentList(new bson_1.ObjectID(req.params.id)).then((commentList) => {
            res.status(200).json({ commentList: commentList });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
};
//# sourceMappingURL=commentCRUD.js.map