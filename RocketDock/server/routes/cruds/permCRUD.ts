import {Request, Response} from 'express';
import {ObjectID} from "bson";
import {log} from "../../utils/logger/ConsoleLogger";
import {Log, LogAction, LogType} from "../../../class/events/Log";
import {LogEvent} from "../../class/events/LogEvent";
import {sprintf} from "sprintf-js";
import {PermissionGroup} from "../../../class/perm/PermissionGroup";
import {PermissionManager} from "../../class/permissions/PermissionManager";
import {PATHS} from "../PATHS";
import {LANGUAGE} from "../../../language/LANGUAGE";

module.exports = (router) => {

  /**
   * Method updates a permGroup in database with received data from request
   * @param {e.Request} req req.body.{group}
   * @param {e.Response} res 200: OK, 400: Nothing has changed, 500: Server Error
   */
  router.put(PATHS.group_private, (req: Request, res: Response) => {
    // create instance of group and set data from request
    let group : PermissionGroup = new PermissionGroup(
      req.body.group._groupName,
      req.body.group._permissions
    );
    group.setGroupID(req.body.group._id);

    if(req.session.group && req.session.group._id == group.getGroupID())
      return res.status(401).json({message : LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS});

    // create instance of permissionManager and call update method.
    let groupManager : PermissionManager = new PermissionManager(group);
    groupManager.editPermissionGroup().then((response : any) => {
      // log this event
      new LogEvent(new Log(
        req.session.user._id,
        req.session.user._userData._group,
        req.session.user._username,
        sprintf(LogAction.group_edit,
          group.getGroupName()),
        LogType.group_edit));

      res.status(200).json({message : response});
    }).catch((error : any) => {
      log.error(error);
      res.status(500).json({message : error.message})
    });
  });

  /**
   * Method adds a group to database
   * @param {e.Request} req req.body.{group}
   * @param {e.Response} res 200 OK, 400 No User has been added, 500 Server Error
   */
  router.post(PATHS.group_private, (req: Request, res: Response) => {
    // create instance of group
    let group : PermissionGroup = new PermissionGroup(
      req.body.group._groupName,
      req.body.group._permissions);

    // create instance of permissionManager and call add method.
    let groupManager : PermissionManager = new PermissionManager(group);
    groupManager.createPermissionGroup().then((response : any) => {

      //log this event
      new LogEvent(new Log(
        req.session.user._id,
        req.session.user._userData._group,
        req.session.user._username,
        sprintf(LogAction.group_add,
          group.getGroupName()),
        LogType.group_add));

      res.status(200).json({message : response});
    }).catch((error : any) => {
      log.error(error);
      res.status(500).json({message : error.message})
    });
  });

  /**
   * Method deletes group from database
   * @param {e.Request} req req.params.{id}
   * @param {e.Response} res 200 Ok, 400 Group does not exist, 500 Server Error
   */
  router.delete(PATHS.group_private + "/:id", (req: Request, res: Response) => {
    // get request data and split into id and groupName
    let params : any[] = req.params.id.toString().split("|");

    if(req.session.group && req.session.group._id == params[0])
      return res.status(401).json({message : LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS});

    let group : PermissionGroup = new PermissionGroup(params[1]);
    group.setGroupID(new ObjectID(params[0]));

    // create instance of permissionManager and call delete method.
    let groupManager : PermissionManager = new PermissionManager(group);
    groupManager.deletePermissionGroup().then((response: any) => {

      // log this event
      new LogEvent(
        new Log(req.session.user._id,
          req.session.user._userData._group,
          req.session.user._username,
          sprintf(LogAction.group_del,
            params[1]),
          LogType.group_del));

      res.status(200).json({message : response});
    }).catch((error : any) => {
      log.error(error);
      res.status(500).json({message : error.message})
    });
  });

  /**
   * Method gets all groups from database
   * @param {e.Request} req Request
   * @param {e.Response} res 200: {groupList}, 500 Server Error
   */
  router.get(PATHS.group_private, (req: Request, res: Response) => {
    let groupManager : PermissionManager = new PermissionManager();
    groupManager.getPermissionGroups().then((groupList : PermissionGroup[]) => {
      res.status(200).json({ groupList : groupList});
    }).catch((error : any) => {
      log.error(error);
      res.status(500).json({message : error.message });
    });
  });

  /**
   * Method gets group by received data from client
   * @param {e.Request} req req.params.{data}  = {groupName|id}
   * @param {e.Response} res 200: {group}, 200 Group not found, 500 Server error
   */
  router.get(PATHS.group_public + "/:id", (req: Request, res: Response) => {

    let group : PermissionGroup = new PermissionGroup();
    group.setGroupID(req.params.id.length >= 12 ? new ObjectID(req.params.id) : new ObjectID());

    let groupManager : PermissionManager = new PermissionManager(group);
    groupManager.getPermissionGroup()
      .then((group : PermissionGroup) => {

        res.status(200).json({ group : group});
      }).catch((error : any) => {
      log.error(error);
      res.status(500).json({ message : error.message });
    });
  });
};
