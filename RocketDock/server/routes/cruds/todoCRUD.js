"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ToDo_1 = require("../../../class/project/todo/ToDo");
const bson_1 = require("bson");
const ToDoManager_1 = require("../../class/project/todo/ToDoManager");
const LogEvent_1 = require("../../class/events/LogEvent");
const Log_1 = require("../../../class/events/Log");
const sprintf_js_1 = require("sprintf-js");
const ConsoleLogger_1 = require("../../utils/logger/ConsoleLogger");
module.exports = {
    /**
     * Method updates a todoentry in database with received data from request
     * @param {e.Request} req req.body.{todoentry}
     * @param {e.Response} res 200: OK, 400: Nothing has changed, 500: Server Error
     */
    todoEDIT: (req, res) => {
        let todo = new ToDo_1.ToDo(new bson_1.ObjectID(req.body.todo._projectID), new bson_1.ObjectID(req.body.todo._authorID), req.body.todo._todoContent, req.body.todo._startDate, req.body.todo._endDate);
        todo.setToDoID(new bson_1.ObjectID(req.body.todo._id));
        let todoManager = new ToDoManager_1.ToDoManager(todo);
        todoManager.editTodo().then((response) => {
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.todo_edit, todo.getTodoContent()), Log_1.LogType.todo_edit));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error });
        });
    },
    /**
     * Method adds a todoentry to database
     * @param {e.Request} req req.body.{todoentry}
     * @param {e.Response} res 200 OK, 400 No ToDoEntry has been added, 500 Server Error
     */
    todoADD: (req, res) => {
        let todo = new ToDo_1.ToDo(new bson_1.ObjectID(req.body.todo._projectID), new bson_1.ObjectID(req.body.todo._authorID), req.body.todo._todoContent, req.body.todo._startDate, req.body.todo._endDate);
        todo.setToDoID(new bson_1.ObjectID(req.body.todo._id));
        let todoManager = new ToDoManager_1.ToDoManager(todo);
        todoManager.editTodo().then((response) => {
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.todo_add, todo.getTodoContent()), Log_1.LogType.todo_add));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error });
        });
    },
    /**
     * Method deletes todoentry from database
     * @param {e.Request} req req.params.{id}
     * @param {e.Response} res 200 Ok, 400 todoentry does not exist, 500 Server Error
     */
    todoDELETE: (req, res) => {
        let params = req.params.id.toString().split("|"), todo = new ToDo_1.ToDo();
        todo.setToDoID(new bson_1.ObjectID(params[0]));
        let todoManager = new ToDoManager_1.ToDoManager(todo);
        todoManager.deleteToDo().then((response) => {
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.todo_del, params[1]), Log_1.LogType.todo_del));
            res.status(200).json({ message: response });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error });
        });
    },
    /**
     * Method gets all todoentry from database
     * @param {e.Request} req Request
     * @param {e.Response} res 200: {todoList}, 500 Server Error
     */
    todoGET: (req, res) => {
        let todoManager = new ToDoManager_1.ToDoManager(null);
        todoManager.getTodoList().then((todoList) => {
            res.status(200).json({ todoList: todoList });
        }).catch((error) => {
            ConsoleLogger_1.log.error(error);
            res.status(500).json(error);
        });
    }
};
//# sourceMappingURL=todoCRUD.js.map