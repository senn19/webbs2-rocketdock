import {Request, Response} from 'express';
import {User} from '../../../class/user/User';
import {ObjectID} from "bson";
import {UserManager} from "../../class/user/UserManager";
import {log} from "../../utils/logger/ConsoleLogger";
import {Log, LogAction, LogType} from "../../../class/events/Log";
import {LogEvent} from "../../class/events/LogEvent";
import {sprintf} from "sprintf-js";
import {PATHS} from "../PATHS";
import {LANGUAGE} from "../../../language/LANGUAGE";
import {CaptchaManager} from "../../class/user/CaptchaManager";

const SHA256 = require("crypto-js/sha256");

module.exports = (router, client) => {

  /**
   * Method updates an user in database with received data from request
   * @param {e.Request} req req.body.{user}
   * @param {e.Response} res 200: OK, 400: Nothing has changed, 500: Server Error
   */
  router.put(PATHS.user_public, (req : Request, res : Response) => {
    if(req.session.user._id != req.body.user._id && !client.IsAdmin(req))
      return res.status(401).json({message : LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS});
    // create new user object und fill it with data from request
    let user : User = new User(
      req.body.user._username,
      req.body.user._password,
      req.body.user._email,
      req.body.user._userData,
      req.body.user._userVerified);
    user.setID(req.body.user._id);
    user.setDate(req.body.user._date);

    // create new userManager object and call editUser function
    let userManager : UserManager = new UserManager(user);
    userManager.editUser().then((response : any) => {

      if(client.IsAdmin(req))
      // log this edit event
        new LogEvent(new Log(
          req.session.user._id,
          req.session.user._userData._group,
          req.session.user._username,
          sprintf(
            LogAction.user_edit,
            user.getUsername()),
          LogType.user_edit));

      res.status(200).json({message : response});
    }).catch((error : any) => {
      // log error in console for further development information
      log.error(error);
      res.status(500).json({message : error.message})
    });
  });


  /**
   * Method adds an user to database
   * @param {e.Request} req req.body.{user}
   * @param {e.Response} res 200 OK, 400 No User has been added, 500 Server Error
   */
  router.post(PATHS.user_create, (req : Request, res : Response) => {
    if(req.body.user._username == null)
      return res.status(400).json({message : LANGUAGE.EN.USER_MANAGER.MISSING_USERNAME});

    // create new user object and fill it with data from request
    let user: User = new User(
      req.body.user._username,
      SHA256(req.body.user._password).toString(),
      req.body.user._email,
      req.body.user._userData,
      req.body.user._userVerified);

    Promise.resolve().then(() => {
      if(req.body.captcha || !client.IsAdmin(req)) {
        let captchaManager : CaptchaManager = new CaptchaManager();
        return captchaManager.validateCaptcha(req.body.captcha);
      }
    }).then(() => {
      // create new userManager object and call addUser function
      let userManager: UserManager = new UserManager(user);
      return userManager.addUser()
    }).then((response : any) => {

      if(client.IsAdmin(req))
      // log this add event
        new LogEvent(new Log(
          req.session.user._id,
          req.session.user._userData._group,
          req.session.user._username,
          sprintf(LogAction.user_add,
            user.getUsername()),
          LogType.user_add));

      res.status(200).json({message : response});
    }).catch((error : any) => {
      // log error in console for further development information
      log.error(error);
      res.status(500).json({message : error.message});
    });
  });


  /**
   * Method deletes user from database
   * @param {e.Request} req req.params.{id}
   * @param {e.Response} res 200 Ok, 400 User does not exist, 500 Server Error
   */
  router.delete(PATHS.user_private + "/:id", (req : Request, res : Response) => {
    let query : string = req.params.id.toString().split("|");

    if(req.session.user._id == query[0] && client.IsAdmin(req))
      return res.status(401).json({message : LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS});

    // create new object of user and set ID
    let user : User = new User(query[1]);
    user.setID(query[0]);

    // create new object of userManager and call deleteUser
    let userManager : UserManager = new UserManager(user);
    userManager.deleteUser().then((response : any) => {
      // log this delete event
      new LogEvent(new Log(
        req.session.user._id,
        req.session.user._userData._group,
        req.session.user._username,
        sprintf(LogAction.user_del,
          query[1]),
        LogType.user_del));

      res.status(200).json({message : response});
    }).catch((error : any) => {
      // log error in console for further development information
      log.error(error);
      res.status(500).json({message : error.message})
    });
  });


  /**
   * Method gets all user from database
   * @param {e.Request} req Request
   * @param {e.Response} res 200: {userList}, 500 Server Error
   */
  router.get(PATHS.user_public, (req : Request, res : Response) => {

    //create new object of userManager and call getUserList
    let userManager : UserManager = new UserManager(null);
    userManager.getUserList().then((userList : User[]) => {

      res.status(200).json({ userList : userList});
    }).catch((error : any) => {
      // log error in console for further development information
      log.error(error);
      res.status(500).json({message : error.message});
    });
  });


  /**
   * Method gets user by received data from client
   * @param {e.Request} req req.params.{data}  = {username|id}
   * @param {e.Response} res 200: {user}, 200 User not found, 500 Server error
   */
  router.get(PATHS.user_public + "/:id", (req: Request, res : Response) => {
    // create user object and set params from request
    let user : User = new User();
    user.setID(req.params.id.length >= 12 ? new ObjectID(req.params.id) : new ObjectID());

    // create userManager object and call getUserByData
    let userManager : UserManager = new UserManager(user);
    userManager.getUserByData()
      .then((user : User) => {

        res.status(200).json({ user : user});
      }).catch((error : any) => {
      // log error in console for further development information
      log.error(error);
      res.status(500).json({message : error.message });
    });
  });
};
