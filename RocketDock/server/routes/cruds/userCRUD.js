"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("../../../class/user/User");
const bson_1 = require("bson");
const UserManager_1 = require("../../class/user/UserManager");
const ConsoleLogger_1 = require("../../utils/logger/ConsoleLogger");
const Log_1 = require("../../../class/events/Log");
const LogEvent_1 = require("../../class/events/LogEvent");
const sprintf_js_1 = require("sprintf-js");
const PATHS_1 = require("../PATHS");
const LANGUAGE_1 = require("../../../language/LANGUAGE");
const CaptchaManager_1 = require("../../class/user/CaptchaManager");
const SHA256 = require("crypto-js/sha256");
module.exports = (router, client) => {
    /**
     * Method updates an user in database with received data from request
     * @param {e.Request} req req.body.{user}
     * @param {e.Response} res 200: OK, 400: Nothing has changed, 500: Server Error
     */
    router.put(PATHS_1.PATHS.user_public, (req, res) => {
        if (req.session.user._id != req.body.user._id && !client.IsAdmin(req))
            return res.status(401).json({ message: LANGUAGE_1.LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS });
        // create new user object und fill it with data from request
        let user = new User_1.User(req.body.user._username, req.body.user._password, req.body.user._email, req.body.user._userData, req.body.user._userVerified);
        user.setID(req.body.user._id);
        user.setDate(req.body.user._date);
        // create new userManager object and call editUser function
        let userManager = new UserManager_1.UserManager(user);
        userManager.editUser().then((response) => {
            if (client.IsAdmin(req))
                // log this edit event
                new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.user_edit, user.getUsername()), Log_1.LogType.user_edit));
            res.status(200).json({ message: response });
        }).catch((error) => {
            // log error in console for further development information
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method adds an user to database
     * @param {e.Request} req req.body.{user}
     * @param {e.Response} res 200 OK, 400 No User has been added, 500 Server Error
     */
    router.post(PATHS_1.PATHS.user_create, (req, res) => {
        if (req.body.user._username == null)
            return res.status(400).json({ message: LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.MISSING_USERNAME });
        // create new user object and fill it with data from request
        let user = new User_1.User(req.body.user._username, SHA256(req.body.user._password).toString(), req.body.user._email, req.body.user._userData, req.body.user._userVerified);
        Promise.resolve().then(() => {
            if (req.body.captcha || !client.IsAdmin(req)) {
                let captchaManager = new CaptchaManager_1.CaptchaManager();
                return captchaManager.validateCaptcha(req.body.captcha);
            }
        }).then(() => {
            // create new userManager object and call addUser function
            let userManager = new UserManager_1.UserManager(user);
            return userManager.addUser();
        }).then((response) => {
            if (client.IsAdmin(req))
                // log this add event
                new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.user_add, user.getUsername()), Log_1.LogType.user_add));
            res.status(200).json({ message: response });
        }).catch((error) => {
            // log error in console for further development information
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method deletes user from database
     * @param {e.Request} req req.params.{id}
     * @param {e.Response} res 200 Ok, 400 User does not exist, 500 Server Error
     */
    router.delete(PATHS_1.PATHS.user_private + "/:id", (req, res) => {
        let query = req.params.id.toString().split("|");
        if (req.session.user._id == query[0] && client.IsAdmin(req))
            return res.status(401).json({ message: LANGUAGE_1.LANGUAGE.EN.PERMISSIONS.INSUFFICIENT_PERMISSIONS });
        // create new object of user and set ID
        let user = new User_1.User(query[1]);
        user.setID(query[0]);
        // create new object of userManager and call deleteUser
        let userManager = new UserManager_1.UserManager(user);
        userManager.deleteUser().then((response) => {
            // log this delete event
            new LogEvent_1.LogEvent(new Log_1.Log(req.session.user._id, req.session.user._userData._group, req.session.user._username, sprintf_js_1.sprintf(Log_1.LogAction.user_del, query[1]), Log_1.LogType.user_del));
            res.status(200).json({ message: response });
        }).catch((error) => {
            // log error in console for further development information
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method gets all user from database
     * @param {e.Request} req Request
     * @param {e.Response} res 200: {userList}, 500 Server Error
     */
    router.get(PATHS_1.PATHS.user_public, (req, res) => {
        //create new object of userManager and call getUserList
        let userManager = new UserManager_1.UserManager(null);
        userManager.getUserList().then((userList) => {
            res.status(200).json({ userList: userList });
        }).catch((error) => {
            // log error in console for further development information
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
    /**
     * Method gets user by received data from client
     * @param {e.Request} req req.params.{data}  = {username|id}
     * @param {e.Response} res 200: {user}, 200 User not found, 500 Server error
     */
    router.get(PATHS_1.PATHS.user_public + "/:id", (req, res) => {
        // create user object and set params from request
        let user = new User_1.User();
        user.setID(req.params.id.length >= 12 ? new bson_1.ObjectID(req.params.id) : new bson_1.ObjectID());
        // create userManager object and call getUserByData
        let userManager = new UserManager_1.UserManager(user);
        userManager.getUserByData()
            .then((user) => {
            res.status(200).json({ user: user });
        }).catch((error) => {
            // log error in console for further development information
            ConsoleLogger_1.log.error(error);
            res.status(500).json({ message: error.message });
        });
    });
};
//# sourceMappingURL=userCRUD.js.map