module.exports = function(io) {
  io.on('connection', function(socket) {
    socket.on('comment', function() {
      socket.broadcast.emit('comment');
    });
  });
};
