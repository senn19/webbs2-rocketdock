import * as fs from 'fs';
import {WriteStream} from 'fs';
import {TYPES} from './TYPES';
import {sprintf} from "sprintf-js";

const shell = require("shelljs");

class ConsoleLogger {

  private readonly _logPath : string = './logs/%s-console.log';
  private readonly _logger : WriteStream;

  private readonly _errPath : string = './logs/errors/%s-console.log';
  private readonly _errLogger : WriteStream;


  constructor() {
    let date : Date = new Date(),
      dateFormat : string = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();
    shell.mkdir('-p', './logs/errors/');
    this._logger = fs.createWriteStream(sprintf(this._logPath,dateFormat), {flags : 'a'});
    this._errLogger = fs.createWriteStream(sprintf(this._errPath,dateFormat), {flags : 'a'});
  }

  public info(message : string|any) : void {
    console.log(TYPES.INFO, message);
    this._logger.write(message + "\r\n");
  }

  public warn(message : string|any) : void {
    console.log(TYPES.WARNING, message);
    this._logger.write(message + "\r\n");
  }

  public error(message : string|any) : void {
    console.error(TYPES.DANGER, message);
    this._errLogger.write(message + "\r\n");
  }

  public success(message : string|any) : void {
    console.log(TYPES.SUCCESS, message);
    this._logger.write(message + "\r\n");
  }
}

const log : ConsoleLogger = new ConsoleLogger();
export {log};
