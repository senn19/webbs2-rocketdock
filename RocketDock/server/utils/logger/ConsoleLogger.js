"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const TYPES_1 = require("./TYPES");
const sprintf_js_1 = require("sprintf-js");
const shell = require("shelljs");
class ConsoleLogger {
    constructor() {
        this._logPath = './logs/%s-console.log';
        this._errPath = './logs/errors/%s-console.log';
        let date = new Date(), dateFormat = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();
        shell.mkdir('-p', './logs/errors/');
        this._logger = fs.createWriteStream(sprintf_js_1.sprintf(this._logPath, dateFormat), { flags: 'a' });
        this._errLogger = fs.createWriteStream(sprintf_js_1.sprintf(this._errPath, dateFormat), { flags: 'a' });
    }
    info(message) {
        console.log(TYPES_1.TYPES.INFO, message);
        this._logger.write(message + "\r\n");
    }
    warn(message) {
        console.log(TYPES_1.TYPES.WARNING, message);
        this._logger.write(message + "\r\n");
    }
    error(message) {
        console.error(TYPES_1.TYPES.DANGER, message);
        this._errLogger.write(message + "\r\n");
    }
    success(message) {
        console.log(TYPES_1.TYPES.SUCCESS, message);
        this._logger.write(message + "\r\n");
    }
}
const log = new ConsoleLogger();
exports.log = log;
//# sourceMappingURL=ConsoleLogger.js.map