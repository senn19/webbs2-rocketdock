"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TYPES = {
    INFO: '\x1b[34m%s\x1b[0m',
    WARNING: '\x1b[33m%s\x1b[0m',
    DANGER: '\x1b[31m%s\x1b[0m',
    SUCCESS: '\x1b[32m%s\x1b[0m'
};
exports.TYPES = TYPES;
//# sourceMappingURL=TYPES.js.map