"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ClientPermissions {
    IsAdmin(req) {
        return req.session.group && req.session.group._permissions._isAdmin;
    }
    IsProjectManager(req) {
        return req.session.group && req.session.group._permissions._isProjectManager;
    }
    IsLoggedIn(req) {
        return req.session.user || req.isAuthenticated();
    }
}
exports.ClientPermissions = ClientPermissions;
//# sourceMappingURL=ClientPermissions.js.map