import {Request} from 'express';

export class ClientPermissions {

  public IsAdmin(req : Request) : boolean {
    return req.session.group && req.session.group._permissions._isAdmin
  }

  public IsProjectManager(req : Request) : boolean {
    return req.session.group && req.session.group._permissions._isProjectManager
  }

  public IsLoggedIn(req : Request) : boolean {
    return req.session.user || req.isAuthenticated();
  }
}
