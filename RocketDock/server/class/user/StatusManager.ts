import {database} from "../../database/mongoDb";
import {COLLECTIONS} from "../../database/COLLECTIONS";
import {MongoError, UpdateWriteOpResult} from "mongodb";
import {DatabaseError} from "../errorTypes/DatabaseError";

export class StatusManager {
    private _user : any;


    constructor(user : any) {
      this._user = user;
    }

    public setStatus() : Promise<any> {

      return database.collection(COLLECTIONS.userManager).updateOne({_id : this._user._id}, {$set: { "_userData._status" : !this._user._userData._status}})
        .then((result : UpdateWriteOpResult) => {
          if(result.modifiedCount === 1) {
            return "Status has been successfully changed.";
          } else {
            throw Error("The Status couldn't be changed.");
          }
        }).catch((error : MongoError) => {
        throw new DatabaseError("There is an Error within your Database. Error Code:" +error);
      });
    }
}
