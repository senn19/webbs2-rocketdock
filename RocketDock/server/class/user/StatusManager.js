"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoDb_1 = require("../../database/mongoDb");
const COLLECTIONS_1 = require("../../database/COLLECTIONS");
const DatabaseError_1 = require("../errorTypes/DatabaseError");
class StatusManager {
    constructor(user) {
        this._user = user;
    }
    setStatus() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.userManager).updateOne({ _id: this._user._id }, { $set: { "_userData._status": !this._user._userData._status } })
            .then((result) => {
            if (result.modifiedCount === 1) {
                return "Status has been successfully changed.";
            }
            else {
                throw Error("The Status couldn't be changed.");
            }
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError("There is an Error within your Database. Error Code:" + error);
        });
    }
}
exports.StatusManager = StatusManager;
//# sourceMappingURL=StatusManager.js.map