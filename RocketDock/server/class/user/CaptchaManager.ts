const reCAPTCHA = require('recaptcha2');

export class CaptchaManager {

  private reCaptcha : any = new reCAPTCHA({
    siteKey : '6LcEyYsUAAAAAFOaUTFTyKeSD7sPZULsHBVkwv0s',
    secretKey : '6LcEyYsUAAAAAD-Cbw3zstIIbU9PHSXQ7rmCAFWW',
  });

  public validateCaptcha(captcha : string) : Promise<boolean> {
    return this.reCaptcha.validate(captcha)
      .catch((error : any) => {
          throw Error(this.reCaptcha.translateErrors(error));
      });
  }
}
