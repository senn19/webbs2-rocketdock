"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("../../../class/user/User");
const COLLECTIONS_1 = require("../../database/COLLECTIONS");
const mongoDb_1 = require("../../database/mongoDb");
const mongodb_1 = require("mongodb");
const DatabaseError_1 = require("../errorTypes/DatabaseError");
const LANGUAGE_1 = require("../../../language/LANGUAGE");
const sprintf_js_1 = require("sprintf-js");
const SHA256 = require("crypto-js/sha256");
class UserManager {
    constructor(user) {
        this._emailRegExp = /^(([^<>()\[\].,;:\s@"]+(\.[^<>()\[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
        this._usernameRexExp = /^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/;
        this._user = user;
    }
    validateEmail() {
        return this._emailRegExp.test(this._user.getEmail());
    }
    ;
    validateUsername() {
        return this._usernameRexExp.test(this._user.getUsername());
    }
    editUser() {
        this._updateUser = {};
        // validate editUser information, if attributes aren't set, they won't be updated.
        if (this._user.getUsername())
            this._updateUser._username = this._user.getUsername();
        if (this._user.getPassword())
            this._updateUser._password = SHA256(this._user.getPassword()).toString();
        if (this._user.getEmail())
            this._updateUser._email = this._user.getEmail();
        if (this._user.getUserData())
            this._updateUser._userData = this._user.getUserData();
        if (this._user.getUserData()._group)
            this._updateUser._userData._group = new mongodb_1.ObjectID(this._user.getUserData()._group);
        if (this._user.getUserVerified())
            this._updateUser._userVerified = this._user.getUserVerified();
        return this.getUserByData()
            .then((response) => {
            if (response.getID().toHexString() != this._user.getID().toHexString() &&
                (response.getEmail() == this._updateUser._email || response.getUsername() == this._updateUser._username))
                throw Error(sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.UPDATE_ALREADY_EXISTS, this._user.getUsername()));
            if (!this.validateEmail())
                throw Error(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.EMAIL_INVALID);
            if (!this.validateUsername())
                throw Error(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.USERNAME_INVALID);
            // update user in database
            return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.userManager)
                .updateOne({ _id: this._user.getID() }, { $set: this._updateUser });
        }).then((result) => {
            if (result.modifiedCount === 1)
                return sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.UPDATE, this._user.getUsername());
            throw Error(sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.UPDATE_ERROR, this._user.getUsername()));
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    addUser() {
        //first check if user already exists
        return this.getUserByData()
            .then((response) => {
            if (response)
                throw Error(sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.ADD_ALREADY_EXISTS, (this._user.getUsername() != null ? this._user.getUsername() : this._user.getEmail())));
            if (!this.validateEmail())
                throw Error(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.EMAIL_INVALID);
            if (!this.validateUsername())
                throw Error(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.USERNAME_INVALID);
            // if user does not exist insert a new user into database
            return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.userManager)
                .insertOne(this._user);
        }).then((result) => {
            if (result.insertedCount === 1)
                return sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.ADD, (this._user.getUsername() != null ? this._user.getUsername() : this._user.getEmail()));
            throw Error(sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.ADD_ERROR, (this._user.getUsername() != null ? this._user.getUsername() : this._user.getEmail())));
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    deleteUser() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.userManager).deleteOne({ _id: this._user.getID() })
            .then((result) => {
            if (result.deletedCount === 1)
                return sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.DELETE, this._user.getUsername());
            throw Error(sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.USER_MANAGER.DELETE_ERROR, this._user.getUsername()));
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    getUserList() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.userManager)
            .find({}, { projection: { _password: null } })
            .toArray()
            .then((response) => {
            let userList = [];
            for (let item of response) {
                let user = new User_1.User(item._username, item._password, item._email, item._userData);
                user.setDate(item._date);
                user.setID(item._id);
                userList.push(user);
            }
            return userList;
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    getUserByData() {
        //first define the mongoDB filter query. prevent filtering null values
        let query = [{ _id: this._user.getID() }];
        if (this._user.getUsername() != null)
            query[query.length] = { _username: new RegExp(this._user.getUsername(), "i") };
        if (this._user.getEmail() != null)
            query[query.length] = { _email: new RegExp(this._user.getEmail(), "i") };
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.userManager)
            .findOne({ $or: query }, { projection: { _password: null } })
            .then((response) => {
            if (response) {
                let user = new User_1.User(response._username, response._password, response._email, response._userData, response._userVerified);
                user.setDate(response._date);
                user.setID(response._id);
                return user;
            }
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
}
exports.UserManager = UserManager;
//# sourceMappingURL=UserManager.js.map