import {User} from "../../../class/user/User";
import {COLLECTIONS} from "../../database/COLLECTIONS";
import {database} from "../../database/mongoDb";
import {MongoError, ObjectID, UpdateWriteOpResult} from "mongodb";
import {DatabaseError} from "../errorTypes/DatabaseError";
import {DeleteWriteOpResultObject, InsertOneWriteOpResult} from "mongodb";
import {LANGUAGE} from "../../../language/LANGUAGE";
import {sprintf, vsprintf} from "sprintf-js";


const SHA256 = require("crypto-js/sha256");

export class UserManager {

  private _user : User;
  private _updateUser : any;

  private _emailRegExp : RegExp =
    /^(([^<>()\[\].,;:\s@"]+(\.[^<>()\[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;

  private _usernameRexExp : RegExp =
    /^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/;

  constructor(user : User) {
    this._user = user;
  }


  private validateEmail() : boolean {
    return this._emailRegExp.test(this._user.getEmail());
  };

  private validateUsername() : boolean {
    return this._usernameRexExp.test(this._user.getUsername());
  }

  public editUser() : Promise<string> {
    this._updateUser = {};

    // validate editUser information, if attributes aren't set, they won't be updated.
    if (this._user.getUsername())
      this._updateUser._username = this._user.getUsername();
    if (this._user.getPassword())
      this._updateUser._password = SHA256(this._user.getPassword()).toString();
    if (this._user.getEmail())
      this._updateUser._email = this._user.getEmail();
    if (this._user.getUserData())
      this._updateUser._userData = this._user.getUserData();
    if (this._user.getUserData()._group)
      this._updateUser._userData._group = new ObjectID(this._user.getUserData()._group);
    if (this._user.getUserVerified())
      this._updateUser._userVerified = this._user.getUserVerified();

    return this.getUserByData()
      .then((response: User) => {
        if (response.getID().toHexString() != this._user.getID().toHexString() &&
          (response.getEmail() == this._updateUser._email || response.getUsername() == this._updateUser._username))
          throw Error(sprintf(LANGUAGE.EN.USER_MANAGER.UPDATE_ALREADY_EXISTS, this._user.getUsername()));

        if (!this.validateEmail())
          throw Error(LANGUAGE.EN.USER_MANAGER.EMAIL_INVALID);
        if (!this.validateUsername())
          throw Error(LANGUAGE.EN.USER_MANAGER.USERNAME_INVALID);

        // update user in database
        return database.collection(COLLECTIONS.userManager)
          .updateOne({_id: this._user.getID()}, {$set: this._updateUser})
      }).then((result: UpdateWriteOpResult) => {

        if (result.modifiedCount === 1)
          return sprintf(LANGUAGE.EN.USER_MANAGER.UPDATE, this._user.getUsername());
        throw Error(sprintf(LANGUAGE.EN.USER_MANAGER.UPDATE_ERROR, this._user.getUsername()));

      }).catch((error: MongoError) => {
        if (error instanceof Error)
          throw new Error(error.message);
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      })
  }


  public addUser() : Promise<string> {

    //first check if user already exists
    return this.getUserByData()
      .then((response : any) => {
        if(response)
          throw Error(sprintf(LANGUAGE.EN.USER_MANAGER.ADD_ALREADY_EXISTS, (this._user.getUsername() != null ? this._user.getUsername() : this._user.getEmail())));

        if (!this.validateEmail())
          throw Error(LANGUAGE.EN.USER_MANAGER.EMAIL_INVALID);
        if (!this.validateUsername())
          throw Error(LANGUAGE.EN.USER_MANAGER.USERNAME_INVALID);
        // if user does not exist insert a new user into database
        return database.collection(COLLECTIONS.userManager)
          .insertOne(this._user)
      }).then((result: InsertOneWriteOpResult) => {

        if (result.insertedCount === 1)
          return sprintf(LANGUAGE.EN.USER_MANAGER.ADD, (this._user.getUsername() != null ? this._user.getUsername() : this._user.getEmail()));
        throw Error(sprintf(LANGUAGE.EN.USER_MANAGER.ADD_ERROR, (this._user.getUsername() != null ? this._user.getUsername() : this._user.getEmail())));

      }).catch((error: MongoError) => {
        if(error instanceof Error)
          throw new Error(error.message);
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }


  public deleteUser() : Promise<string> {
    return database.collection(COLLECTIONS.userManager).deleteOne({_id : this._user.getID()})
      .then((result : DeleteWriteOpResultObject) => {
        if(result.deletedCount === 1)
          return sprintf(LANGUAGE.EN.USER_MANAGER.DELETE, this._user.getUsername());
        throw Error(sprintf(LANGUAGE.EN.USER_MANAGER.DELETE_ERROR, this._user.getUsername()));
      }).catch((error : MongoError) => {

        if(error instanceof Error)
          throw new Error(error.message);
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }


  public getUserList() : Promise<User[]> {
    return database.collection(COLLECTIONS.userManager)
      .find({}, {projection:{_password: null}})
      .toArray()
      .then((response : any[]) => {
        let userList : User[] = [];
        for(let item of response) {
          let user : User = new User(
            item._username,
            item._password,
            item._email,
            item._userData);
          user.setDate(item._date);
          user.setID(item._id);
          userList.push(user);
        }
        return userList;
      }).catch((error : MongoError) => {
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }


  public getUserByData() : Promise<User> {
    //first define the mongoDB filter query. prevent filtering null values
    let query : Array<any> = [{_id : this._user.getID()}];
    if(this._user.getUsername() != null)
      query[query.length] = {_username : new RegExp(this._user.getUsername(), "i")};
    if(this._user.getEmail() != null)
      query[query.length] = {_email : new RegExp(this._user.getEmail(), "i")};

    return database.collection(COLLECTIONS.userManager)
      .findOne({$or: query},{projection:{_password : null}})
      .then((response : any) => {
        if(response) {
          let user: User = new User(
            response._username,
            response._password,
            response._email,
            response._userData,
            response._userVerified);
          user.setDate(response._date);
          user.setID(response._id);
          return user;
        }
      }).catch((error : MongoError) => {
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }
}
