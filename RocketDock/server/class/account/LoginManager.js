"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoDb_1 = require("../../database/mongoDb");
const COLLECTIONS_1 = require("../../database/COLLECTIONS");
const DatabaseError_1 = require("../errorTypes/DatabaseError");
const LANGUAGE_1 = require("../../../language/LANGUAGE");
const sprintf_js_1 = require("sprintf-js");
const SHA256 = require("crypto-js/sha256");
class LoginManager {
    constructor(email, password, req = null) {
        this._email = email;
        this._password = SHA256(password).toString();
        this._req = req;
    }
    /**
     * method to login a given user, creates a session with user and loggedIn status.
     * @return {Promise} returns user, if found
     */
    loginUser() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.userManager)
            .findOne({ $and: [{ _email: this._email }, { _password: this._password }] }, { projection: { _password: null } })
            .then((user) => {
            if (!user)
                throw new Error(LANGUAGE_1.LANGUAGE.EN.ACCOUNT_MANAGER.LOGIN_ERROR);
            this._req.session.user = user;
            return user;
        })
            .catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
}
exports.LoginManager = LoginManager;
//# sourceMappingURL=LoginManager.js.map