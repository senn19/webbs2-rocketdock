import {database} from "../../database/mongoDb";
import {Request} from "express";
import {MongoError} from "mongoDb";
import {COLLECTIONS} from "../../database/COLLECTIONS";
import {DatabaseError} from "../errorTypes/DatabaseError";
import {LANGUAGE} from "../../../language/LANGUAGE";
import {vsprintf} from "sprintf-js";

const SHA256 = require("crypto-js/sha256");

export class LoginManager {
  private readonly _email : string;
  private readonly _password : string;
  private readonly _req : Request;

  constructor(email: string, password: string, req : Request = null) {
    this._email = email;
    this._password = SHA256(password).toString();
    this._req = req;
  }

  /**
   * method to login a given user, creates a session with user and loggedIn status.
   * @return {Promise} returns user, if found
   */
  public loginUser() : Promise<any> {
    return database.collection(COLLECTIONS.userManager)
      .findOne({$and: [{_email: this._email}, {_password: this._password}]}, {projection:{_password: null}})
      .then((user : any) => {
        if(!user) throw new Error(LANGUAGE.EN.ACCOUNT_MANAGER.LOGIN_ERROR);
        this._req.session.user = user;
        return user;
      })
      .catch((error : MongoError) => {
        if (error instanceof Error)
          throw new Error(error.message);
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }
}
