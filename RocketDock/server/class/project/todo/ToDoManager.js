"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ToDo_1 = require("../../../../class/project/todo/ToDo");
const mongoDb_1 = require("../../../const/mongoDb");
const COLLECTIONS_1 = require("../../../const/COLLECTIONS");
const mongodb_1 = require("mongodb");
const DatabaseError_1 = require("../../errorTypes/DatabaseError");
class ToDoManager {
    constructor(todo) {
        this._todo = todo;
    }
    editTodo() {
        this._updateTodo = {};
        if (this._todo.getTodoContent())
            this._updateTodo._todoContent = this._todo.getTodoContent();
        if (this._todo.getStartDate())
            this._updateTodo._startDate = this._todo.getStartDate();
        if (this._todo.getEndDate())
            this._updateTodo._endDate = this._todo.getEndDate();
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.todoManager).updateOne({ _id: this._todo.getToDoID() }, { $set: this._updateTodo })
            .then((result) => {
            if (result.modifiedCount === 1) {
                return "ToDo Entry has been successfully updated.";
            }
            else {
                throw Error("No entries has been updated. Has anything changed?");
            }
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError("An Error has occured. Database Error Code: " + error);
        });
    }
    deleteToDo() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.todoManager).deleteOne({ _id: this._todo.getToDoID() })
            .then((result) => {
            if (result.deletedCount === 1) {
                return "ToDo Entry has been successfully deleted";
            }
            else {
                throw Error("ToDo Entry could not been deleted");
            }
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError("An Error has occured. Database Error Code: " + error);
        });
    }
    addTodo() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.todoManager).insertOne(this._todo)
            .then((result) => {
            if (result.insertedCount === 1) {
                return "ToDo Entry has been successfully inserted.";
            }
            else {
                throw Error("ToDo Entry could not been inserted.");
            }
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError("An Error has occured. Database Error Code: " + error);
        });
    }
    getTodoList() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.todoManager).find({ _projectID: this._todo.getProjectID() }).toArray()
            .then((response) => {
            let todoList = [];
            for (let item of response) {
                let todo = new ToDo_1.ToDo(new mongodb_1.ObjectID(item._projectID), new mongodb_1.ObjectID(item._authorID), item._todoContent, item._startDate, item._endDate);
                todo.setStartDate(item._startDate);
                todo.setEndDate(item._endDate);
                todo.setToDoID(item._id);
                todoList.push(todo);
            }
            return todoList;
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError("An Error has occured. Database Error Code: " + error);
        });
    }
}
exports.ToDoManager = ToDoManager;
//# sourceMappingURL=ToDoManager.js.map