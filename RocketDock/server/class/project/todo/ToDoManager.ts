import {ToDo} from "../../../../class/project/todo/ToDo";
import {database} from "../../../const/mongoDb";
import {COLLECTIONS} from "../../../const/COLLECTIONS";
import {DeleteWriteOpResultObject, InsertOneWriteOpResult, MongoError, ObjectID, UpdateWriteOpResult} from "mongodb";
import {DatabaseError} from "../../errorTypes/DatabaseError";

export class ToDoManager {
  private _todo : ToDo;
  private _updateTodo: any;

  constructor(todo: ToDo){
    this._todo = todo;
  }

  public editTodo() : Promise<string> {
    this._updateTodo = {};
    if(this._todo.getTodoContent()) this._updateTodo._todoContent = this._todo.getTodoContent();
    if(this._todo.getStartDate()) this._updateTodo._startDate = this._todo.getStartDate();
    if(this._todo.getEndDate()) this._updateTodo._endDate = this._todo.getEndDate();
    return database.collection(COLLECTIONS.todoManager).updateOne({_id: this._todo.getToDoID()}, {$set:this._updateTodo})
      .then((result: UpdateWriteOpResult) => {
        if(result.modifiedCount === 1) {
          return "ToDo Entry has been successfully updated.";
        } else {
          throw Error("No entries has been updated. Has anything changed?");
        }
      }).catch((error: MongoError) => {
        throw new DatabaseError("An Error has occured. Database Error Code: " +error);
      });
  }

  public deleteToDo(): Promise<string> {
    return database.collection(COLLECTIONS.todoManager).deleteOne({_id: this._todo.getToDoID()})
      .then((result: DeleteWriteOpResultObject) => {
        if (result.deletedCount === 1) {
          return "ToDo Entry has been successfully deleted";
        } else {
          throw Error("ToDo Entry could not been deleted");
        }
      }) .catch((error: MongoError) => {
        throw new DatabaseError("An Error has occured. Database Error Code: " +error)
      });
  }

  public addTodo() : Promise<string> {
    return database.collection(COLLECTIONS.todoManager).insertOne(this._todo)
      .then((result: InsertOneWriteOpResult)=> {
        if(result.insertedCount === 1) {
          return "ToDo Entry has been successfully inserted.";
        } else {
          throw Error("ToDo Entry could not been inserted.");
        }
      }).catch((error: MongoError) => {
        throw new DatabaseError("An Error has occured. Database Error Code: " +error);
      });
  }

  public getTodoList() : Promise<ToDo[]> {
    return database.collection(COLLECTIONS.todoManager).find({_projectID: this._todo.getProjectID()}).toArray()
      .then((response: any []) => {
        let todoList : ToDo[] = [];
        for(let item of response) {
          let todo : ToDo = new ToDo(
            new ObjectID(item._projectID),
            new ObjectID(item._authorID),
            item._todoContent,
            item._startDate,
            item._endDate
          );
          todo.setStartDate(item._startDate);
          todo.setEndDate(item._endDate);
          todo.setToDoID(item._id);
          todoList.push(todo);
        }
        return todoList;
      }).catch((error: MongoError) => {
        throw new DatabaseError("An Error has occured. Database Error Code: " + error);
      });
  }


}
