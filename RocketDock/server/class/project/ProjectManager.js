"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Project_1 = require("../../../class/project/Project");
const mongoDb_1 = require("../../database/mongoDb");
const COLLECTIONS_1 = require("../../database/COLLECTIONS");
const mongodb_1 = require("mongodb");
const DatabaseError_1 = require("../errorTypes/DatabaseError");
const LANGUAGE_1 = require("../../../language/LANGUAGE");
const sprintf_js_1 = require("sprintf-js");
class ProjectManager {
    constructor(project = null) {
        this._urlRegExp = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=]+$/;
        this._project = project;
    }
    validateURL(urls) {
        return urls.every((value) => { return this._urlRegExp.test(value); });
    }
    editProject() {
        this._updateProject = {};
        if (this._project.getProjectName())
            this._updateProject._projectName = this._project.getProjectName();
        if (this._project.getProjectDescription())
            this._updateProject._projectDescription = this._project.getProjectDescription();
        if (this._project.getProjectAuthor())
            this._updateProject._projectAuthor = this._project.getProjectAuthor();
        if (this._project.getProjectDetails())
            this._updateProject._projectDetails = this._project.getProjectDetails();
        this._updateProject._projectStatus = this._project.getProjectStatus();
        return Promise.resolve().then(() => {
            if (!this.validateURL(this._project.getProjectDetails()._projectURIs))
                throw Error(LANGUAGE_1.LANGUAGE.EN.PROJECT_MANAGER.URL_INVALID);
        }).then(() => {
            return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.projectManager).updateOne({ _id: this._project.getProjectID() }, { $set: this._updateProject });
        }).then((result) => {
            if (result.modifiedCount === 1)
                return sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.PROJECT_MANAGER.UPDATE, this._project.getProjectName());
            throw Error(sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.PROJECT_MANAGER.UPDATE_ERROR, this._project.getProjectName()));
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    deleteProject() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.projectManager).deleteOne({ _id: this._project.getProjectID() })
            .then((result) => {
            if (result.deletedCount === 1)
                return sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.PROJECT_MANAGER.DELETE, this._project.getProjectName());
            throw Error(sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.PROJECT_MANAGER.DELETE_ERROR, this._project.getProjectName()));
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    addProject() {
        return Promise.resolve().then(() => {
            if (!this.validateURL(this._project.getProjectDetails()._projectURIs))
                throw Error(LANGUAGE_1.LANGUAGE.EN.PROJECT_MANAGER.URL_INVALID);
        }).then(() => {
            return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.projectManager).insertOne(this._project);
        }).then((result) => {
            if (result.insertedCount === 1)
                return sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.PROJECT_MANAGER.ADD, this._project.getProjectName());
            throw Error(sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.PROJECT_MANAGER.ADD_ERROR, this._project.getProjectName()));
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    getProjectList() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.projectManager).find({}).toArray()
            .then((response) => {
            let projectList = [];
            for (let item of response) {
                let project = new Project_1.Project(item._projectName, item._projectDescription, new mongodb_1.ObjectID(item._projectAuthor), item._projectDetails, item._projectStatus);
                project.setProjectDate(item._date);
                project.setProjectID(item._id);
                projectList.push(project);
            }
            return projectList;
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    getProjectByData() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.projectManager).findOne({ _id: this._project.getProjectID() })
            .then((response) => {
            if (response) {
                let project = new Project_1.Project(response._projectName, response._projectDescription, new mongodb_1.ObjectID(response._projectAuthor), response._projectDetails, response._projectStatus);
                project.setProjectDate(response._projectDate);
                project.setProjectID(response._id);
                return project;
            }
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    getProjectByUser(userID) {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.projectManager).find({
            "_projectDetails._projectTeam._id": userID
        }).toArray()
            .then((response) => {
            let projectList = [];
            for (let item of response) {
                let project = new Project_1.Project(item._projectName, item._projectDescription, new mongodb_1.ObjectID(item._projectAuthor), item._projectDetails, item._projectStatus);
                project.setProjectDate(item._date);
                project.setProjectID(item._id);
                projectList.push(project);
            }
            return projectList;
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
}
exports.ProjectManager = ProjectManager;
//# sourceMappingURL=ProjectManager.js.map