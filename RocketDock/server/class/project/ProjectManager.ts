import {Project} from "../../../class/project/Project";
import {database} from "../../database/mongoDb";
import {COLLECTIONS} from "../../database/COLLECTIONS";
import {DeleteWriteOpResultObject, InsertOneWriteOpResult, MongoError, ObjectID, UpdateWriteOpResult} from "mongodb";
import {DatabaseError} from "../errorTypes/DatabaseError";
import {LANGUAGE} from "../../../language/LANGUAGE";
import {sprintf, vsprintf} from "sprintf-js";


export class ProjectManager {

  private _project : Project;
  private _updateProject : any;

  private _urlRegExp : RegExp =
    /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=]+$/;

  public validateURL(urls : string[]) : boolean {
    return urls.every((value) => {return this._urlRegExp.test(value)})
  }

  constructor(project : Project = null){
    this._project= project;
  }

  public editProject() : Promise<string> {
    this._updateProject = {};

    if(this._project.getProjectName())
      this._updateProject._projectName = this._project.getProjectName();
    if(this._project.getProjectDescription())
      this._updateProject._projectDescription = this._project.getProjectDescription();
    if(this._project.getProjectAuthor())
      this._updateProject._projectAuthor = this._project.getProjectAuthor();
    if(this._project.getProjectDetails())
      this._updateProject._projectDetails = this._project.getProjectDetails();
    this._updateProject._projectStatus= this._project.getProjectStatus();

    return Promise.resolve().then(() => {
      if(!this.validateURL(this._project.getProjectDetails()._projectURIs))
        throw Error(LANGUAGE.EN.PROJECT_MANAGER.URL_INVALID);
    }).then(() => {
      return database.collection(COLLECTIONS.projectManager).updateOne({_id : this._project.getProjectID()}, {$set:this._updateProject})
    }).then((result : UpdateWriteOpResult) => {
      if (result.modifiedCount === 1)
        return sprintf(LANGUAGE.EN.PROJECT_MANAGER.UPDATE, this._project.getProjectName());
      throw Error(sprintf(LANGUAGE.EN.PROJECT_MANAGER.UPDATE_ERROR, this._project.getProjectName()));
    }).catch((error : MongoError) => {
      if(error instanceof Error)
        throw new Error(error.message);
      throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
    });
  }


  public deleteProject():Promise<string> {
    return database.collection(COLLECTIONS.projectManager).deleteOne({_id: this._project.getProjectID()})
      .then((result: DeleteWriteOpResultObject) => {
        if (result.deletedCount === 1)
          return sprintf(LANGUAGE.EN.PROJECT_MANAGER.DELETE, this._project.getProjectName());
        throw Error(sprintf(LANGUAGE.EN.PROJECT_MANAGER.DELETE_ERROR, this._project.getProjectName()));
      }).catch((error: MongoError) => {
        if(error instanceof Error)
          throw new Error(error.message);
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any))
      });
  }


  public addProject() : Promise<string> {
    return Promise.resolve().then(() => {
      if(!this.validateURL(this._project.getProjectDetails()._projectURIs))
        throw Error(LANGUAGE.EN.PROJECT_MANAGER.URL_INVALID);
    }).then(() => {
      return database.collection(COLLECTIONS.projectManager).insertOne(this._project)
    }).then((result : InsertOneWriteOpResult) => {
      if(result.insertedCount === 1)
        return sprintf(LANGUAGE.EN.PROJECT_MANAGER.ADD, this._project.getProjectName());
      throw Error(sprintf(LANGUAGE.EN.PROJECT_MANAGER.ADD_ERROR, this._project.getProjectName()));
    }).catch((error : MongoError) => {
      if(error instanceof Error)
        throw new Error(error.message);
      throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
    });
  }


  public getProjectList() : Promise<Project[]> {
    return database.collection(COLLECTIONS.projectManager).find({}).toArray()
      .then((response : any[]) => {
        let projectList : Project[] = [];
        for(let item of response) {
          let project : Project = new Project(
            item._projectName,
            item._projectDescription,
            new ObjectID(item._projectAuthor),
            item._projectDetails,
            item._projectStatus
          );
          project.setProjectDate(item._date);
          project.setProjectID(item._id);
          projectList.push(project);
        }
        return projectList;
      }).catch((error : MongoError) => {
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }


  public getProjectByData() : Promise<Project> {
    return database.collection(COLLECTIONS.projectManager).findOne(
      {_id : this._project.getProjectID()})
      .then((response : any) => {
        if(response) {
          let project: Project = new Project(
            response._projectName,
            response._projectDescription,
            new ObjectID(response._projectAuthor),
            response._projectDetails,
            response._projectStatus);
          project.setProjectDate(response._projectDate);
          project.setProjectID(response._id);
          return project;
        }
      }).catch((error : MongoError) => {
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }


  public getProjectByUser(userID : ObjectID) : Promise<Project[]> {
    return database.collection(COLLECTIONS.projectManager).find({
      "_projectDetails._projectTeam._id": userID
    }).toArray()
      .then((response : any) => {
        let projectList : Project[] = [];
        for(let item of response) {
          let project : Project = new Project(
            item._projectName,
            item._projectDescription,
            new ObjectID(item._projectAuthor),
            item._projectDetails,
            item._projectStatus
          );
          project.setProjectDate(item._date);
          project.setProjectID(item._id);
          projectList.push(project);
        }
        return projectList;
      }).catch((error : MongoError) => {
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }
}
