import {ObjectID} from "bson";
import {database} from "../../../database/mongoDb";
import {COLLECTIONS} from "../../../database/COLLECTIONS";
import {DeleteWriteOpResultObject, InsertOneWriteOpResult, MongoError, UpdateWriteOpResult} from "mongodb";
import {DatabaseError} from "../../errorTypes/DatabaseError";
import {Comment} from "../../../../class/project/comments/Comment"
import {vsprintf} from "sprintf-js";
import {LANGUAGE} from "../../../../language/LANGUAGE";

export class CommentManager {
  private _comment: Comment;
  private _updateComment: any;

  constructor(comment: Comment = null) {
    this._comment = comment;
  }


  public editComment(): Promise<string> {
    this._updateComment = {};
    if (this._comment.getContent()) this._updateComment._commentID = this._comment.getCommentID();
    return database.collection(COLLECTIONS.commentManager).updateOne({_id: this._comment.getCommentID()}, {$set: this._updateComment})
      .then((result: UpdateWriteOpResult) => {
        if (result.modifiedCount === 1)
          return LANGUAGE.EN.COMMENT_MANAGER.UPDATE;
        throw Error(LANGUAGE.EN.COMMENT_MANAGER.UPDATE_ERROR);
      }).catch((error: MongoError) => {
        if(error instanceof Error)
          throw new Error(error.message);
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }

  /**
   * method to add an comment to database
   * @return {Promise<String>} returns an error or a message
   */
  public addComment(): Promise<string> {
    return database.collection(COLLECTIONS.commentManager).insertOne(this._comment)
      .then((result: InsertOneWriteOpResult) => {
        if (result.insertedCount === 1)
          return LANGUAGE.EN.COMMENT_MANAGER.ADD;
        throw Error(LANGUAGE.EN.COMMENT_MANAGER.ADD_ERROR);
      }).catch((error: MongoError) => {
        if(error instanceof Error)
          throw new Error(error.message);
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }

  /**
   * method to delete an comment from database
   * @return {Promise<String>} returns an error or a message
   */
  public deleteComment(): Promise<string> {
    return database.collection(COLLECTIONS.commentManager).deleteOne({_id: this._comment.getCommentID()})
      .then((result: DeleteWriteOpResultObject) => {
        if (result.deletedCount === 1)
          return LANGUAGE.EN.COMMENT_MANAGER.DELETE;
        throw Error(LANGUAGE.EN.COMMENT_MANAGER.DELETE_ERROR);
      }).catch((error: MongoError) => {
        if(error instanceof Error)
          throw new Error(error.message);
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any))
      });
  }

  /**
   * method to get all comment from database
   * @return {Promise<Comment[]>} returns comment[]
   */
  public getCommentList(id : ObjectID): Promise<Comment[]> {
    return database.collection(COLLECTIONS.commentManager).find({_projectID : id}).toArray()
      .then((response: any[]) => {
        let commentList: Comment[] = [];
        for (let item of response) {
          let comment: Comment = new Comment(
            new ObjectID(item._projectID),
            new ObjectID(item._authorID),
            item._content);
          comment.setCommentID(item._id);
          comment.setDate(item._date);
          commentList.push(comment);
        }
        return commentList;
      }).catch((error: MongoError) => {
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }
}
