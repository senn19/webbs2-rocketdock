"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bson_1 = require("bson");
const mongoDb_1 = require("../../../database/mongoDb");
const COLLECTIONS_1 = require("../../../database/COLLECTIONS");
const DatabaseError_1 = require("../../errorTypes/DatabaseError");
const Comment_1 = require("../../../../class/project/comments/Comment");
const sprintf_js_1 = require("sprintf-js");
const LANGUAGE_1 = require("../../../../language/LANGUAGE");
class CommentManager {
    constructor(comment = null) {
        this._comment = comment;
    }
    editComment() {
        this._updateComment = {};
        if (this._comment.getContent())
            this._updateComment._commentID = this._comment.getCommentID();
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.commentManager).updateOne({ _id: this._comment.getCommentID() }, { $set: this._updateComment })
            .then((result) => {
            if (result.modifiedCount === 1)
                return LANGUAGE_1.LANGUAGE.EN.COMMENT_MANAGER.UPDATE;
            throw Error(LANGUAGE_1.LANGUAGE.EN.COMMENT_MANAGER.UPDATE_ERROR);
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    /**
     * method to add an comment to database
     * @return {Promise<String>} returns an error or a message
     */
    addComment() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.commentManager).insertOne(this._comment)
            .then((result) => {
            if (result.insertedCount === 1)
                return LANGUAGE_1.LANGUAGE.EN.COMMENT_MANAGER.ADD;
            throw Error(LANGUAGE_1.LANGUAGE.EN.COMMENT_MANAGER.ADD_ERROR);
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    /**
     * method to delete an comment from database
     * @return {Promise<String>} returns an error or a message
     */
    deleteComment() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.commentManager).deleteOne({ _id: this._comment.getCommentID() })
            .then((result) => {
            if (result.deletedCount === 1)
                return LANGUAGE_1.LANGUAGE.EN.COMMENT_MANAGER.DELETE;
            throw Error(LANGUAGE_1.LANGUAGE.EN.COMMENT_MANAGER.DELETE_ERROR);
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    /**
     * method to get all comment from database
     * @return {Promise<Comment[]>} returns comment[]
     */
    getCommentList(id) {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.commentManager).find({ _projectID: id }).toArray()
            .then((response) => {
            let commentList = [];
            for (let item of response) {
                let comment = new Comment_1.Comment(new bson_1.ObjectID(item._projectID), new bson_1.ObjectID(item._authorID), item._content);
                comment.setCommentID(item._id);
                comment.setDate(item._date);
                commentList.push(comment);
            }
            return commentList;
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
}
exports.CommentManager = CommentManager;
//# sourceMappingURL=CommentManager.js.map