const GoogleImages = require('google-images');

export class IconManager {

  private _client = new GoogleImages("002392352099506185508:cusf9wxxy3a", "AIzaSyBlOCvSYb8v2nGBXBMB8_vAOTNHWvBCScQ");
  private _searchString : string;

  constructor(searchString : string) {
    this._searchString = searchString;
  }

  public iconSearch() : Promise<string> {
    return this._client.search(this._searchString).then((images : any) => {
        return images[0].url;
      }).catch((error : any) => {});
  }
}
