export class DatabaseError extends Error {

  constructor(e: string) {
    super(e);
  }
}
