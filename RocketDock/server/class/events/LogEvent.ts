import {Log} from "../../../class/events/Log";
import {database} from "../../database/mongoDb";
import {MongoError} from "mongoDb";
import {log} from "../../utils/logger/ConsoleLogger";

const logCollections = {
  collection : "logEvents",
};

export class LogEvent {
  constructor(eventLog : Log) {
    database.collection(logCollections.collection).insertOne(eventLog)
      .catch((error : MongoError) => {
      log.error(error);
    });
  }
}
