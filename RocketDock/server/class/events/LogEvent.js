"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoDb_1 = require("../../database/mongoDb");
const ConsoleLogger_1 = require("../../utils/logger/ConsoleLogger");
const logCollections = {
    collection: "logEvents",
};
class LogEvent {
    constructor(eventLog) {
        mongoDb_1.database.collection(logCollections.collection).insertOne(eventLog)
            .catch((error) => {
            ConsoleLogger_1.log.error(error);
        });
    }
}
exports.LogEvent = LogEvent;
//# sourceMappingURL=LogEvent.js.map