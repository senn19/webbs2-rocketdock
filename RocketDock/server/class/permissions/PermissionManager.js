"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const COLLECTIONS_1 = require("../../database/COLLECTIONS");
const PermissionGroup_1 = require("../../../class/perm/PermissionGroup");
const mongoDb_1 = require("../../database/mongoDb");
const DatabaseError_1 = require("../errorTypes/DatabaseError");
const sprintf_js_1 = require("sprintf-js");
const LANGUAGE_1 = require("../../../language/LANGUAGE");
class PermissionManager {
    constructor(permGroup = null) {
        this._permissionGroup = permGroup;
    }
    /**
     * this method creates a database connection and returns the permission group of the given groupID
     * @return {Promise<PermissionGroup>} returned
     */
    getPermissionGroup() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.permManager).findOne({ _id: this._permissionGroup.getGroupID() })
            .then((permissionGroup) => {
            return permissionGroup;
        })
            .catch((error) => {
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    /**
     * method to add a group to database
     * @return {Promise<String>} returns an error or a message
     */
    createPermissionGroup() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.permManager).insertOne(this._permissionGroup)
            .then((result) => {
            if (result.insertedCount === 1)
                return sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.GROUP_MANAGER.ADD, this._permissionGroup.getGroupName());
            throw Error(sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.GROUP_MANAGER.ADD_ERROR, this._permissionGroup.getGroupName()));
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    /**
     * method to delete a group from database
     * @return {Promise<String>} returns an error or a message
     */
    deletePermissionGroup() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.permManager).deleteOne({ _id: this._permissionGroup.getGroupID() })
            .then((result) => {
            if (result.deletedCount === 1)
                return sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.GROUP_MANAGER.DELETE, this._permissionGroup.getGroupName());
            throw Error(sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.GROUP_MANAGER.DELETE_ERROR, this._permissionGroup.getGroupName()));
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    /**
     * method to get all user from database
     * @return {Promise<User[]>} returns user[]
     */
    getPermissionGroups() {
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.permManager).find().toArray()
            .then((response) => {
            let permGroupList = [];
            for (let item of response) {
                let permGroup = new PermissionGroup_1.PermissionGroup(item._groupName, item._permissions);
                permGroup.setGroupID(item._id);
                permGroupList.push(permGroup);
            }
            return permGroupList;
        }).catch((error) => {
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
    /**
     * method to edit a group in database
     * @return {Promise<String>} throws an error or a message
     */
    editPermissionGroup() {
        this._updatePermGroup = {};
        if (this._permissionGroup.getGroupName())
            this._updatePermGroup._groupName = this._permissionGroup.getGroupName();
        if (this._permissionGroup.getPermissions())
            this._updatePermGroup._permissions = this._permissionGroup.getPermissions();
        return mongoDb_1.database.collection(COLLECTIONS_1.COLLECTIONS.permManager).updateOne({ _id: this._permissionGroup.getGroupID() }, { $set: this._updatePermGroup })
            .then((result) => {
            if (result.modifiedCount === 1)
                return sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.GROUP_MANAGER.UPDATE, this._permissionGroup.getGroupName());
            throw Error(sprintf_js_1.sprintf(LANGUAGE_1.LANGUAGE.EN.GROUP_MANAGER.UPDATE_ERROR, this._permissionGroup.getGroupName()));
        }).catch((error) => {
            if (error instanceof Error)
                throw new Error(error.message);
            throw new DatabaseError_1.DatabaseError(sprintf_js_1.vsprintf(LANGUAGE_1.LANGUAGE.EN.DATABASE.ERROR, error));
        });
    }
}
exports.PermissionManager = PermissionManager;
//# sourceMappingURL=PermissionManager.js.map