import {COLLECTIONS} from "../../database/COLLECTIONS";
import {PermissionGroup} from "../../../class/perm/PermissionGroup";
import {database} from "../../database/mongoDb";
import {DeleteWriteOpResultObject, InsertOneWriteOpResult, MongoError, UpdateWriteOpResult} from "mongoDb";
import {DatabaseError} from "../errorTypes/DatabaseError";
import {sprintf, vsprintf} from "sprintf-js";
import {LANGUAGE} from "../../../language/LANGUAGE";

export class PermissionManager {

  private _permissionGroup : PermissionGroup;
  private _updatePermGroup : any;

  constructor(permGroup : PermissionGroup = null) {
    this._permissionGroup = permGroup;
  }

  /**
   * this method creates a database connection and returns the permission group of the given groupID
   * @return {Promise<PermissionGroup>} returned
   */
  public getPermissionGroup() : Promise<PermissionGroup> {
    return database.collection(COLLECTIONS.permManager).findOne({_id : this._permissionGroup.getGroupID()})
      .then((permissionGroup : PermissionGroup) => {
        return permissionGroup;
      })
      .catch((error : MongoError) => {
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      })
  }


  /**
   * method to add a group to database
   * @return {Promise<String>} returns an error or a message
   */
  public createPermissionGroup() : Promise<string> {
    return database.collection(COLLECTIONS.permManager).insertOne(this._permissionGroup)
      .then((result : InsertOneWriteOpResult) => {
        if(result.insertedCount === 1)
          return sprintf(LANGUAGE.EN.GROUP_MANAGER.ADD, this._permissionGroup.getGroupName());
        throw Error(sprintf(LANGUAGE.EN.GROUP_MANAGER.ADD_ERROR, this._permissionGroup.getGroupName()));
      }).catch((error : MongoError) => {
        if(error instanceof Error)
          throw new Error(error.message);
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }

  /**
   * method to delete a group from database
   * @return {Promise<String>} returns an error or a message
   */
  public deletePermissionGroup() : Promise<string> {
    return database.collection(COLLECTIONS.permManager).deleteOne({_id : this._permissionGroup.getGroupID()})
      .then((result : DeleteWriteOpResultObject) => {
        if(result.deletedCount === 1)
          return sprintf(LANGUAGE.EN.GROUP_MANAGER.DELETE, this._permissionGroup.getGroupName());
        throw Error(sprintf(LANGUAGE.EN.GROUP_MANAGER.DELETE_ERROR, this._permissionGroup.getGroupName()));
      }).catch((error : MongoError) => {
        if(error instanceof Error)
          throw new Error(error.message);
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any))
      });
  }

  /**
   * method to get all user from database
   * @return {Promise<User[]>} returns user[]
   */
  public getPermissionGroups() : Promise<PermissionGroup[]> {
    return database.collection(COLLECTIONS.permManager).find().toArray()
      .then((response : any[]) => {
        let permGroupList : PermissionGroup[] = [];
        for(let item of response) {
          let permGroup : PermissionGroup = new PermissionGroup(
            item._groupName,
            item._permissions
          );
          permGroup.setGroupID(item._id);
          permGroupList.push(permGroup);
        }
        return permGroupList;
      }).catch((error : MongoError) => {
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }

  /**
   * method to edit a group in database
   * @return {Promise<String>} throws an error or a message
   */
  public editPermissionGroup() : Promise<string> {
    this._updatePermGroup = {};
    if(this._permissionGroup.getGroupName()) this._updatePermGroup._groupName = this._permissionGroup.getGroupName();
    if(this._permissionGroup.getPermissions()) this._updatePermGroup._permissions = this._permissionGroup.getPermissions();
    return database.collection(COLLECTIONS.permManager).updateOne({_id : this._permissionGroup.getGroupID()}, {$set:this._updatePermGroup})
      .then((result : UpdateWriteOpResult) => {
        if(result.modifiedCount === 1)
          return sprintf(LANGUAGE.EN.GROUP_MANAGER.UPDATE, this._permissionGroup.getGroupName());
        throw Error(sprintf(LANGUAGE.EN.GROUP_MANAGER.UPDATE_ERROR, this._permissionGroup.getGroupName()));
      }).catch((error : MongoError) => {
        if(error instanceof Error)
          throw new Error(error.message);
        throw new DatabaseError(vsprintf(LANGUAGE.EN.DATABASE.ERROR, error as any));
      });
  }
}
