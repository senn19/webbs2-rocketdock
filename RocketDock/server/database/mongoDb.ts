import {Db, MongoClient, MongoError} from "mongodb";
import {INSTALL_COLLECTION} from "./COLLECTIONS";
import {log} from "../utils/logger/ConsoleLogger";

let database : Db;

/**
 * Method to create connection to mongo database and store this connection in var:database.
 * On connection build the database will be checked on missing collections,
 * if collections are missing, they will be added through createCollection()
 */
MongoClient.connect("mongodb://localhost:27017/rocketDock", { useNewUrlParser: true })
  .then((dbClient : MongoClient) => {
    database = dbClient.db("rocketDock");
    log.info("Database has been started. Everything is fine. :) <3\n" +
      "Check if tables have already been created.");
    listCollections().then((collections : any) => {
      if(collections.length > 0) {
        log.warn("Collections missing. Creating Tables...");
        createCollections(collections);
      } else {
        log.success("No collections are missing. Everything is fine. :)");
      }
    });
  }).catch((error : MongoError) => {
  log.error("An Error has occurred. Database could not be started. :( </3 \n Error: "+error);
});

/**
 * create Collections in database on already connected mongoDB
 * @param {any[]} collections
 */
function createCollections(collections : any[]) : void {
  for(let collection of collections) {
    log.warn("Collection >>" + collection.name + "<< missing. Creating collection.");
    database.createCollection(collection.name).then(() => {
      database.collection(collection.name).insertOne(collection.default);
      log.success("Collection has been created.");
    });
  }
}

/**
 * lists all collection of database and gets all missing collections compared to
 * local list of collection with default data. const: MONGODB
 * @return {Promise<any[]>} returns MissingCollections
 */
function listCollections() : Promise<any[]> {
  return database.collections().then((collections : any) => {
    let missingCollections : string[] = [], currentCollection : any;

    for(let localCollection of INSTALL_COLLECTION) {
      currentCollection = null;

      if(collections.length > 0) {
        for(let dbCollection of collections) {
          if(localCollection.name == dbCollection.namespace.split(".")[1]) {
            currentCollection = null;
            break;
          }
          currentCollection = localCollection;
        }
      } else {
        currentCollection = localCollection;
      }

      if(currentCollection) missingCollections.push(currentCollection);
    }
    return missingCollections;
  });
}

export {database};
