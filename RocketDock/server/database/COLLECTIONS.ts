import {ObjectID} from "bson";

const SHA256 = require("crypto-js/sha256");
const O_ID = new ObjectID();

const INSTALL_COLLECTION = [
  { name: "userList",
    default : {
      _id : O_ID,
      _username: "admin",
      _password: SHA256("admin").toString(),
      _email: "admin@admin.de",
      _date: new Date().toLocaleString(),
      _userData : {
        _status : false,
        _foreName : "Admin",
        _lastName : "Admin",
        _sex : "Male",
        _description : "admin loves trees",
        _avatar : "/assets/avatar/admin.png",
        _group : O_ID
      },
      _userVerified : {
        _facebookID : "null",
      }
    }},
  { name: "permGroups",
    default : {
      _id : O_ID,
      _groupName : "Admin",
      _permissions : {
        _isAdmin : true,
        _isProjectManager : false,
        _isMember : false,
      }
    }},
  { name: "logEvents",
    default : {
      _userID : O_ID,
      _groupID : O_ID,
      _username : "mongoDB",
      _action : "has successfully installed all the necessary entries.",
      _date : new Date().toLocaleString(),
      _type : "mongoDB"
    }},
];

const COLLECTIONS = {
  userManager : "userList",
  eventManager : "logEvents",
  permManager : "permGroups",
  projectManager : "projects",
  commentManager : "comments",
  todoManager : "todo",
  contactManager:"contact",
};

export {INSTALL_COLLECTION, COLLECTIONS};
