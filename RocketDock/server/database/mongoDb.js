"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
const COLLECTIONS_1 = require("./COLLECTIONS");
const ConsoleLogger_1 = require("../utils/logger/ConsoleLogger");
let database;
exports.database = database;
/**
 * Method to create connection to mongo database and store this connection in var:database.
 * On connection build the database will be checked on missing collections,
 * if collections are missing, they will be added through createCollection()
 */
mongodb_1.MongoClient.connect("mongodb://localhost:27017/rocketDock", { useNewUrlParser: true })
    .then((dbClient) => {
    exports.database = database = dbClient.db("rocketDock");
    ConsoleLogger_1.log.info("Database has been started. Everything is fine. :) <3\n" +
        "Check if tables have already been created.");
    listCollections().then((collections) => {
        if (collections.length > 0) {
            ConsoleLogger_1.log.warn("Collections missing. Creating Tables...");
            createCollections(collections);
        }
        else {
            ConsoleLogger_1.log.success("No collections are missing. Everything is fine. :)");
        }
    });
}).catch((error) => {
    ConsoleLogger_1.log.error("An Error has occurred. Database could not be started. :( </3 \n Error: " + error);
});
/**
 * create Collections in database on already connected mongoDB
 * @param {any[]} collections
 */
function createCollections(collections) {
    for (let collection of collections) {
        ConsoleLogger_1.log.warn("Collection >>" + collection.name + "<< missing. Creating collection.");
        database.createCollection(collection.name).then(() => {
            database.collection(collection.name).insertOne(collection.default);
            ConsoleLogger_1.log.success("Collection has been created.");
        });
    }
}
/**
 * lists all collection of database and gets all missing collections compared to
 * local list of collection with default data. const: MONGODB
 * @return {Promise<any[]>} returns MissingCollections
 */
function listCollections() {
    return database.collections().then((collections) => {
        let missingCollections = [], currentCollection;
        for (let localCollection of COLLECTIONS_1.INSTALL_COLLECTION) {
            currentCollection = null;
            if (collections.length > 0) {
                for (let dbCollection of collections) {
                    if (localCollection.name == dbCollection.namespace.split(".")[1]) {
                        currentCollection = null;
                        break;
                    }
                    currentCollection = localCollection;
                }
            }
            else {
                currentCollection = localCollection;
            }
            if (currentCollection)
                missingCollections.push(currentCollection);
        }
        return missingCollections;
    });
}
//# sourceMappingURL=mongoDb.js.map