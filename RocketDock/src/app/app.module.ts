import {BrowserModule} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from '@angular/common/http';
import {ChartsModule} from 'ng2-charts';
import {AppComponent} from './app.component';
import {UserComponent} from './acp/acp-user-manager/user-list/user/user.component';
import {UserListComponent} from './acp/acp-user-manager/user-list/user-list.component';
import {UserManagerService} from "./services/userManager/user-manager.service";
import {UserAlertComponent} from './alertBox/user-alert.component';
import {UserFormComponent} from './acp/acp-user-manager/user-list/user-form/user-form.component';
import {AlertManagerService} from "./services/alertManager/alert-manager.service";
import {UserModalComponent} from './acp/acp-user-manager/user-list/user-modal/user-modal.component';
import {LoginFormComponent} from './login/login-form/login-form.component';
import {AccountManagerService} from "./services/accountManager/account-manager.service";
import {LogoutComponent} from "./login/logout/logout.component";
import {AdminControlPanelComponent} from './acp/admin-control-panel/admin-control-panel.component';
import {UserControlPanelComponent} from './ucp/user-control-panel/user-control-panel.component';
import {LoginComponent} from './login/login.component';
import {NavMenuComponent} from './ucp/nav-menu/nav-menu.component';
import {MemberListComponent} from './ucp/member-list/member-list.component';
import {FooterMenuComponent} from './ucp/footer-menu/footer-menu.component';
import {HomeComponent} from './ucp/home/home.component';
import {MemberComponent} from './ucp/member-list/member/member.component';
import {MemberCardComponent} from './ucp/member-list/member/member-card/member-card.component';
import {AcpNavMenuComponent} from './acp/acp-nav-menu/acp-nav-menu.component';
import {AcpSideNavComponent} from './acp/acp-side-nav/acp-side-nav.component';
import {AcpHomeComponent} from './acp/acp-home/acp-home.component';
import {UserEditComponent} from './acp/acp-user-manager/user-edit/user-edit.component';
import {AcpUserManagerComponent} from './acp/acp-user-manager/acp-user-manager.component';
import {ProjectsComponent} from './ucp/projects/projects.component';
import {ChartComponent} from './utils/chart/chart.component';
import {ProjectsDoneComponent} from './ucp/projects/projects-done/projects-done.component';
import {ProjectsOpenComponent} from './ucp/projects/projects-open/projects-open.component';
import {ProjectsDetailComponent} from './ucp/projects/projects-detail/projects-detail.component';
import {AcpGroupManagerComponent} from './acp/acp-group-manager/acp-group-manager.component';
import {AcpProjectManagerComponent} from './acp/acp-project-manager/acp-project-manager.component';
import {AcpSettingsComponent} from './acp/acp-settings/acp-settings.component';
import {AcpLogsComponent} from './acp/acp-logs/acp-logs.component';
import {PermManagerService} from "./services/permManager/perm-manager.service";
import {AcpGroupFormComponent} from './acp/acp-group-manager/acp-group-form/acp-group-form.component';
import {AcpGroupModalComponent} from './acp/acp-group-manager/acp-group-modal/acp-group-modal.component';
import {ProjectManagerService} from "./services/projectManager/project-manager.service";
import {IDtoUserNamePipe} from './pipes/IDtoUserName/idto-user-name.pipe';
import {AcpAreYouSureComponent} from './utils/modals/acp-are-you-sure/acp-are-you-sure.component';
import {AcpProjectEditComponent} from './acp/acp-project-manager/acp-project-edit/acp-project-edit.component';
import {AcpProjectListComponent} from './acp/acp-project-manager/acp-project-list/acp-project-list.component';
import {BooleanToStatusPipe} from './pipes/BooleanToStatus/boolean-to-status.pipe';
import {AcpProjectAddComponent} from './acp/acp-project-manager/acp-project-add/acp-project-add.component';
import {CommentManagerService} from "./services/commentManager/comment-manager.service";
import {BooleanToAccessLevelPipe} from './pipes/BooleanToAccessLevel/boolean-to-access-level.pipe';
import {IDtoGroupNamePipe} from './pipes/IDtoGroupName/idto-group-name.pipe';
import {WebpageToNamePipe} from './pipes/webpageToName/webpage-to-name.pipe';
import {ProjectsDetailTodosComponent} from "./ucp/projects/projects-detail/projects-detail-todos/projects-detail-todos.component";
import {AboutusComponent} from './ucp/aboutus/aboutus.component';
import {DisclaimerComponent} from './ucp/disclaimer/disclaimer.component';
import {UserProfileEditComponent } from './ucp/user-profile-edit/user-profile-edit.component';
import {RegisterComponent } from './login/register/register.component';
import {RecaptchaModule} from "ng-recaptcha";
import {ImageFinderService} from "./services/imageFinder/image-finder.service";
import {WebpageToImagePipe } from './pipes/webpageToImage/webpage-to-image.pipe';

const appRoutes: Routes = [
  {
    path: 'ucp', component: UserControlPanelComponent, children: [
      {path: '', component: HomeComponent},
      {path: 'userList', component: MemberListComponent},
      {path: 'userList/:id', component: MemberListComponent},
      {
        path: 'projects', component: ProjectsComponent, children: [
          {path: '', component: ProjectsOpenComponent},
          {path: 'done', component: ProjectsDoneComponent},
          {path: 'create', component: AcpProjectAddComponent},
          {
            path: 'details/:id', component: ProjectsDetailComponent, children: [
              {path: 'todos', component: ProjectsDetailTodosComponent}
            ]
          },
          {path: 'edit/:id', component: AcpProjectEditComponent}
        ]
      },
      {path: 'disclaimer', component: DisclaimerComponent},
      {path: 'aboutus', component: AboutusComponent},
      {path: 'profile/:id', component: UserProfileEditComponent},
    ]
  },
  {
    path: 'acp', component: AdminControlPanelComponent, children: [
      {path: '', component: AcpHomeComponent},
      {
        path: 'userList', component: AcpUserManagerComponent, children: [
          {path: '', component: UserListComponent},
          {path: 'edit/:id', component: UserEditComponent},
        ]
      },
      {path: 'groupList', component: AcpGroupManagerComponent},
      {path: 'settings', component: AcpSettingsComponent},
      {
        path: 'projectList', component: AcpProjectManagerComponent, children: [
          {path: '', component: AcpProjectListComponent},
          {path: 'add', component: AcpProjectAddComponent},
          {path: 'edit/:id', component: AcpProjectEditComponent},
        ]
      },
      {path: 'changeLogs', component: AcpLogsComponent}
    ]
  },
  {path: 'login', component: LoginComponent, children : [
      {path: '', component: LoginFormComponent},
      {path: 'create', component: RegisterComponent}
    ]},
];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UserListComponent,
    UserAlertComponent,
    UserFormComponent,
    UserModalComponent,
    LoginFormComponent,
    LogoutComponent,
    AdminControlPanelComponent,
    UserControlPanelComponent,
    LoginComponent,
    NavMenuComponent,
    MemberListComponent,
    FooterMenuComponent,
    HomeComponent,
    MemberComponent,
    MemberCardComponent,
    AcpNavMenuComponent,
    AcpSideNavComponent,
    AcpHomeComponent,
    UserEditComponent,
    AcpUserManagerComponent,
    ProjectsComponent,
    ChartComponent,
    ProjectsDoneComponent,
    ProjectsOpenComponent,
    ProjectsDetailComponent,
    AcpGroupManagerComponent,
    AcpProjectManagerComponent,
    AcpSettingsComponent,
    AcpLogsComponent,
    AcpGroupFormComponent,
    AcpGroupModalComponent,
    IDtoUserNamePipe,
    AcpAreYouSureComponent,
    AcpProjectEditComponent,
    AcpProjectListComponent,
    BooleanToStatusPipe,
    AcpProjectAddComponent,
    BooleanToAccessLevelPipe,
    IDtoGroupNamePipe,
    WebpageToNamePipe,
    ProjectsDetailTodosComponent,
    AboutusComponent,
    DisclaimerComponent,
    UserProfileEditComponent,
    RegisterComponent,
    WebpageToImagePipe,
  ],
  imports: [
    NgbModule.forRoot(),
    FormsModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    ChartsModule,
    RecaptchaModule
  ],
  providers: [UserManagerService, AlertManagerService, AccountManagerService, PermManagerService, ProjectManagerService,
    CommentManagerService, ImageFinderService],
  bootstrap: [AppComponent],
  entryComponents: [
    UserModalComponent,
    AcpGroupModalComponent,
    AcpAreYouSureComponent
  ]
})
export class AppModule {
}
