import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpLogsComponent } from './acp-logs.component';

describe('AcpLogsComponent', () => {
  let component: AcpLogsComponent;
  let fixture: ComponentFixture<AcpLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpLogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
