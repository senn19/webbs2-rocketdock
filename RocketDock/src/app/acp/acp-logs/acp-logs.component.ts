import { Component, OnInit } from '@angular/core';
import {EventManagerService} from "../../services/eventManager/event-manager.service";
import {Log} from "../../../../class/events/Log";
import {PermManagerService} from "../../services/permManager/perm-manager.service";
import {PermissionGroup} from "../../../../class/perm/PermissionGroup";

@Component({
  selector: 'app-acp-logs',
  templateUrl: './acp-logs.component.html',
  styleUrls: ['./acp-logs.component.scss']
})
export class AcpLogsComponent implements OnInit {

  public eventLogs : Log[] = [];
  public fullLogs : Log[] = [];
  public showLogs : Log[] = [];

  public showCount : number = 0;
  public maxShowCount : number = 15;

  public f_username : string;
  public f_word : string;
  public f_date : string;
  public f_group : PermissionGroup = null;
  public f_groups : PermissionGroup[];

  constructor(public eventService : EventManagerService, public permService : PermManagerService) { }

  ngOnInit() {
    this.eventService.getEventList()
      .then((eventLogs : Log[]) => {
        this.fullLogs = eventLogs;
        this.eventLogs = eventLogs;
        this.showLogs = this.eventLogs.slice(0, this.maxShowCount);
      });
    this.permService.getGroupList().then((groupList : PermissionGroup[]) => {
      this.f_groups = groupList;
    });
  }

  public onForward() : void {
    if(this.showCount + this.maxShowCount < this.eventLogs.length) {
      this.showCount = this.showCount + this.maxShowCount;
      this.showLogs = this.eventLogs.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }

  public onBackward() : void {
    if(this.showCount > 0) {
      this.showCount = this.showCount - this.maxShowCount;
      this.showLogs = this.eventLogs.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }

  public filterByUserName() : void {
    this.eventLogs = this.fullLogs;
    this.eventLogs = this.eventLogs.filter((log : Log) => log._username.toLowerCase().includes(this.f_username.toLowerCase()));
    this.showLogs = this.eventLogs.slice(0, this.maxShowCount);
  }

  public filterByWords() : void {
    this.eventLogs = this.fullLogs;
    this.eventLogs = this.eventLogs.filter((log : Log) => log._username.toLowerCase().includes(this.f_word) || log._action.includes(this.f_word.toLowerCase()));
    this.showLogs = this.eventLogs.slice(0, this.maxShowCount);
  }

  public filterByDate() : void {
    this.eventLogs = this.fullLogs;
    this.eventLogs = this.eventLogs.filter((log : Log) =>
      log._date.substr(0, log._date.length - 9).includes(this.f_date.replace(/-0+/g, '-')));
    this.showLogs = this.eventLogs.slice(0, this.maxShowCount);
  }

  public filterByGroup() : void {
    this.eventLogs = this.fullLogs;
    if(this.f_group) this.eventLogs = this.eventLogs.filter((log : Log) => log._groupID == this.f_group.getGroupID());
    this.showLogs = this.eventLogs.slice(0, this.maxShowCount);
  }

  public resetFilters() : void {
    this.eventLogs = this.fullLogs;
    this.showLogs = this.eventLogs.slice(0, this.maxShowCount);
  }
}
