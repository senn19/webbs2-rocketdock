import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-acp-nav-menu',
  templateUrl: './acp-nav-menu.component.html',
  styleUrls: ['./acp-nav-menu.component.scss']
})
export class AcpNavMenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public openSideNav() : void {
    document.querySelector("#sidebar").classList.toggle("active");
  }

}
