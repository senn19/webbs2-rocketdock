import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpNavMenuComponent } from './acp-nav-menu.component';

describe('AcpNavMenuComponent', () => {
  let component: AcpNavMenuComponent;
  let fixture: ComponentFixture<AcpNavMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpNavMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpNavMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
