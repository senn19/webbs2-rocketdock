import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpSideNavComponent } from './acp-side-nav.component';

describe('AcpSideNavComponent', () => {
  let component: AcpSideNavComponent;
  let fixture: ComponentFixture<AcpSideNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpSideNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpSideNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
