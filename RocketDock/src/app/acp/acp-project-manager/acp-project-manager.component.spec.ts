import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpProjectManagerComponent } from './acp-project-manager.component';

describe('AcpProjectManagerComponent', () => {
  let component: AcpProjectManagerComponent;
  let fixture: ComponentFixture<AcpProjectManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpProjectManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpProjectManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
