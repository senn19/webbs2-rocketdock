import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpProjectAddComponent } from './acp-project-add.component';

describe('AcpProjectAddComponent', () => {
  let component: AcpProjectAddComponent;
  let fixture: ComponentFixture<AcpProjectAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpProjectAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpProjectAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
