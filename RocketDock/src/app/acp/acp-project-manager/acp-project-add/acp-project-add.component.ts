import { Component, OnInit } from '@angular/core';
import {Project} from "../../../../../class/project/Project";
import {ProjectDetails} from "../../../../../class/project/ProjectDetails";
import {User} from "../../../../../class/user/User";
import {UserManagerService} from "../../../services/userManager/user-manager.service";
import {ProjectManagerService} from "../../../services/projectManager/project-manager.service";
import {ObjectID} from "bson";
import {AlertManagerService} from "../../../services/alertManager/alert-manager.service";
import {Location} from "@angular/common";
import {LANGUAGE} from "../../../../../language/LANGUAGE";

@Component({
  selector: 'app-acp-project-add',
  templateUrl: './acp-project-add.component.html',
  styleUrls: ['./acp-project-add.component.scss']
})
export class AcpProjectAddComponent implements OnInit {

  public project : Project = new Project();
  public userList : User[] = [];
  public displayList : User[] = [];
  public teamList : User[] = [];
  public filter : string;

  public LANGUAGE = LANGUAGE;
  public urlList : Array<{ value : string}> = [{ value : "http://"}];

  constructor(public usmService : UserManagerService, public projectService : ProjectManagerService, public alertService : AlertManagerService,
              public _location: Location) { }

  ngOnInit() {
    this.project.setProjectStatus(true);
    this.project.setProjectDetails(new ProjectDetails(null, null, null, false));
    this.usmService.getUserList().then((userList : User[]) => {
      this.userList = userList;
    });
  }

  // Urls

  public addUrlInput() {
    this.urlList.push({ value : "http://"});
  }

  public removeUrlInput(index : number) {
    this.urlList.splice(index, 1);
  }


  // TEAM MEMBER

  public filterByWord() : void {
    this.displayList = [];
    if(this.filter.length >= 3) {
      this.displayList = this.userList.filter((user : User) => user.getUsername().toLowerCase().includes(this.filter.toLowerCase())
        || (user.getUserData() && user.getUserData()._foreName ? user.getUserData()._foreName.toLowerCase().includes(this.filter.toLowerCase()) : false)
        || (user.getUserData() && user.getUserData()._lastName ? user.getUserData()._lastName.toLowerCase().includes(this.filter.toLowerCase()) : false)
        || (user.getEmail() ? user.getEmail().includes(this.filter.toLowerCase()) : false));
    }
  }

  public onAdd(user : User) : void {
    if(this.teamList.indexOf(user) > 0) return;
    this.teamList.push(user);
    this.userList.splice(this.userList.indexOf(user), 1);
    this.displayList.splice(this.displayList.indexOf(user), 1);
  }

  public onRemove(user : User) : void {
    if((this.project.getProjectAuthor() ? this.project.getProjectAuthor() == user.getID() : false)) {
      this.project.setProjectAuthor(null);
    }
    this.teamList.splice(this.teamList.indexOf(user), 1);
    this.userList.push(user);
    this.displayList.push(user);
  }

  public onPromote(user : User) : void {
    if(!this.project.getProjectAuthor()) {
      this.project.setProjectAuthor(user.getID());
      if(this.teamList.indexOf(user) >= 0)
        this.teamList.splice(this.teamList.indexOf(user), 1);
      this.teamList.unshift(user);
      if(this.userList.indexOf(user) >= 0)
        this.userList.splice(this.userList.indexOf(user), 1);
      if(this.displayList.indexOf(user) >= 0)
        this.displayList.splice(this.displayList.indexOf(user), 1);
    }
  }

  public onDemote() : void {
    this.project.setProjectAuthor(null);
  }

  public checkPromotion(userID : ObjectID) {
    return userID == (this.project.getProjectAuthor() ? this.project.getProjectAuthor() : false);
  }

  // TEAM MEMBER END

  // PROJECT IMAGE

  public processImage(imageInput : any) {
    let reader : FileReader = new FileReader();
    reader.readAsDataURL(imageInput.target.files[0]);
    reader.onload = (event) => {
      //@ts-ignore - result exists on target.
      this.project.getProjectDetails()._projectAvatar = event.target.result;
    };
  }

  // PROJECT SUBMIT/CANCEL

  public onSubmit() : void {
    this.project.getProjectDetails()._projectTeam = this.teamList;
    this.project.getProjectDetails()._projectURIs = [];

    if (this.urlList[0].value != "http://")
    {for (let url of this.urlList) {this.project.getProjectDetails()._projectURIs.push(url.value)}}
    if(!this.project.getProjectName() || !this.project.getProjectDescription())
      return this.alertService.dangerAlert(this.LANGUAGE.EN.ACP.PROJECT_MANAGER.ALERTS.MISSING_PROJECT_DATA);
    if(!this.project.getProjectDetails()._projectAvatar)
      return this.alertService.dangerAlert(this.LANGUAGE.EN.ACP.PROJECT_MANAGER.ALERTS.MISSING_PROJECT_AVATAR);
    if(!this.project.getProjectDetails()._projectTeam || !this.project.getProjectAuthor())
      return this.alertService.dangerAlert(this.LANGUAGE.EN.ACP.PROJECT_MANAGER.ALERTS.MISSING_PROJECT_MANAGER);
    if(!this.project.getProjectDetails()._projectURIs)
      return this.alertService.dangerAlert(this.LANGUAGE.EN.ACP.PROJECT_MANAGER.ALERTS.MISSING_PROJECT_URLS);
    this.projectService.addProject(this.project)
      .then((project : Project) => {if(project) this._location.back(); window.scroll(0,0)})
      .catch((error) => console.log(error));
  }

  public onCancel() : void {
    this._location.back();
  }

}
