import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpProjectEditComponent } from './acp-project-edit.component';

describe('AcpProjectEditComponent', () => {
  let component: AcpProjectEditComponent;
  let fixture: ComponentFixture<AcpProjectEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpProjectEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpProjectEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
