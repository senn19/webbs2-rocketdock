import { Component, OnInit } from '@angular/core';
import {ProjectManagerService} from "../../../services/projectManager/project-manager.service";
import {ActivatedRoute} from "@angular/router";
import {ObjectID} from "bson";
import {User} from "../../../../../class/user/User";
import {UserManagerService} from "../../../services/userManager/user-manager.service";
import {AlertManagerService} from "../../../services/alertManager/alert-manager.service";
import {Project} from "../../../../../class/project/Project";
import {Location} from "@angular/common";
import {LANGUAGE} from "../../../../../language/LANGUAGE";

@Component({
  selector: 'app-acp-project-edit',
  templateUrl: './acp-project-edit.component.html',
  styleUrls: ['./acp-project-edit.component.scss']
})
export class AcpProjectEditComponent implements OnInit {

  public project : Project = new Project();
  public userList : User[] = [];
  public displayList : User[] = [];
  public teamList : User[] = [];
  public filter : string;

  public LANGUAGE = LANGUAGE;
  public urlList : Array<{ value : string}> = [];

  constructor(public projectService : ProjectManagerService, public route : ActivatedRoute, public usmService : UserManagerService,
              public alertService : AlertManagerService, public _location: Location) { }

  ngOnInit() {
    this.projectService.getProjectByData(new ObjectID(this.route.snapshot.paramMap.get("id")))
      .then((project : Project) => {
        this.project = project;
        for(let url of project.getProjectDetails()._projectURIs) {
          this.urlList.push({value : url});
        }
        this.teamList = project.getProjectDetails()._projectTeam;
        this.usmService.getUserList().then((userList : User[]) => {
          this.userList = userList;
        });
      });
  }

  // Urls

  public addUrlInput() {
    this.urlList.push({ value : "http://"});
  }

  public removeUrlInput(index : number) {
    this.urlList.splice(index, 1);
  }

  // TEAM MEMBER

  public filterByWord() : void {
    this.displayList = [];
    if(this.filter.length >= 3) {
      this.displayList = this.userList.filter((user : User) => user.getUsername().toLowerCase().includes(this.filter.toLowerCase())
        || (user.getUserData() && user.getUserData()._foreName ? user.getUserData()._foreName.toLowerCase().includes(this.filter.toLowerCase()) : false)
        || (user.getUserData() && user.getUserData()._lastName ? user.getUserData()._lastName.toLowerCase().includes(this.filter.toLowerCase()) : false)
        || (user.getEmail() ? user.getEmail().includes(this.filter.toLowerCase()) : false));
    }
  }

  public onAdd(user : User) : void {
    if(this.teamList.indexOf(user) > 0) return;
    this.teamList.push(user);
    this.userList.splice(this.userList.indexOf(user), 1);
    this.displayList.splice(this.displayList.indexOf(user), 1);
  }

  public onRemove(user : User) : void {
    if((this.project.getProjectAuthor() ? this.project.getProjectAuthor() == user.getID() : false)) {
      this.project.setProjectAuthor(null);
    }
    this.teamList.splice(this.teamList.indexOf(user), 1);
    this.userList.push(user);
    this.displayList.push(user);
  }

  public onPromote(user : User) : void {
    if(!this.project.getProjectAuthor()) {
      this.project.setProjectAuthor(user.getID());
      if(this.teamList.indexOf(user) >= 0)
        this.teamList.splice(this.teamList.indexOf(user), 1);
      this.teamList.unshift(user);
      if(this.userList.indexOf(user) >= 0)
        this.userList.splice(this.userList.indexOf(user), 1);
      if(this.displayList.indexOf(user) >= 0)
        this.displayList.splice(this.displayList.indexOf(user), 1);
    }
  }

  public onDemote() : void {
    this.project.setProjectAuthor(null);
  }

  public checkPromotion(userID : any) {
    return userID == (this.project.getProjectAuthor() ? this.project.getProjectAuthor().toHexString() : false);
  }

  // PROJECT IMAGE

  public processImage(imageInput : any) {
    let reader : FileReader = new FileReader();
    reader.readAsDataURL(imageInput.target.files[0]);
    reader.onload = (event) => {
      //@ts-ignore - result exists on target.
      this.project.getProjectDetails()._projectAvatar = event.target.result;
    };
  }

  // PROJECT SAVE/CANCEL

  public onSubmit() : void {
    this.project.getProjectDetails()._projectTeam = this.teamList;
    this.project.getProjectDetails()._projectURIs = [];

    if (this.urlList[0].value != "http://")
    {for (let url of this.urlList) {this.project.getProjectDetails()._projectURIs.push(url.value)}}
    if(!this.project.getProjectName() || !this.project.getProjectDescription())
      return this.alertService.dangerAlert(this.LANGUAGE.EN.ACP.PROJECT_MANAGER.ALERTS.MISSING_PROJECT_DATA);
    if(!this.project.getProjectDetails()._projectAvatar)
      return this.alertService.dangerAlert(this.LANGUAGE.EN.ACP.PROJECT_MANAGER.ALERTS.MISSING_PROJECT_AVATAR);
    if(!this.project.getProjectDetails()._projectTeam || !this.project.getProjectAuthor())
      return this.alertService.dangerAlert(this.LANGUAGE.EN.ACP.PROJECT_MANAGER.ALERTS.MISSING_PROJECT_MANAGER);
    if(!this.project.getProjectDetails()._projectURIs)
      return this.alertService.dangerAlert(this.LANGUAGE.EN.ACP.PROJECT_MANAGER.ALERTS.MISSING_PROJECT_URLS);
    this.projectService.editProject(this.project)
      .then(() => {window.scroll(0,0)})
      .catch((error) => console.log(error));
  }

  public onCancel() : void {
    this._location.back();
  }
}
