import { Component, OnInit } from '@angular/core';
import {Project} from "../../../../../class/project/Project";
import {ProjectManagerService} from "../../../services/projectManager/project-manager.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AcpAreYouSureComponent} from "../../../utils/modals/acp-are-you-sure/acp-are-you-sure.component";

@Component({
  selector: 'app-acp-project-list',
  templateUrl: './acp-project-list.component.html',
  styleUrls: ['./acp-project-list.component.scss']
})
export class AcpProjectListComponent implements OnInit {

  public itemData : Project[] = [];
  public copyData : Project[] = [];
  public f_project : string;

  public maxShowCount : number = 8;
  public showCount : number = 0;

  constructor(public projectService : ProjectManagerService, public modalService : NgbModal) {
    this.updateList();
  }

  ngOnInit() {
  }

  public onDelete(project : Project) : void {
    const modal = this.modalService.open(AcpAreYouSureComponent, {centered : true});
    modal.result.then(() => {
      this.projectService.deleteProject(project).then(() => {
        this.updateList();
      });
    }).catch((error) => {});
  }

  public updateList() : void {
    this.projectService.getProjectList().then((projectList : Project[]) => {
      this.itemData = projectList.slice(this.showCount, this.showCount + this.maxShowCount);
      this.copyData = projectList;
    });
  }

  public filterByProject() {
    this.itemData = this.copyData;
    this.itemData = this.itemData.filter((project : Project) => project.getProjectName().toLowerCase().includes(this.f_project.toLowerCase()));
  }

  public onForward() : void {
    if(this.showCount + this.maxShowCount < this.copyData.length) {
      this.showCount = this.showCount + this.maxShowCount;
      this.itemData = this.copyData.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }

  public onBackward() : void {
    if(this.showCount > 0) {
      this.showCount = this.showCount - this.maxShowCount;
      this.itemData = this.copyData.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }
}
