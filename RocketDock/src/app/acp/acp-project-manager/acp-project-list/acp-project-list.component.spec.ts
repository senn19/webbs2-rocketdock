import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpProjectListComponent } from './acp-project-list.component';

describe('AcpProjectListComponent', () => {
  let component: AcpProjectListComponent;
  let fixture: ComponentFixture<AcpProjectListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpProjectListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpProjectListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
