import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpHomeComponent } from './acp-home.component';

describe('AcpHomeComponent', () => {
  let component: AcpHomeComponent;
  let fixture: ComponentFixture<AcpHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
