import { Component, OnInit } from '@angular/core';
import {EventManagerService} from "../../services/eventManager/event-manager.service";
import {Log} from "../../../../class/events/Log";
import {LogType} from "../../../../class/events/Log";


@Component({
  selector: 'app-acp-home',
  templateUrl: './acp-home.component.html',
  styleUrls: ['./acp-home.component.scss']
})
export class AcpHomeComponent implements OnInit {
  public chartData = [
    { data: [330, 600, 260, 700], label: 'Account A' },
    { data: [120, 455, 100, 340], label: 'Account B' },
    { data: [45, 67, 800, 500], label: 'Account C' }
  ];

  public chartLabels = ['January', 'February', 'Mars', 'April'];

  public lastActivity : Log[] = [];
  public max_Activity = 5;


  constructor(public eventManager : EventManagerService) {}

  ngOnInit() {
    this.lastActivity = [];
    this.eventManager.getEventList().then((eventList) => {
      let count : number = 0;
      for(let event of eventList) {
        if(event._type == LogType.login && count < this.max_Activity) {
          this.lastActivity.push(event);
          count++;
        }
      }
    });



  }
}
