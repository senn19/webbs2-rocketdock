import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpUserManagerComponent } from './acp-user-manager.component';

describe('AcpUserManagerComponent', () => {
  let component: AcpUserManagerComponent;
  let fixture: ComponentFixture<AcpUserManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpUserManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpUserManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
