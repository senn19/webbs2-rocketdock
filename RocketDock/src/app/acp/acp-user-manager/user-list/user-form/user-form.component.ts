import {Component, EventEmitter, Output} from '@angular/core';
import {User} from "../../../../../../class/user/User";
import {UserData} from "../../../../../../class/user/UserData";

class UserForm {
  public username : string;
  public password : string;
  public email : string;
  public foreName : string;
  public lastName : string;
}

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent {
  @Output() addUser = new EventEmitter<User>();
  public user : UserForm = new UserForm();

  constructor() { }

  /**
   *  emits addUser Event
   */
  public onSubmit() : void {
    if(!this.user.username || !this.user.email || !this.user.password) return;
    this.addUser.emit(new User(
      this.user.username,
      this.user.password,
      this.user.email,
      new UserData(
        this.user.foreName,
        this.user.lastName,
        null,
        null,
      )
    ));
  }
}
