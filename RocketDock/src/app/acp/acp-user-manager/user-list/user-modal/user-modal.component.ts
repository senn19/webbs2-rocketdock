import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {User} from "../../../../../../class/user/User";
import {UserData} from "../../../../../../class/user/UserData";

class UserForm {
  public username : string;
  public password : string;
  public email : string;
  public foreName : string;
  public lastName : string;
  public description : string;
}

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss']
})
export class UserModalComponent implements OnInit {
  public user : UserForm = new UserForm();
  @Input() clickedUser : User;

  constructor(public activeModal : NgbActiveModal) {}

  ngOnInit() {
      this.user.username = this.clickedUser.getUsername();
      this.user.password = this.clickedUser.getPassword();
      this.user.foreName = this.clickedUser.getUserData()._foreName;
      this.user.email = this.clickedUser.getEmail();
      this.user.lastName = this.clickedUser.getUserData()._lastName;
      this.user.description = this.clickedUser.getUserData()._description;
  }

  public onSubmit() : void {
    let user : User = new User(
      this.user.username,
      this.user.password,
      this.user.email,
      new UserData(
        this.user.foreName,
        this.user.lastName,
        null,
        null,
        this.user.description
      )
    );
    user.setID(this.clickedUser.getID());
    user.setDate(this.clickedUser.getDate());
    this.activeModal.close(user);
  }
}
