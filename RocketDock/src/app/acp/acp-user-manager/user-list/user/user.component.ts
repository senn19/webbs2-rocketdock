import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {User} from "../../../../../../class/user/User";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {UserModalComponent} from "../user-modal/user-modal.component";
import {Router} from "@angular/router";
import {AcpAreYouSureComponent} from "../../../../utils/modals/acp-are-you-sure/acp-are-you-sure.component";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnChanges {
  @Input() public itemData : User[];

  public copyData : User[] = [];
  public showCount : number = 0;
  public maxShowCount : number = 8;

  @Output() deleteUser = new EventEmitter<User>();
  @Output() editUser = new EventEmitter<User>();

  constructor(public modalService : NgbModal, public router : Router) {}

  ngOnChanges(): void {
    this.copyData = this.itemData;
    if(this.itemData)
      this.itemData = this.itemData.slice(this.showCount, this.showCount + this.maxShowCount);
  }

  /**
   * emits deleteUser Event
   * @param {User} user : user to delete
   */
  public onDelete(user : User) : void {
    const modal = this.modalService.open(AcpAreYouSureComponent, { centered: true });
    modal.result.then(() => {
      this.deleteUser.emit(user);
    }).catch((error) => {});
  }

  public onEdit(user : User) : void {
    this.router.navigate(["/edit/"+user.getID()]);
  }

  /**
   * modalService call with user as param
   * @param {User} user to edit
   */
  public editItem(user : User) {
    const modal = this.modalService.open(UserModalComponent, { centered: true });
    modal.componentInstance.clickedUser = user;
    modal.result.then((result : User) => {
      this.editUser.emit(result);
    }).catch((error) => {});
  }

  public onForward() : void {
    if(this.showCount + this.maxShowCount < this.copyData.length) {
      this.showCount = this.showCount + this.maxShowCount;
      this.itemData = this.copyData.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }

  public onBackward() : void {
    if(this.showCount > 0) {
      this.showCount = this.showCount - this.maxShowCount;
      this.itemData = this.copyData.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }
}
