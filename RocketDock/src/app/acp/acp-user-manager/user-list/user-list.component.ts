import {Component} from '@angular/core';
import {User} from "../../../../../class/user/User";
import {UserManagerService} from "../../../services/userManager/user-manager.service";
import {AlertManagerService} from "../../../services/alertManager/alert-manager.service";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})

export class UserListComponent {
  public listData: User[];
  public copyData : User[];
  public listHead: any;

  public f_username : string;

  /**
   * fills class attributes with userArray
   * @param {UserManagerService} usmService
   * @param alertService
   */
  constructor(public usmService: UserManagerService, public alertService: AlertManagerService) {
    this.updateList();
  }

  /**
   * service call to edit user,
   * @param {User} user to edit
   */
  public editUser(user : User) : void {
    this.usmService.editUser(user);
    this.updateList();
  }


  /**
   * service call to delete user,
   * @param {User} user : user to delete
   */
  public deleteUser(user: User) : void {
    this.usmService.deleteUser(user);
    this.updateList();
  }

  /**
   * service call to add user,
   * @param {User} user : user to add
   */
  public addUser(user: User) : void {
    this.usmService.addUser(user);
    this.updateList();
  }

  /**
   * service call to update userList
   */
  public updateList() : void {
    this.usmService.getUserList().then((userList : User[]) => {
      this.listData = [];
      for(let user of userList) {
        this.listData.push(user);
      }
      this.copyData = this.listData;
    })
  }

  public filterByUsername() : void {
    this.listData = this.copyData;
    this.listData = this.listData.filter((user : User) => user.getUsername().toLowerCase().includes(this.f_username.toLowerCase()));
  }
}
