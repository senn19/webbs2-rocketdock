import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserManagerService} from "../../../services/userManager/user-manager.service";
import {ActivatedRoute} from "@angular/router";
import {ObjectID} from "bson";
import {User} from "../../../../../class/user/User";
import {PermissionGroup} from "../../../../../class/perm/PermissionGroup";
import {PermManagerService} from "../../../services/permManager/perm-manager.service";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit, OnDestroy {

  public user : User = new User();
  public groups : PermissionGroup[] = [];
  public currGroup : PermissionGroup;

  constructor(public usmService : UserManagerService, public route : ActivatedRoute,
              public permService : PermManagerService) {}


  ngOnInit() : void {
    document.querySelector("#acpMenu").insertAdjacentHTML('beforeend', `
          <li class="backward nav-item pl-3">
            <a class="nav-link" href="/acp/userList">
              <img src="/assets/icons/backward.svg" height="24px">
            </a>
          </li>`);
    this.permService.getGroupList().then((groupList : PermissionGroup[]) => {
      this.groups = groupList;

      this.usmService.getUserByData(new ObjectID(this.route.snapshot.paramMap.get("id")))
        .then((user : User) => {
          this.user = user;
          for(let group of this.groups) {
            if(group.getGroupID() == this.user.getUserData()._group) {
              this.currGroup = group;
            }
          }
        })
    });
  }

  ngOnDestroy(): void {
    document.querySelector(".backward").remove();
  }

  public onSubmit() : void {
    if(this.currGroup)
      this.user.getUserData()._group = new ObjectID(this.currGroup.getGroupID());
    this.usmService.editUser(this.user).catch();
  }

  public processImage(imageInput : any) {
    let reader : FileReader = new FileReader();
    reader.readAsDataURL(imageInput.target.files[0]);
    reader.onload = (event) => {
      //@ts-ignore - result exists on target.
      this.user.getUserData()._avatar = event.target.result;
    };
  }
}
