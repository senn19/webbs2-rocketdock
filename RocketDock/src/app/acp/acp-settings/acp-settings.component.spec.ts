import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpSettingsComponent } from './acp-settings.component';

describe('AcpSettingsComponent', () => {
  let component: AcpSettingsComponent;
  let fixture: ComponentFixture<AcpSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
