import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpGroupModalComponent } from './acp-group-modal.component';

describe('AcpGroupModalComponent', () => {
  let component: AcpGroupModalComponent;
  let fixture: ComponentFixture<AcpGroupModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpGroupModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpGroupModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
