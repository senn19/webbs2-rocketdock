import {Component, Input, OnInit} from '@angular/core';
import {PermissionGroup} from "../../../../../class/perm/PermissionGroup";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Permissions} from "../../../../../class/perm/Permissions";

@Component({
  selector: 'app-acp-group-modal',
  templateUrl: './acp-group-modal.component.html',
  styleUrls: ['./acp-group-modal.component.scss']
})
export class AcpGroupModalComponent implements OnInit {

  @Input() group : PermissionGroup;

  public permissions : string[] = Object.keys(new Permissions());
  public accessLevel : string = this.permissions[2];

  constructor(public activeModal : NgbActiveModal) { }

  ngOnInit() {
    for(let property in this.group.getPermissions()) {
      if(this.group.getPermissions()[property]) {
        for(let key in this.permissions) {
          if(this.permissions[key] === property) {
            this.accessLevel = this.permissions[key];
          }
        }
      }
    }
  }

  public onSubmit() : void {
    if(!this.group.getGroupName()) return;
    let permissions : Permissions = new Permissions();
    for(let property in permissions) {
      if(property.toString() === this.accessLevel) {
        permissions[property] = true;
      }
    }
    this.group.setPermissions(permissions);
    this.activeModal.close(this.group);
  }
}
