import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpGroupManagerComponent } from './acp-group-manager.component';

describe('AcpGroupManagerComponent', () => {
  let component: AcpGroupManagerComponent;
  let fixture: ComponentFixture<AcpGroupManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpGroupManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpGroupManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
