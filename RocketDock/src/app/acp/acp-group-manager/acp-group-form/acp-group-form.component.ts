import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PermissionGroup} from "../../../../../class/perm/PermissionGroup";
import {Permissions} from "../../../../../class/perm/Permissions";

@Component({
  selector: 'app-acp-group-form',
  templateUrl: './acp-group-form.component.html',
  styleUrls: ['./acp-group-form.component.scss']
})
export class AcpGroupFormComponent implements OnInit {

  @Output() addGroup : EventEmitter<PermissionGroup> = new EventEmitter<PermissionGroup>();

  public group : PermissionGroup = new PermissionGroup();
  public permissions : string[] = Object.keys(new Permissions());
  public accessLevel : string = this.permissions[2];

  constructor() {}

  ngOnInit() {
  }

  public onSubmit() : void {
    if(!this.group.getGroupName()) return;
    let permissions : Permissions = new Permissions();
    for(let property in permissions) {
      if(property.toString() === this.accessLevel) {
        permissions[property] = true;
      }
    }
    this.group.setPermissions(permissions);
    this.addGroup.emit(this.group);
  }
}
