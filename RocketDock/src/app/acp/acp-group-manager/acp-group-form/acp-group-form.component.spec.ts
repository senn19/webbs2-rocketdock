import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpGroupFormComponent } from './acp-group-form.component';

describe('AcpGroupFormComponent', () => {
  let component: AcpGroupFormComponent;
  let fixture: ComponentFixture<AcpGroupFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpGroupFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpGroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
