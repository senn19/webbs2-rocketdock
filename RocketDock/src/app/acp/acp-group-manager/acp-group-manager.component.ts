import { Component, OnInit } from '@angular/core';
import {PermissionGroup} from "../../../../class/perm/PermissionGroup";
import {PermManagerService} from "../../services/permManager/perm-manager.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AcpGroupModalComponent} from "./acp-group-modal/acp-group-modal.component";
import {AcpAreYouSureComponent} from "../../utils/modals/acp-are-you-sure/acp-are-you-sure.component";

@Component({
  selector: 'app-acp-group-manager',
  templateUrl: './acp-group-manager.component.html',
  styleUrls: ['./acp-group-manager.component.scss']
})
export class AcpGroupManagerComponent implements OnInit {

  public itemData : PermissionGroup[] = [];
  public copyData : PermissionGroup[] = [];
  public f_group : string;

  public showCount : number = 0;
  public maxShowCount : number = 8;

  constructor(public groupManager : PermManagerService, public modalService : NgbModal) {
    this.updateList();
  }

  ngOnInit() {
  }

  public editItem(group : PermissionGroup) : void {
    const modal = this.modalService.open(AcpGroupModalComponent, { centered: true });
    modal.componentInstance.group = group;
    modal.result.then((group : PermissionGroup) => {
      this.groupManager.editGroup(group)
        .then(() => {
          this.updateList();
        })
    }).catch((error) => {});
  }

  public onDelete(group : PermissionGroup) : void {
    const modal = this.modalService.open(AcpAreYouSureComponent, { centered: true });
    modal.result.then(() => {
      this.groupManager.deleteGroup(group)
        .then(() => {
          this.updateList();
        })
    }).catch((error) => {});
  }

  public addItem(group : PermissionGroup) : void {
    this.groupManager.addGroup(group)
      .then(() => {
        this.updateList();
      })
  }

  public updateList() : void {
    this.groupManager.getGroupList()
      .then((groupList : PermissionGroup[]) => {
        this.itemData = groupList.slice(this.showCount, this.showCount + this.maxShowCount);
        this.copyData = groupList;
      })
  }

  public filterByGroup() : void {
    this.itemData = this.copyData;
    this.itemData = this.itemData.filter((group : PermissionGroup) => group.getGroupName().toLowerCase().includes(this.f_group.toLowerCase()));
  }

  public onForward() : void {
    if(this.showCount + this.maxShowCount < this.copyData.length) {
      this.showCount = this.showCount + this.maxShowCount;
      this.itemData = this.copyData.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }

  public onBackward() : void {
    if(this.showCount > 0) {
      this.showCount = this.showCount - this.maxShowCount;
      this.itemData = this.copyData.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }
}
