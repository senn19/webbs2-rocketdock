import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-acp-are-you-sure',
  templateUrl: './acp-are-you-sure.component.html',
  styleUrls: ['./acp-are-you-sure.component.scss']
})
export class AcpAreYouSureComponent implements OnInit {

  constructor(public activeModal : NgbActiveModal) { }

  ngOnInit() {
  }

  public onContinue() : void {
    this.activeModal.close();
  }

}
