import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpAreYouSureComponent } from './acp-are-you-sure.component';

describe('AreYouSureComponent', () => {
  let component: AcpAreYouSureComponent;
  let fixture: ComponentFixture<AcpAreYouSureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpAreYouSureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpAreYouSureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
