import {Component, Input, OnInit} from '@angular/core';
import {Subject} from "rxjs";
import {debounceTime} from "rxjs/internal/operators";
import {AlertManagerService} from "../services/alertManager/alert-manager.service";

interface Alert {
  _message : string;
  _type : string;

  closeAlert() : void;
}

@Component({
  selector: 'app-user-alert',
  templateUrl: './user-alert.component.html',
  styleUrls: ['./user-alert.component.scss']
})
export class UserAlertComponent implements OnInit, Alert {
  public _alert = new Subject<string[]>();

  public _message : string;
  public _type : string;
  public _icon : string;

  constructor(public alertService : AlertManagerService) {
    this.alertService.alert = this._alert;
  }

  ngOnInit() : void {
    this._alert.subscribe((message : string[]) => {
      this._message = message[2];
      this._icon = message[1];
      this._type = message[0];
    });
    this._alert.pipe(debounceTime(5000))
      .subscribe(() => {
        this._message = null;
      });
  }

  public closeAlert() : void {
    this._alert.next([null, null, null]);
  }
}
