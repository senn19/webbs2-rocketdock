import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'booleanToStatus'
})
export class BooleanToStatusPipe implements PipeTransform {

  transform(value: boolean, args?: any): any {
    if(value) return "open";
    return "closed";
  }

}
