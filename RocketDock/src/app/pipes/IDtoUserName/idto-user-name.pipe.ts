import { Pipe, PipeTransform } from '@angular/core';
import {User} from "../../../../class/user/User";
import {ObjectID} from "bson";
import {UserManagerService} from "../../services/userManager/user-manager.service";

@Pipe({
  name: 'iDtoUserName'
})
export class IDtoUserNamePipe implements PipeTransform {

  constructor(private usmService : UserManagerService) {}

  transform(id: ObjectID, args?: any): Promise<string> {
    return this.usmService.getUserByData(new ObjectID(id))
      .then((user : User) => {
        if(user) return user.getUsername();
        return "No User found";
      });
  }

}
