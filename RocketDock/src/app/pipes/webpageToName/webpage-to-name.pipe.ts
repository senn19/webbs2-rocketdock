import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'webpageToName'
})
export class WebpageToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    value = value.match(/(?:[-a-zA-Z0-9@:%_+~.#=]{2,256}\.)?([-a-zA-Z0-9@:%_+~#=]*)\.[a-z]{2,6}\b(?:[-a-zA-Z0-9@:%_+.~#?&\/=]*)/)[1];
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
}
