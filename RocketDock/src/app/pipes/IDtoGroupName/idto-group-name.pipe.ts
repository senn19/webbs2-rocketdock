import { Pipe, PipeTransform } from '@angular/core';
import {ObjectID} from "bson";
import {PermManagerService} from "../../services/permManager/perm-manager.service";
import {PermissionGroup} from "../../../../class/perm/PermissionGroup";
import {LANGUAGE} from "../../../../language/LANGUAGE";

@Pipe({
  name: 'iDtoGroupName'
})
export class IDtoGroupNamePipe implements PipeTransform {

  constructor(private permService : PermManagerService) {}

  transform(id: ObjectID, args?: any): any {
    return this.permService.getGroupByData(new ObjectID(id))
      .then((group : PermissionGroup) => {
        if(group != null) return group.getGroupName();
        return LANGUAGE.EN.UCP.HOME.GROUP_NULL;
      });
  }

}
