import { Pipe, PipeTransform } from '@angular/core';
import {ImageFinderService} from "../../services/imageFinder/image-finder.service";

@Pipe({
  name: 'webpageToImage'
})
export class WebpageToImagePipe implements PipeTransform {

  constructor(public iconFinder : ImageFinderService) {}

  transform(value: any, args?: any): any {
    return this.iconFinder.findImage(value + " Icon")
      .then((image : string) => {
      if(image) return image;
      return "/assets/icons/error.svg";
    })
  }

}
