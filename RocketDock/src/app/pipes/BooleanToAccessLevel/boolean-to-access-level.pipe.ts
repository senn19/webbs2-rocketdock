import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'booleanToAccessLevel'
})
export class BooleanToAccessLevelPipe implements PipeTransform {

  transform(permissions: any, args?: any): any {
    for(let perm in permissions) {
      if(permissions.hasOwnProperty(perm) && permissions[perm]) {
        return perm.substr(3);
      }
    }
  }

}
