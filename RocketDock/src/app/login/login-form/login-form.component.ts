import {Component, EventEmitter, Output} from '@angular/core';
import {AccountManagerService} from "../../services/accountManager/account-manager.service";
import {LANGUAGE} from "../../../../language/LANGUAGE";
import {Router} from "@angular/router";

class LoginForm {
  public login_email;
  public login_password;
}


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {
  public user : LoginForm = new LoginForm();
  public LANGUAGE = LANGUAGE;
  @Output() public loginEvent = new EventEmitter();

  constructor(public loginService : AccountManagerService, public router : Router) { }

  public onLogin() : void {
    this.loginService.loginUser(this.user.login_email, this.user.login_password)
      .then((response : any) => {
        if(response != null)
          setTimeout(() => {
            this.router.navigate(["/ucp/"]);
          }, 2000)
      });
  }


}
