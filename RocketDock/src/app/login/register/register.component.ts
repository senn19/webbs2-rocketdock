import { Component, OnInit } from '@angular/core';
import {LANGUAGE} from "../../../../language/LANGUAGE";
import {User} from "../../../../class/user/User";
import {AlertManagerService} from "../../services/alertManager/alert-manager.service";
import {UserManagerService} from "../../services/userManager/user-manager.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public user : User = new User();
  public r_email : string;
  public r_password : string;
  public captcha : string;

  public LANGUAGE = LANGUAGE;

  constructor(public alertManager : AlertManagerService, private userManager : UserManagerService,
              private router : Router) { }

  public onRegister() {
    if (this.user.getPassword() == null || this.user.getEmail() == null || this.user.getUsername() == null)
      return this.alertManager.infoAlert(LANGUAGE.EN.UCP.REGISTER.ALERTS.EMAIL_PASSWORD_MISSING);
    if(this.user.getEmail() != this.r_email)
      return this.alertManager.infoAlert(LANGUAGE.EN.UCP.REGISTER.ALERTS.EMAIL_MISS_MATCH);
    if(this.user.getPassword() != this.r_password)
      return this.alertManager.infoAlert(LANGUAGE.EN.UCP.REGISTER.ALERTS.PASSWORD_MISS_MATCH);
    if(!this.captcha)
      return this.alertManager.infoAlert(LANGUAGE.EN.UCP.REGISTER.ALERTS.CAPTCHA);
    this.userManager.addUser(this.user, this.captcha)
      .then((result : any) => {
        if(result != null)
          return this.router.navigate(["/login"]);
        setTimeout(() => {
          location.reload();
        },2000);
      })
  }

  public resolved(captchaResponse: string) {
    this.captcha = captchaResponse;
  }


  ngOnInit() {
  }

}
