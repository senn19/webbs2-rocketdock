import {Component, EventEmitter, Output} from '@angular/core';
import {AccountManagerService} from "../../services/accountManager/account-manager.service";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent {
  @Output() public logoutEvent = new EventEmitter();

  constructor(public logOutService : AccountManagerService) { }

  public onLogOut() {
    this.logOutService.logoutUser().then(() => {this.logoutEvent.emit();});
  }

}
