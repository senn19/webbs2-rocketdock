import {Component, AfterViewInit, OnDestroy} from '@angular/core';
import {LANGUAGE} from "../../../language/LANGUAGE";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})


export class LoginComponent implements AfterViewInit, OnDestroy {

  public LANGUAGE = LANGUAGE;
  public bodyQuery = document.querySelector('body');

  constructor() { }

  ngAfterViewInit() {
    this.bodyQuery.classList.add('endlessConstellation');
    this.bodyQuery.classList.add('animated');
  }

  ngOnDestroy() {
    this.bodyQuery.classList.remove('endlessConstellation');
    this.bodyQuery.classList.remove('animated');
  }
}
