import { Component, OnInit } from '@angular/core';
import {LANGUAGE} from "../../../../language/LANGUAGE";

@Component({
  selector: 'app-disclaimer',
  templateUrl: './disclaimer.component.html',
  styleUrls: ['./disclaimer.component.scss']
})
export class DisclaimerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public LANGUAGE = LANGUAGE;
}
