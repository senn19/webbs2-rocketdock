import {Component} from '@angular/core';
import {AccountManagerService} from "../../services/accountManager/account-manager.service";
import {User} from "../../../../class/user/User";
import {UserManagerService} from "../../services/userManager/user-manager.service";
import {ProjectManagerService} from "../../services/projectManager/project-manager.service";
import {ObjectID} from "bson";
import {LANGUAGE} from "../../../../language/LANGUAGE";
import {sprintf} from "sprintf-js";
import {Project} from "../../../../class/project/Project";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  public user : User = new User();
  public group : ObjectID;
  public projects : Project[] = [];

  public LANGUAGE = LANGUAGE;

  constructor(public accountManager : AccountManagerService, public usmService : UserManagerService,
              public projectService : ProjectManagerService) {
    this.accountManager.getSessionData().then(() => {
      this.usmService.getUserByData(this.accountManager.user.getID())
        .then((user : User) => {
          this.user = user;
          this.group = (user.getUserData() ? user.getUserData()._group : null);
        }).then(() => {
        projectService.getProjectsByUser(this.user)
          .then((projects : Project[]) => {
            this.projects = projects;
          })
      });
    });
  }

  public print(value : string, string : string) {
    return sprintf(value, string);
  }
}
