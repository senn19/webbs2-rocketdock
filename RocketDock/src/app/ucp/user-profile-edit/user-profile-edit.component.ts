import {Component, OnInit} from '@angular/core';
import {User} from "../../../../class/user/User";
import {UserManagerService} from "../../services/userManager/user-manager.service";
import {ActivatedRoute} from "@angular/router";
import {ObjectID} from "bson";
import {Location} from "@angular/common";
import {LANGUAGE} from "../../../../language/LANGUAGE";
import {UserData} from "../../../../class/user/UserData";

@Component({
  selector: 'app-user-profile-edit',
  templateUrl: './user-profile-edit.component.html',
  styleUrls: ['./user-profile-edit.component.scss']
})
export class UserProfileEditComponent implements OnInit {

  public user : User = new User();
  public LANGUAGE = LANGUAGE;

  constructor(public usmService : UserManagerService, public route : ActivatedRoute, public _location: Location) {}


  ngOnInit() : void {
      this.usmService.getUserByData(new ObjectID(this.route.snapshot.paramMap.get("id")))
        .then((user : User) => {
          if(!user.getUserData()) {
            user.setUserData(new UserData(null, null));
          }
          this.user = user;
        });
  }

  public onSubmit() : void {
    this.usmService.editUser(this.user).catch();
    window.scroll(0,0);
  }

  public processImage(imageInput : any) {
    let reader : FileReader = new FileReader();
    reader.readAsDataURL(imageInput.target.files[0]);
    reader.onload = (event) => {
      //@ts-ignore - result exists on target.
      this.user.getUserData()._avatar = event.target.result;
    };
  }

  public onCancel() : void {
    this._location.back();
  }
}

