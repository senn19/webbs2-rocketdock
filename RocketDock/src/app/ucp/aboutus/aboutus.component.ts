import { Component } from '@angular/core';
import {LANGUAGE} from "../../../../language/LANGUAGE";

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss']
})
export class AboutusComponent {

  constructor() { }

  public LANGUAGE = LANGUAGE;


}
