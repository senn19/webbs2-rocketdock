import {Component} from '@angular/core';
import {UserManagerService} from "../../services/userManager/user-manager.service";
import {AlertManagerService} from "../../services/alertManager/alert-manager.service";
import {User} from "../../../../class/user/User";
import {LANGUAGE} from "../../../../language/LANGUAGE";

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.scss']
})
export class MemberListComponent {
  public listData: User[];
  public listDataCopy : User[];
  public LANGUAGE = LANGUAGE;
  public f_word : string;

  /**
   * fills class attributes with userArray
   * @param {UserManagerService} usmService
   * @param alertService
   */
  constructor(public usmService: UserManagerService, public alertService: AlertManagerService) {
    this.updateList();
  }

  public updateList() : void {
    this.usmService.getUserList().then((userList : User[]) => {
      this.listData = [];
      for(let user of userList) {
        this.listData.push(user);
      }
      this.listDataCopy = this.listData;
    })
  }

  public filterByWords() {
    this.listData = this.listDataCopy;
    this.listData = this.listData.filter((user : User) => user.getUsername().toLowerCase().includes(this.f_word)
      || (user.getUserData() && user.getUserData()._foreName ? user.getUserData()._foreName.toLowerCase().includes(this.f_word.toLowerCase()) : false)
      || (user.getUserData() && user.getUserData()._lastName ? user.getUserData()._lastName.toLowerCase().includes(this.f_word.toLowerCase()) : false)
      || (user.getEmail() ? user.getEmail().includes(this.f_word.toLowerCase()) : false));
  }
}
