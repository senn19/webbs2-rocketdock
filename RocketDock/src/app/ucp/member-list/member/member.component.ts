import {Component, Input, OnChanges} from '@angular/core';
import {User} from "../../../../../class/user/User";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})
export class MemberComponent implements OnChanges {
  @Input() public itemData : User[];
  public user : User;

  public copyData : User[];
  public showCount : number = 0;
  public maxShowCount : number = 12;

  constructor(public route : ActivatedRoute, public location : Location) { }

  ngOnChanges(): void {
    this.copyData = this.itemData;
    if(this.itemData) {
      this.user = this.itemData.filter((user : User) => user.getID().toHexString() == this.route.snapshot.paramMap.get("id"))[0];
      this.itemData = this.itemData.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }

  /**
   * creates Array Object from JSON Object
   * @param {User} user : user JSON to transform
   * @returns {any[]} : user Array Object
   */
  public itemToArray(user : User) : any[] {
    return Object.keys(user).map((key) => {
      return user[key]
    });
  }

  public clickedMember(member : User) : void {
    this.user = member;
    this.location.go("/ucp/userList/"+this.user.getID());
  }

  public onForward() : void {
    if(this.showCount + this.maxShowCount < this.copyData.length) {
      this.showCount = this.showCount + this.maxShowCount;
      this.itemData = this.copyData.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }

  public onBackward() : void {
    if(this.showCount > 0) {
      this.showCount = this.showCount - this.maxShowCount;
      this.itemData = this.copyData.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }

  public closeCard() : void {
    this.user = null;
    this.location.go("/ucp/userList");
  }
}
