import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {User} from "../../../../../../class/user/User";
import {LANGUAGE} from "../../../../../../language/LANGUAGE";
import {sprintf} from "sprintf-js";
import {Project} from "../../../../../../class/project/Project";
import {ProjectManagerService} from "../../../../services/projectManager/project-manager.service";

@Component({
  selector: 'app-member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.scss']
})
export class MemberCardComponent implements OnChanges {
  @Input() user : User = null;
  @Output() close : EventEmitter<null> = new EventEmitter<null>();
  public LANGUAGE = LANGUAGE;
  public projects : Project[];

  constructor(public projectManager : ProjectManagerService) { }

  public closeCard() {
    this.close.emit();
  }

  public print(value : string, string : string) {
    return sprintf(value, string);
  }

  ngOnChanges(): void {
    if(this.user) {
      this.projectManager.getProjectsByUser(this.user).then((projects : Project[]) => {
        this.projects = projects;
      });
    }
  }

}
