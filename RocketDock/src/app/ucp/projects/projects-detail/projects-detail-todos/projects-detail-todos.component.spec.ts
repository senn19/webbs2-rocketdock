import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsDetailTodosComponent } from './projects-detail-todos.component';

describe('ProjectsDetailTodosComponent', () => {
  let component: ProjectsDetailTodosComponent;
  let fixture: ComponentFixture<ProjectsDetailTodosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsDetailTodosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsDetailTodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
