import { Component } from '@angular/core';
import {Project} from "../../../../../class/project/Project";
import {ProjectManagerService} from "../../../services/projectManager/project-manager.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ObjectID} from "bson";
import {AccountManagerService} from "../../../services/accountManager/account-manager.service";
import {User} from "../../../../../class/user/User";
import {AcpAreYouSureComponent} from "../../../utils/modals/acp-are-you-sure/acp-are-you-sure.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {UserManagerService} from "../../../services/userManager/user-manager.service";
import {Comment} from "../../../../../class/project/comments/Comment";
import {CommentManagerService} from "../../../services/commentManager/comment-manager.service";

@Component({
  selector: 'app-projects-detail',
  templateUrl: './projects-detail.component.html',
  styleUrls: ['./projects-detail.component.scss']
})
export class ProjectsDetailComponent {
  public project : Project = new Project();
  public commentList : Comment[] = [];
  public comment : Comment = new Comment();
  public teamMember : User[] = [];
  public user : User;
  public group : any;

  constructor(public commentService : CommentManagerService, public projectService : ProjectManagerService, public route : ActivatedRoute, public accountManager : AccountManagerService,
              public modalService : NgbModal, public router : Router, public usmManager : UserManagerService) { }

  ngOnInit() {
    this.projectService.getProjectByData(new ObjectID(this.route.snapshot.paramMap.get("id")))
      .then((project : Project) => {
        this.project = project;
        for(let member of this.project.getProjectDetails()._projectTeam) {
          this.usmManager.getUserByData(member.getID()).then((user : User) => {
            this.teamMember.push(user);
          });
        }
        this.updateCommentList();
        return this.accountManager.getSessionData()
      })
      .then(() => {
        this.user = this.accountManager.user;
        this.group = this.accountManager.group;
      });
  }

  public onDelete(project : Project) : void {
    const modal = this.modalService.open(AcpAreYouSureComponent, {centered : true});
    modal.result.then(() => {
      this.projectService.deleteProject(project).then(() => {
        this.router.navigate(["/ucp/projects"]);
      });
    });
  }

  public updateCommentList() {
    this.commentService.getCommentList(new ObjectID(this.route.parent.snapshot.paramMap.get("id")))
      .then((commentList : Comment[]) => {
        this.commentList = commentList;
      })
  }

  public onDeleteComment(comment : Comment) {
    this.commentService.deleteComment(comment).then(() => {
      this.updateCommentList();
    });
  }

  public onSubmit() {
    if(!this.comment.getContent()) return;
    this.comment.setAuthorID(new ObjectID(this.user.getID()));
    this.comment.setProjectID(new ObjectID(this.route.parent.snapshot.paramMap.get("id")));
    this.commentService.addComment(this.comment).then(() => {
      this.updateCommentList();
    });
  }

  public checkMemberOnProject() {
    for(let member of this.teamMember) {
      if(member.getID().toHexString() == this.user.getID().toHexString())
        return true;
    }
    return false;
  }

}
