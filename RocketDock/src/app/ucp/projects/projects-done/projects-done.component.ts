import { Component, OnInit } from '@angular/core';
import {Project} from "../../../../../class/project/Project";
import {ProjectManagerService} from "../../../services/projectManager/project-manager.service";
import {AccountManagerService} from "../../../services/accountManager/account-manager.service";
import {User} from "../../../../../class/user/User";
import {LANGUAGE} from "../../../../../language/LANGUAGE";

@Component({
  selector: 'app-projects-done',
  templateUrl: './projects-done.component.html',
  styleUrls: ['./projects-done.component.scss']
})
export class ProjectsDoneComponent implements OnInit {

  constructor(public projectService : ProjectManagerService, public session : AccountManagerService) { }

  public projects : Project[] = [];
  public projectsCopy : Project[] = [];
  public user : User = null;
  public group : any = null;
  public f_word : string;

  public myProjects : boolean = false;

  public showCount : number = 0;
  public maxShowCount : number = 6;

  public LANGUAGE = LANGUAGE;

  ngOnInit() {
    this.session.getSessionData().then(() => {
      this.updateProjects();
      this.user = this.session.user;
      this.group = this.session.group;
    });
  }

  public updateProjects() {
    if(this.myProjects) {
      this.projectService.getProjectsByUser(this.user).then((projectList : Project[]) => {
        this.listProjects(projectList);
      });
    } else {
      this.projectService.getProjectList().then((projectList : Project[]) => {
        this.listProjects(projectList);
      });
    }
  }

  public listProjects(projectList : Project[]) {
    this.projects = [];
    for(let project of projectList) {
      if(!project.getProjectStatus()) {
        this.projects.push(project);
      }
    }
    this.projectsCopy = this.projects;
    this.projects = this.projects.slice(this.showCount, this.showCount + this.maxShowCount);
  }

  public listMyProjects() {
    this.myProjects = !this.myProjects;
    this.updateProjects();
  }


  // filter

  public filterByWords() : void {
    this.projects = this.projectsCopy;
    this.projects = this.projects.filter((project : Project) => project.getProjectName().toLowerCase().includes(this.f_word)
      || project.getProjectDescription().toLowerCase().includes(this.f_word.toLowerCase()));
    this.projects = this.projects.slice(this.showCount, this.showCount + this.maxShowCount);
  }


  // open projects

  public onOpen(project : Project) : void {
    project.setProjectStatus(true);
    this.projectService.editProject(project)
      .then(() => {
        this.updateProjects();
      });
  }

  // pagination

  public onForward() : void {
    if(this.showCount + this.maxShowCount < this.projectsCopy.length) {
      this.showCount = this.showCount + this.maxShowCount;
      this.projects = this.projectsCopy.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }

  public onBackward() : void {
    if(this.showCount > 0) {
      this.showCount = this.showCount - this.maxShowCount;
      this.projects = this.projectsCopy.slice(this.showCount, this.showCount + this.maxShowCount);
    }
  }
}
