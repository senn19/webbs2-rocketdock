import {Component, Input} from '@angular/core';
import {Router} from "@angular/router";
import {User} from "../../../../class/user/User";

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent {
  @Input() group : boolean;
  @Input() user : User;

  constructor(public router : Router) {}

  public redirectOnLogout() {
    setTimeout(() => {
      location.reload();
      this.router.navigate(["/login"]);
    }, 2000);
  }

  public closeResponsiveMenu() {
    let menu = document.getElementById("navMenu");
    if(menu.classList.contains("show"))
      menu.classList.remove("show");
  }
}
