import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {AccountManagerService} from "../../services/accountManager/account-manager.service";
import {User} from "../../../../class/user/User";

@Component({
  selector: 'app-user-control-panel',
  templateUrl: './user-control-panel.component.html',
  styleUrls: ['./user-control-panel.component.scss']
})
export class UserControlPanelComponent implements OnInit, AfterViewInit, OnDestroy {
  public bodyQuery =  document.querySelector('body');
  group : boolean = false;
  user : User;

  constructor(public accountManager : AccountManagerService) {}

  ngOnInit() {
    this.accountManager.getSessionData().then(()=> {
      if(this.accountManager.group)
        this.group = (this.accountManager.group._permissions._isAdmin);
        this.user = this.accountManager.user;
    });
  }

  ngAfterViewInit() {
    this.bodyQuery.classList.add('endlessConstellation');
    this.bodyQuery.classList.add('animated');
    this.bodyQuery.classList.add('mBottom');
  }

  ngOnDestroy() {
    this.bodyQuery.classList.remove('endlessConstellation');
    this.bodyQuery.classList.remove('animated');
    this.bodyQuery.classList.remove('mBottom');
  }

}
