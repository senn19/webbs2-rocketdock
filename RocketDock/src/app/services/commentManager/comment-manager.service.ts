import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {AlertManagerService} from "../alertManager/alert-manager.service";
import {Comment} from "../../../../class/project/comments/Comment"
import {sprintf} from "sprintf-js";
import {ObjectID} from "bson";
import {PATHS} from "../../../../server/routes/PATHS";
import {LANGUAGE} from "../../../../language/LANGUAGE";


@Injectable({
  providedIn: 'root'
})
export class CommentManagerService {


  constructor(private httpClient: HttpClient, private alertService: AlertManagerService) {}

  /**
   * deletes comment from database
   * @param {Comment} comment to delete
   * @returns {Promise<Comment>} deleted comment
   */
  public deleteComment(comment: Comment): Promise<Comment> {
    return this.httpClient.delete<Comment>(PATHS.comment_crud + "/" + comment.getCommentID() )
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return comment;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }

  /**
   * adds comment to database
   * @param {Comment} comment to add
   * @returns {Promise<Comment>} added comment
   */
  public addComment(comment: Comment): Promise<Comment> {
    return this.httpClient.post<Comment>(PATHS.comment_crud, {comment: comment})
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return comment;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }

  /**
   * edits a comment from database
   * @param {Comment} comment to edit
   * @returns {Promise<Comment>} edited comment
   */
  public editComment(comment : Comment): Promise<Comment> {
    return this.httpClient.put<Comment>(PATHS.comment_crud, {comment: comment})
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return comment;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }

  /**
   * gets commentList from Database
   * @returns {Promise<Comment[]>} commentList
   */
  public getCommentList(id : ObjectID): Promise<Comment[]> {
    return this.httpClient.get<Comment[]>(PATHS.comment_crud + "/" + id)
      .toPromise()
      .then((response : any) => {
        let commentList : Comment[] = [];
        for (let item of response.commentList) {
          let comment: Comment = new Comment(
            new ObjectID(item._projectID),
            new ObjectID(item._authorID),
            item._content
          );
          comment.setCommentID(new ObjectID(item._id));
          commentList.push(comment);
        }
        return commentList;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      })
  }
}
