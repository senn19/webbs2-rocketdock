import {Injectable} from '@angular/core';
import {User} from "../../../../class/user/User";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {AlertManagerService} from "../alertManager/alert-manager.service";
import {sprintf} from "sprintf-js";
import {ObjectID} from "bson";
import {LANGUAGE} from "../../../../language/LANGUAGE";
import {PATHS} from "../../../../server/routes/PATHS";

@Injectable()
export class UserManagerService {

  constructor(private httpClient: HttpClient, private alertService: AlertManagerService) {}

  /**
   * deletes user from database
   * @param {User} user to delete
   * @returns {Promise<User>} deleted user
   */
  public deleteUser(user: User): Promise<User> {
    return this.httpClient.delete<User>(PATHS.user_private + "/" + user.getID() + "|" + user.getUsername())
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return user;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }

  /**
   * adds user to database
   * @param {User} user to add
   * @param captcha
   * @returns {Promise<User>} added user
   */
  public addUser(user: User, captcha? : string): Promise<User> {
    return this.httpClient.post<User>(PATHS.user_create, {user: user, captcha : captcha})
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return user;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }

  /**
   * edits a user from database
   * @param {User} user to edit
   * @returns {Promise<User>} edited user
   */
  public editUser(user : User): Promise<User> {
    return this.httpClient.put<User>(PATHS.user_public, {user: user})
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return user;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }

  /**
   * gets userList from Database
   * @returns {Promise<User[]>} userList
   */
  public getUserList(): Promise<User[]> {
    return this.httpClient.get<User[]>(PATHS.user_public)
      .toPromise()
      .then((response : any) => {
        let userList : User[] = [];
        for (let item of response.userList) {
          let user: User = new User(item._username,
            item._password,
            item._email,
            item._userData);
          user.setID(new ObjectID(item._id));
          user.setDate(item._date);
          userList.push(user);
        }
        return userList;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      })
  }

  /**
   * get user by params
   * @param {ObjectID} id (optional)
   * @returns {Promise<User>} user if exists
   */
  public getUserByData(id : ObjectID) : Promise<User> {
    return this.httpClient.get<User>(PATHS.user_public + "/" + id)
      .toPromise()
      .then((response : any) => {
        if(!response.user) return null;
        let user = new User(
          response.user._username,
          response.user._password,
          response.user._email,
          response.user._userData,
          response.user._userVerified);
        user.setDate(response.user._date);
        user.setID(response.user._id);
        return user;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      })
  }
}
