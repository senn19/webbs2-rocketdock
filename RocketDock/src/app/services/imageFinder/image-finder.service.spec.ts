import { TestBed } from '@angular/core/testing';

import { ImageFinderService } from './image-finder.service';

describe('ImageFinderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImageFinderService = TestBed.get(ImageFinderService);
    expect(service).toBeTruthy();
  });
});
