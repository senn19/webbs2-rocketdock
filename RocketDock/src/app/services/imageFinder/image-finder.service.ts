import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {PATHS} from "../../../../server/routes/PATHS";

@Injectable({
  providedIn: 'root'
})
export class ImageFinderService {

  constructor(private httpClient: HttpClient) { }

  public findImage(image : string) : Promise<string> {
    return this.httpClient.post<string>(PATHS.iconFinder, {string : image}).toPromise()
      .then((response : any) => {
        if(!response) return null;
        return response.image;
      }).catch((error: HttpErrorResponse) => {
        console.log(error);
        return null;
      })
  }

}
