import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Log} from "../../../../class/events/Log";
import {sprintf} from "sprintf-js";
import {AlertManagerService} from "../alertManager/alert-manager.service";
import {PATHS} from "../../../../server/routes/PATHS";
import {LANGUAGE} from "../../../../language/LANGUAGE";

@Injectable({
  providedIn: 'root'
})
export class EventManagerService {


  constructor(private httpClient: HttpClient, private alertService: AlertManagerService) {}

  public getEventList() : Promise<Log[]> {
    return this.httpClient.get<Log[]>(PATHS.events)
      .toPromise()
      .then((response : any) => {
        let eventList : Log[] = [];
        for(let log of response.logs) {
          eventList.push(new Log(log._userID, log._groupID, log._username, log._action, log._type, log._date));
        }
        return eventList;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }
}
