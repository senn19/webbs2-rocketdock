import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {AlertManagerService} from "../alertManager/alert-manager.service";
import {User} from "../../../../class/user/User";
import {PATHS} from "../../../../server/routes/PATHS";
import {LANGUAGE} from "../../../../language/LANGUAGE";
import {sprintf} from "sprintf-js";

@Injectable({
  providedIn: 'root'
})
export class AccountManagerService {
  public user : User;
  public group : any;

  constructor(private httpClient: HttpClient, private alertService: AlertManagerService) {}

  /**
   * method to log in user
   * @param email to log in
   * @param password to check
   * @returns {Promise<boolean>} loggedIn value returning
   */
  public loginUser(email, password) : Promise<boolean> {
    return this.httpClient.post(PATHS.login, {_email : email, _password : password}).toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return response;
      }).catch((error : HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
    });
  }

  /**
   * method to check login status
   * @returns {Promise<any>} returns logIn status
   */
  public getSessionData() : Promise<boolean> {
    return this.httpClient.get(PATHS.check).toPromise()
      .then((response : any) => {
        if(!response) return null;
        if(response.group) this.group = response.group;
        if(response.user) {
          this.user = new User(
            response.user._username,
            response.user._password,
            response.user._email,
            response.user._userData);
          this.user.setID(response.user._id);
          this.user.setDate(response.user._date);
        }
        return response;
      })
  }

  /**
   * method to destroy session
   * @returns {Promise<boolean | void>}
   */
  public logoutUser() : Promise<boolean | void> {
    return this.httpClient.get(PATHS.logout).toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return;
      }).catch(() => {
        this.alertService.dangerAlert(LANGUAGE.EN.ACCOUNT_MANAGER.LOGOUT_ERROR);
    });
  }
}
