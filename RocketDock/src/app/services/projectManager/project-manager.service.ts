import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {AlertManagerService} from "../alertManager/alert-manager.service";
import {sprintf} from "sprintf-js";
import {ObjectID} from "bson";
import {Project} from "../../../../class/project/Project";
import {User} from "../../../../class/user/User";
import {PATHS} from "../../../../server/routes/PATHS";
import {LANGUAGE} from "../../../../language/LANGUAGE";

@Injectable({
  providedIn: 'root'
})
export class ProjectManagerService {

  constructor(private httpClient: HttpClient, private alertService: AlertManagerService) {}

  /**
   * deletes project from database
   * @param {Project} project to delete
   * @returns {Promise<Project>} deleted project
   */
  public deleteProject(project: Project): Promise<Project> {
    return this.httpClient.delete<Project>(PATHS.project_crud + "/" + project.getProjectID() + "|" + project.getProjectName())
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return project;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }


  /**
   * adds project to database
   * @param {Project} project to add
   * @returns {Promise<Project>} added project
   */
  public addProject(project : Project): Promise<Project> {
    return this.httpClient.post<Project>(PATHS.project_crud, {project : project})
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return project;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }

  /**
   * edits a project from database
   * @param {Project} project to edit
   * @returns {Promise<Project>} edited project
   */
  public editProject(project : Project): Promise<Project> {
    return this.httpClient.put<Project>(PATHS.project_crud, {project: project})
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return project;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }

  /**
   * gets projectList from Database
   * @returns {Promise<Project[]>} projectList
   */
  public getProjectList(): Promise<Project[]> {
    return this.httpClient.get<Project[]>(PATHS.project_crud)
      .toPromise()
      .then((response : any) => {
        let projectList : Project[] = [];
        for (let item of response.projectList) {
          let project: Project = new Project(item._projectName,
            item._projectDescription,
            new ObjectID(item._projectAuthor),
            item._projectDetails,
            item._projectStatus);
          project.setProjectID(item._id);
          project.setProjectDate(item._projectDate);
          projectList.push(project);
        }
        return projectList;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      })
  }

  /**
   * get project by params
   * @param {ObjectID} id (optional)
   * @returns {Promise<Project>} project if exists
   */
  public getProjectByData(id : ObjectID) : Promise<Project> {
    return this.httpClient.get<Project>(PATHS.project_crud + "/" + id)
      .toPromise()
      .then((response : any) => {
        if(!response.project) return null;
        let projectTeam : User[] = [],
          project = new Project(
            response.project._projectName,
            response.project._projectDescription,
            new ObjectID(response.project._projectAuthor),
            response.project._projectDetails,
            response.project._projectStatus);
        for(let member of response.project._projectDetails._projectTeam) {
          let user : User = new User(
            member._username,
            member._password,
            member._email,
            member._userData,
            member._userVerified
          );
          user.setID(member._id);
          user.setDate(member._date);
          projectTeam.push(user);
        }
        project.getProjectDetails()._projectTeam = projectTeam;
        project.setProjectDate(response.project._projectDate);
        project.setProjectID(new ObjectID(response.project._id));
        return project;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      })
  }

  public getProjectsByUser(user : User) : Promise<Project[]> {
    return this.httpClient.get<Project>(sprintf(PATHS.project_filter, PATHS.filter.user) + "/" + user.getID())
      .toPromise()
      .then((response : any) => {
        if(!response || !response.projects) return null;
        let projectList : Project[] = [];
        for (let item of response.projects) {
          let project: Project = new Project(item._projectName,
            item._projectDescription,
            new ObjectID(item._projectAuthor),
            item._projectDetails,
            item._projectStatus);
          project.setProjectID(item._id);
          project.setProjectDate(item._projectDate);
          projectList.push(project);
        }
        return projectList;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      })
  }
}
