import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {AlertManagerService} from "../alertManager/alert-manager.service";
import {PermissionGroup} from "../../../../class/perm/PermissionGroup";
import {sprintf} from "sprintf-js";
import {ObjectID} from "bson";
import {PATHS} from "../../../../server/routes/PATHS";
import {LANGUAGE} from "../../../../language/LANGUAGE";

@Injectable({
  providedIn: 'root'
})
export class PermManagerService {

  constructor(private httpClient: HttpClient, private alertService: AlertManagerService) {}

  /**
   * deletes group from database
   * @param {Group} group to delete
   * @returns {Promise<Group>} deleted group
   */
  public deleteGroup(group : PermissionGroup): Promise<PermissionGroup> {
    return this.httpClient.delete<PermissionGroup>(PATHS.group_private + "/" + group.getGroupID() + "|" + group.getGroupName())
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return group;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }

  /**
   * adds group to database
   * @param {PermissionGroup} group to add
   * @returns {Promise<PermissionGroup>} added group
   */
  public addGroup(group: PermissionGroup): Promise<PermissionGroup> {
    return this.httpClient.post<PermissionGroup>(PATHS.group_private, {group: group})
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return group;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }

  /**
   * edits a group from database
   * @param {PermissionGroup} group to edit
   * @returns {Promise<PermissionGroup>} edited group
   */
  public editGroup(group : PermissionGroup): Promise<PermissionGroup> {
    return this.httpClient.put<PermissionGroup>(PATHS.group_private, {group: group})
      .toPromise()
      .then((response : any) => {
        this.alertService.successAlert(response.message);
        return group;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      });
  }

  /**
   * gets groupList from Database
   * @returns {Promise<PermissionGroup[]>} groupList
   */
  public getGroupList(): Promise<PermissionGroup[]> {
    return this.httpClient.get<PermissionGroup[]>(PATHS.group_private)
      .toPromise()
      .then((response : any) => {
        let groupList : PermissionGroup[] = [];
        for (let item of response.groupList) {
          let group : PermissionGroup = new PermissionGroup(
            item._groupName,
            item._permissions);
          group.setGroupID(item._id);
          groupList.push(group);
        }
        return groupList;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      })
  }

  /**
   * get group by params
   * @param {ObjectID} id (optional)
   * @returns {Promise<PermissionGroup>} group if exists
   */
  public getGroupByData(id? : ObjectID) : Promise<PermissionGroup> {
    return this.httpClient.get<PermissionGroup>(PATHS.group_public + "/" + id)
      .toPromise()
      .then((response : any) => {
        if(!response.group) return null;
        let group = new PermissionGroup(
          response.group._groupName,
          response.group._permissions);
        group.setGroupID(response.group._id);
        return group;
      }).catch((error: HttpErrorResponse) => {
        this.alertService.dangerAlert(sprintf(LANGUAGE.EN.CLIENT.ERROR, error.error.message));
        return null;
      })
  }
}
