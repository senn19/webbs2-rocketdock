import { TestBed } from '@angular/core/testing';

import { PermManagerService } from './perm-manager.service';

describe('PermManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PermManagerService = TestBed.get(PermManagerService);
    expect(service).toBeTruthy();
  });
});
