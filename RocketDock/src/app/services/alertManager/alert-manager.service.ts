import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable()
export class AlertManagerService {
  public alert : Subject<string[]>;

  constructor() { }

  public successAlert(message : string) : void {
    this.alert.next(["success", "fa-check-circle-o", message]);
  }

  public dangerAlert(message : string) : void {
    this.alert.next(["danger", "fa-exclamation-circle", message]);
  }

  public infoAlert(message : string) : void {
    this.alert.next(["info", "fa-info-circle", message]);
  }

  public warningAlert(message : string) : void {
    this.alert.next(["warning", "fa-exclamation-triangle", message]);
  }
}
