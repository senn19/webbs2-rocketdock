"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LANGUAGE = {
    EN: {
        // server/client responds on different actions
        USER_MANAGER: {
            UPDATE: "Account %s has been successfully updated.",
            UPDATE_ALREADY_EXISTS: "Account %s already exists. Username or email must be different.",
            UPDATE_ERROR: "Account %s is already up to date.",
            ADD: "Account %s has been successfully created.",
            ADD_ALREADY_EXISTS: "Account %s already exists. You can't create a new user with the same username or email.",
            ADD_ERROR: "Account %s could not been inserted. Try again later.",
            DELETE: "Account %s has been successfully deleted.",
            DELETE_ERROR: "Account %s could not been deleted. Try again later.",
            EMAIL_INVALID: "Your entered E-Mail is not valid. Please try again.",
            USERNAME_INVALID: "Your entered Username is not valid. Please try again.",
            MISSING_USERNAME: "No Username has been set. An Error has occurred",
        },
        PROJECT_MANAGER: {
            UPDATE: "Project %s has been successfully updated.",
            UPDATE_ERROR: "Project %s is already up to date.",
            ADD: "Project %s has been successfully created.",
            ADD_ERROR: "Project %s could not been inserted. Try again later.",
            DELETE: "Project %s has been successfully deleted.",
            DELETE_ERROR: "Project %s could not been deleted. Try again later.",
            URL_INVALID: "You have entered an invalid URL. Please correct your Inputs.",
        },
        GROUP_MANAGER: {
            UPDATE: "Group %s has been successfully updated.",
            UPDATE_ERROR: "Group %s is already up to date.",
            ADD: "Group %s has been successfully created.",
            ADD_ERROR: "Group %s could not been inserted. Try again later.",
            DELETE: "Group %s has been successfully deleted.",
            DELETE_ERROR: "Group %s could not been deleted. Try again later."
        },
        COMMENT_MANAGER: {
            UPDATE: "Comment has been successfully updated.",
            UPDATE_ERROR: "Comment is already up to date.",
            ADD: "Comment has been successfully created.",
            ADD_ERROR: "Comment could not been inserted. Try again later.",
            DELETE: "Comment has been successfully deleted.",
            DELETE_ERROR: "Comment could not been deleted. Try again later."
        },
        ACCOUNT_MANAGER: {
            LOGIN_ERROR: "Your email does not fit to your password. Please try again.",
            LOGIN: "You have successfully logged in.",
            LOGOUT_ERROR: "An Error has occurred. You have already been logged out.",
            LOGOUT: "You have successfully logged out.",
        },
        DATABASE: {
            ERROR: "Database Error Code: %s",
        },
        PERMISSIONS: {
            INSUFFICIENT_PERMISSIONS: "Insufficient Permissions to access the source."
        },
        CLIENT: {
            ERROR: "An Error has occurred: %s",
        },
        // wording on different angular pages
        UCP: {
            LOGIN: {
                HEADING: "Sign In to your Account",
                REGISTER: "Create a new Account",
                DIVIDER: "OR",
                EMAIL: "Your E-Mail:",
                PASSWORD: "Your Password:",
                PLACEHOLDER_EMAIL: "Enter your E-Mail here...",
                PLACEHOLDER_PASSWORD: "Enter your Password here...",
                REGISTER_BTN: "Sign Up",
                SOCIAL_BTN: "Sign Up with",
                SUBMIT_BTN: "Sign In"
            },
            REGISTER: {
                USERNAME: "Your Username",
                PLACEHOLDER_USERNAME: "Enter your Username here...",
                EMAIL: "Your E-Mail:",
                PLACEHOLDER_EMAIL_REPEAT: "Repeat your E-Mail here...",
                PLACEHOLDER_EMAIL: "Enter your E-Mail here...",
                PASSWORD: "Your Password:",
                PLACEHOLDER_PASSWORD_REPEAT: "Repeat your Password here...",
                PLACEHOLDER_PASSWORD: "Enter your Password here...",
                REGISTER_BTN: "Sign Up",
                CANCEL_BTN: "Cancel",
                ALERTS: {
                    EMAIL_PASSWORD_MISSING: "You need an Username, an E-Mail address and a Password.",
                    EMAIL_MISS_MATCH: "Your entered E-Mail does not fit to your repeated E-Mail.",
                    PASSWORD_MISS_MATCH: "Your entered Password does not fit to your repeated Password.",
                    CAPTCHA: "You need to solve the captcha to continue."
                }
            },
            HOME: {
                WELCOME_BACK: "Welcome back %s!",
                YOUR_PROFILE: "Your RocketDock Profile",
                USER_NULL: "No username set",
                GROUP_NULL: "No group",
                BTN_EDIT_MY_PROFILE: "Edit my profile",
                BTN_VIEW_MY_PROFILE: "View my profile",
                MY_PROJECTS: "Your Projects",
                IN_CASE_YOU_MISSED: "In case you have missed",
                YOUR_TODO: "Your ToDos",
                VIEW_MORE_PROJECTS: "View more Projects...",
                NO_PROJECTS_FOUND: "You have no projects at the moment.",
            },
            MEMBER_LIST: {
                TITLE: "Memberlist",
                FILTER_PLACEHOLDER: "filter by member...",
                MEMBER_SINCE: "(member since %s)",
            },
            ABOUT_US: {
                TITLE: "About Us",
                IMG_TITLE: "Nice to meet you ♥",
                IMG_SUBTITLE: "Here we are. Team RocketDock 🚀",
                ABOUT_TITLE: "Who we are...",
                ABOUT_TEXT: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
            },
            DISCLAIMER: {
                TITLE: "Disclaimer",
                LIMITATION: "Limitation of liability for internal content",
                LIMITATION_TEXT: "The content of our website has been compiled with meticulous care and to the best of our knowledge. However, we cannot assume any liability for the up-to-dateness, completeness or accuracy of any of the pages.\n" +
                    "\n" +
                    "Pursuant to section 7, para. 1 of the TMG (Telemediengesetz – Tele Media Act by German law), we as service providers are liable for our own content on these pages in accordance with general laws. However, pursuant to sections 8 to 10 of the TMG, we as service providers are not under obligation to monitor external information provided or stored on our website. Once we have become aware of a specific infringement of the law, we will immediately remove the content in question. Any liability concerning this matter can only be assumed from the point in time at which the infringement becomes known to us.",
                EXTERNAL_LINKS: "Limitation of liability for external links",
                EXTERNAL_LINKS_TEXT: "Our website contains links to the websites of third parties („external links“). As the content of these websites is not under our control, we cannot assume any liability for such external content. In all cases, the provider of information of the linked websites is liable for the content and accuracy of the information provided. At the point in time when the links were placed, no infringements of the law were recognisable to us. As soon as an infringement of the law becomes known to us, we will immediately remove the link in question.",
                COPYRIGHT: "Copyright",
                COPYRIGHT_TEXT: "The content and works published on this website are governed by the copyright laws of Germany. Any duplication, processing, distribution or any form of utilisation beyond the scope of copyright law shall require the prior written consent of the author or authors in question.",
                DATA_PROTECTION: "Data protection",
                DATA_PROTECTION_TEXT: "A visit to our website can result in the storage on our server of information about the access (date, time, page accessed). This does not represent any analysis of personal data (e.g., name, address or e-mail address). If personal data are collected, this only occurs – to the extent possible – with the prior consent of the user of the website. Any forwarding of the data to third parties without the express consent of the user shall not take place.\n" +
                    "\n" +
                    "We would like to expressly point out that the transmission of data via the Internet (e.g., by e-mail) can offer security vulnerabilities. It is therefore impossible to safeguard the data completely against access by third parties. We cannot assume any liability for damages arising as a result of such security vulnerabilities.\n" +
                    "\n" +
                    "The use by third parties of all published contact details for the purpose of advertising is expressly excluded. We reserve the right to take legal steps in the case of the unsolicited sending of advertising information; e.g., by means of spam mail.",
            },
            PROFILE_EDIT: {
                TITLE: "Edit my Profile",
                PRIVATE_DATA: "Private Data",
                CHANGE_AVATAR: "Change Avatar",
                PUBLIC_DATA: "Public Data",
                SAVE_CHANGES: "Save Changes",
                BTN_SAVE_CHANGES: "Save Changes",
                BTN_CANCEL_RETURN: "Cancel & Return",
            },
            PROJECTS: {
                OPEN_PROJECTS: "All Projects",
                CLOSED_PROJECTS: "Finished Projects",
                MY_PROJECTS: "My Projects",
                BTN_SWITCH_OPEN: "All Projects",
                BTN_SWITCH_CLOSE: "Finished Projects",
                BTN_SWITCH_MY: "My Projects",
                BTN_VIEW: "View Project",
                BTN_CREATE: "Create new Project",
                PROJECT_FILTER_PLACEHOLDER: "filter by project...",
            }
        },
        ACP: {
            PROJECT_MANAGER: {
                ALERTS: {
                    MISSING_PROJECT_DATA: "You've forgot to add some information about your project.",
                    MISSING_PROJECT_MANAGER: "You need at least one project manager to create a project.",
                    MISSING_PROJECT_AVATAR: "You need to upload an image for your project.",
                    MISSING_PROJECT_URLS: "You need to add at least one url to your project."
                },
                ADD: {
                    TITLE: "Create new Project",
                    PROJECT_DATA: "Project Data",
                    PROJECT_IMAGE: "Project Image",
                    PROJECT_MEMBER: "Project Member",
                    PROJECT_LINKS: "Project Links",
                    PROJECT_SETTINGS: "Project Settings",
                },
                EDIT: {
                    TITLE: "Edit your Project",
                }
            }
        }
    }
};
//# sourceMappingURL=LANGUAGE.js.map